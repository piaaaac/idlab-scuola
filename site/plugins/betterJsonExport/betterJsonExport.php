

<?php
// site/plugins/page-methods.php

/*
 * getAllImageURLs
 *
 * Returns an array of image objects for each image in the page
 * (See the buildImage function at the bottom of this gist)
 */

page::$methods['getAllImageURLs'] = function($page) {
  $images = [];
  if ($page->images()->count() === 0) return $images;
  foreach ($page->images()->sortBy('sort') as $imageSource) {
    array_push($images, buildImage($imageSource));
  }
  if ($page->cover() === false) {
    $images[0]->isCover = true;
  }
  return $images;
};



/*
 * getPublicContent
 *
 * returns a page's content in a more JSON-friendly form.
 * Use in a route as         response::json($page->getPublicContent());
 *   or in a template as     json_encode($page->getPublicContent());  
 *
 * @param object   $page         - provided by Kirby
 * @param bool|int $withChildren - allows you to recursively return getPublicContent on the children of the page:
 *                                  $page->getPublicContent(true) - returns all children and their children and so on
 *                                  $page->getPublicContent(2)    - returns children 2 levels deep
 *
 * @return array   $content      - Our friendly array
 */

page::$methods['getPublicContent'] = function($page, $withChildren = false, $onlyVisibleChildren = true) {
  $content = $page->content()->toArray();
  $content['slug'] = $page->uid();
  $content['id'] = $page->id();
  $content['isVisible'] = $page->isVisible();
  $content['type'] = $page->intendedTemplate();
  $content['images'] = $page->getAllImageURLs();

  foreach ($content as $key => $value) {
    if (smellsLikeYaml($value)) {
      $content[$key] = yaml($value);
    }
    if (smellsLikeNum($value)) {
      $content[$key] = floatval($value);
    }
    if (smellsLikeBool($value)) {
      $content[$key] = ($value === 'true') ? true : false;
    }
  }

  if ($withChildren) {
    $content['children'] = [];
    $children = $page->children();
    if ($onlyVisibleChildren) $children = $children->visible();
    if ($children->count() > 0) {
      foreach ($children as $child) {
        $childContent = $child->getPublicContent($withChildren, $onlyVisibleChildren);
        array_push($content['children'], $childContent);
      }
    }
    if (count($content['children']) === 0) unset($content['children']);
  }
  if (gettype($withChildren) === 'integer') $withChildren -= 1;

  return $content;
};

?>

<?php

// site/plugins/helpers.php

/**
 *  A few functions to help detect certain types of content. (Kirby will return "true" as a string!)
 */

function smellsLikeNum($input) {
  if (gettype($input) !== 'string') return false;
  preg_match('/^[0-9,]+(.[0-9]+)?$/', $input, $output);
  // print_r($output);
  return (count($output) > 0);
}

function smellsLikeYaml($input) {
  if (gettype($input) !== 'string') return false;
  if (strlen($input) === 0) return false;
  $yamlArray = '/^-[\n\r\s]+[a-z]*:/';
  preg_match($yamlArray, $input, $output);
  return (count($output) > 0);
};

function smellsLikeBool($input) {
  if ($input === 'true' || $input === 'false') return true;
  return false;
}

/*
 * buildImage
 *
 * used to generate resized versions of the images at a handful of other sizes.
 * This can be useful if you're using srcset on your images on the frontend.
 * Doesn't build upscaled images.
 *
 * @return array   $content      - Our friendly array
 */
function buildImage($imageSource) {
  $imageSizes = [
    '2400' => 2400,
    '1600' => 1600,
    '1200' => 1200,
    '800' => 800,
    '400' => 400,
    '50' => 50
  ];
  $image = new StdClass();
  $image->srcset = [];
  $image->url = (string)$imageSource->url();
  $image->meta = $imageSource->meta()->toArray();
  $image->parentTitle = (string)$imageSource->page()->title();
  if (count($image->meta) === 0) $image->meta = new StdClass();
  array_push($image->srcset, array(
    'label' => 'original',
    'width' => $imageSource->width(),
    'height' => $imageSource->height(),
    'url' => $imageSource->url(),
    'isOriginal' => true,
  ));
  foreach ($imageSizes as $label => $width) {
    if ($imageSource->width() > $width) {
      $thumbnail = [
        'label' => $label,
        'width' => $width,
        'url' => thumb($imageSource, array('width' => $width), false)
      ];
      array_push($image->srcset, $thumbnail);
    }
  }
  return $image;
}


?>