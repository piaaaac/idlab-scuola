<?php
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');
date_default_timezone_set('Europe/Rome');

return function($site, $pages, $page) {

  snippet('logincheck-block-non-admin', ['site' => $site]);

  $method = param("method");
  if(!$method){
    echo "No payment method received (Error code 69781110).";
    exit();
  }
  $orderId = param("order");
  if(!$orderId){
    echo "No order id received (Error code 69781112).";
    exit();
  }
  $notes = $_GET["message-text"];

  // echo "<br>method: $method";
  // echo "<br>orderId: $orderId";
  // echo "<br>notes: $notes";
  // exit();

  $ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);

  $paymentRecord = [];
  $cartArray = cartObjectFromArticleeeeIdsString($ordine->articleeeeIds()->value());
  foreach ($cartArray as $item) {
    $paymentRecord[] = $item->articleeeeId .":EUR". $item->costoFinale;
  }

  // process payment by updating ordine page
  $pageData = array(
    // sezione ordine
    'title'                 => "PAYED-". $ordine->uid() ."-". str::slug($ordine->form_cognome()->value() ."-". $ordine->form_nome()->value()),
    'pagamentoOk'           => 1,
    'pagamentoMetodo'       => $method,
    'dataOraPagamento'      => date('Y-m-d H:i:s'),
    'pagamentoRiferimento'  => "Pagamento confermato da ". $site->user()->firstname() ." ". $site->user()->lastname() ." il ". date('d/m/Y') ." alle ". date('G:i:s'),
    'note'                  => $notes,
    'paymentRecord'         => implode("\n", $paymentRecord)
  );

  // a::show($pageData);
  // exit();

  try {
    $ordine->update($pageData);
  } catch(Exception $e) {
    echo $e->getMessage();
  }
  
  // redirect::to(page("iscrizione2")->url() ."/order:$orderId");
  redirect::to(page("sendemail")->url() ."/emailname:confermaPagamento/order:$orderId");

}
 
?>
