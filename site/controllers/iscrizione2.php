<?php
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');
snippet('emailbodies');

// a::show($_POST);
// exit();

/* ----------------------------------------------
Receives via POST:
- form data
- order code if order exists (kirby url param "order")

---

- if no order num received
  - if there's no posted form data
    - redirect to iscrizione1
  - else if there's posted data
    - create verif code & orderId
    - create new order page
    - send email
    - redirect to self with this orderId
      redirect::to($page->url() ."/order:$newOrderId");

- if order num received
  - load order from orderId
  - if email is not verified
    - show code input form with action iscrizione2-verifyemail
      $snippet = iscrizione2a-inserire-codice
  - if email is verified
    - if order is paid
      - redirect to page showing order recap
    - if order is not paid
      - if there's posted form data
        - update order
        - redirect to self with orderId and no post data
      - else
        - calculate total amount for payment verification
        - prepare payer data for paypal form
        - show recap page
          $snippet = iscrizione2b-recap-ordine
          - order details
          - link to edit details
          - paypal button



### iscrizione2verifyemail
### receiving smth like scuolaarteapplicata.com/iscrizione2Verify/order:00000003gy7tegsF/code:678678
- read orderId = param(order)
- read code = param(code)
- get all iscrizione with that orderId
- make sure they all have the same code or print system error
- if code matches
  - for each matching iscrizione
    - update page iscrizione
  - redirect to iscrizione2 with orderId
    redirect::to(page(iscrizione2)->url() ."/order:$orderId");
- else print message to user offering to resend email or start a new order

----------------------------------------------- */

return function($site, $pages, $page) {
  $postDataPresent = (isset($_POST["dataOraOrdine"])) && ($_POST["dataOraOrdine"] != "");

  // --------------------------------------------
  // if no order num received - it's a new order
  // --------------------------------------------
  if(param("order") == null){

    // if there's no posted form data
    if(!$postDataPresent){

      redirect::to(page("iscrizione1")->url());

    // else if there's posted data
    } else {

      // create a unique verification code and order id
      $f = kirby()->roots->index().'/emailVerificationCodes.txt';
      $raw = f::read($f);
      $lines = explode("\n", trim($raw));
      $orderIds = [];
      $usedCodes = [];
      foreach($lines as $line) {
        $pieces = explode(",", trim($line));
        $orderIds[] = intval(substr($pieces[0], 0, 8));
        $usedCodes[] = $pieces[1];
      }
      $lastOrderId = max($orderIds);
      $newOrderId = sprintf('%08d', ($lastOrderId+1)) . strtolower(str::random(8, "alphaNum"));
      $verificationCode = (string)(str::random(6, "num"));
      while(in_array($verificationCode, $usedCodes)){
        // echo "$verificationCode was already present";
        $verificationCode = (string)(str::random(6, "num"));
      }
      $newLine = "\n$newOrderId,$verificationCode";
      $append = true;
      f::write($f, $newLine, $append);

      
      // --------------------------------------------
      // create new page Ordine
      // --------------------------------------------
      
      // --- prepare values for Kirby page
      
      $cartArray = cartObjectFromArticleeeeIdsString($_POST["cartarticleeeeIds"]);
      $costoTotale = 0;
      foreach ($cartArray as $item) {
        $costoTotale += $item->costoFinale;
      }
      $costoTotale = round($costoTotale, 2);

      $articleNames = [];
      foreach($cartArray as $item){
        $articleNames[] = $item->corso->corsoId .": ". $item->corso->title .", ". $item->turno->uid;
      }

      $pageUid = "o$newOrderId";
      $pageUri = 'segreteria-ordini/'. $pageUid;
      $formData = ordineDataFromPostedForm();
      $metadata = array(
        'title'             => "NEW-$pageUid-". str::slug("$_POST[cognome] $_POST[nome]"),
        'orderId'           => $newOrderId,
        'importoTotale'     => $costoTotale,
        'verificationCode'  => $verificationCode,
        'emailVerificata'   => 0,
        'articleeeeIds'     => $_POST["cartarticleeeeIds"],
        'articleNames'      => implode("\n", $articleNames),
        'cartArray'         => json_encode($cartArray)
      );
      $pageData = $formData + $metadata;

      // --- create Kirby page
      try {
        $newOrdine = page()->create($pageUri, 'segreteria-ordine', $pageData);
      } catch(Exception $e) {
        echo $e->getMessage() ."\nError creating new Kirby page (Error code 32865822)";
      }

      // --- Save iscrizioni to structure field (variable number of fields)

      $n = 1;
      foreach ($cartArray as $item) {
        $data = [];
        $data["iscrizione_articleeeeId"] = $item->articleeeeId;
        $data["iscrizione_article_desc"] = 
          $item->corso->title ." — ". $item->turno->nome 
          ." (". $item->turno->dataInizio ." - ". $item->turno->dataFine .")";
        if (isset($_POST["iscrittoa__$n"]) && $_POST["iscrittoa__$n"] === "use_buyer_data") {
          $data["iscrizione_cognome"]   = $formData["form_cognome"];
          $data["iscrizione_nome"]      = $formData["form_nome"];
          $data["iscrizione_email"]     = $formData["form_email"];
          $data["iscrizione_telefono"]  = $formData["form_telefono"];
        } else {
          $data["iscrizione_cognome"]   = $_POST["iscrittoa__$n-cognome"];
          $data["iscrizione_nome"]      = $_POST["iscrittoa__$n-nome"];
          $data["iscrizione_email"]     = $_POST["iscrittoa__$n-email"];
          $data["iscrizione_telefono"]  = $_POST["iscrittoa__$n-telefono"];
        }
        addToStructure($newOrdine, "iscrizioni", $data);
        $n++;
      }

      // --------------------------------------------
      // send email with the code
      // --------------------------------------------

      $email = verificaEmail($newOrdine, $verificationCode);
      if($email->send()) {
        redirect::to($page->url() ."/order:". $newOrdine->orderId()->value());
      } else {
        echo $email->error()->message();
        exit();
      }
    }
  }

  // --------------------------------------------
  // it's an existing order
  // --------------------------------------------

  else {

    // --- load all ordine from orderId
    $orderId = param("order");
    $ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);
    if(!$ordine){
      echo "Numero ordine non trovato. <a href=\"". $page->url() ."\">Esegui una nuova iscrizione</a>";
      exit();
    }

    // --- if email is not verified
    if(
      $ordine->emailVerificata()->value() != 1
      && userIsAdmin($site) == false
    ){
      return array(
        "snippetName"   => "iscrizione2a-inserire-codice",
        "snippetData"    => array("orderId" => $orderId)
      );
    } 

    // --- if email is verified
    else {

      // --- if order is paid
      if($ordine->pagamentoOk()->value() == 1){

        $orderData = formDataFromPage($ordine);
        $cartArray = null; // won't be used for recap

        $otherData = array(
          'cartArray'           => $cartArray,
          'orderId'             => $orderId
        );
        return array(
          "snippetName"   => "iscrizione2b-recap-ordine",
          "snippetData"   => array(
            "orderIsPayed"      => true,
            "orderData"         => $orderData,
            "otherData"         => $otherData
          )
        );

      }

      // --- if order is not paid
      else{
        
        // --- if there's posted form data
        if($postDataPresent){

          // --------------------------------------------
          // update page ordine
          // --------------------------------------------

          $cartArray = cartObjectFromArticleeeeIdsString($_POST["cartarticleeeeIds"]);
          $costoTotale = 0;
          foreach ($cartArray as $item) {
            $costoTotale += $item->costoFinale;
          }
          $costoTotale = round($costoTotale, 2);

          $articleNames = [];
          foreach($cartArray as $item){
            $articleNames[] = $item->corso->corsoId .": ". $item->corso->title .", ". $item->turno->uid;
          }

          $formData = ordineDataFromPostedForm();
          $metadata = array(
            'title'             => "OPEN-". $ordine->uid() ."-". str::slug("$_POST[cognome] $_POST[nome]"),
            'importoTotale'     => $costoTotale,
            'articleeeeIds'     => $_POST["cartarticleeeeIds"],
            'articleNames'      => implode("\n", $articleNames)
          );
          $pageData = $formData + $metadata;
          try{
            $ordine->update($pageData);
          } catch(Exception $e) {
            echo "Error while updating ordine (Error code 56342412).";
            echo $e->getMessage();
            exit();
          }

          // --- redirect to self with orderId and no post data
          redirect::to($page->url() ."/order:$orderId");

        }

        // --- if there's no posted form data
        else{


          $cartArray = cartObjectFromArticleeeeIdsString($ordine->articleeeeIds()->value());
          $orderData = formDataFromPage($ordine);
          $fatturazioneAziendale = $orderData["fatturazioneAziendale"];

          // --- prepare payer data for paypal form
          $paypalForm = array(
            "pp_first_name"      => $ordine->form_nome()->value(),
            "pp_last_name"       => $ordine->form_cognome()->value(),
            "pp_address1"        => r($fatturazioneAziendale, 
                                      $ordine->form_aziendaCivico() .", ". $ordine->form_aziendaIndirizzo(),
                                      $ordine->form_residenzaCivico() .", ". $ordine->form_residenzaIndirizzo()
                                    ),
            "pp_address2"        => "",
            "pp_address_country" => "Italia",
            "pp_city"            => r($fatturazioneAziendale, 
                                      $ordine->form_aziendaLuogo()->value(), 
                                      $ordine->form_residenzaLuogo()->value()
                                    ),
            "pp_state"           => r($fatturazioneAziendale, 
                                      $ordine->form_aziendaProvincia()->value(), 
                                      $ordine->form_residenzaProvincia()->value()
                                    ),
            "pp_zip"             => r($fatturazioneAziendale, 
                                      $ordine->form_aziendaCap()->value(), 
                                      $ordine->form_residenzaCap()->value()
                                    )
          );
      
          $otherData = array(
            'cartArray'           => $cartArray,
            'orderId'             => $orderId
          );

          return array(
            "snippetName"   => "iscrizione2b-recap-ordine",
            "snippetData"   => array(
              "orderIsPayed"      => false,
              "orderData"         => $orderData,
              "paypalForm"        => $paypalForm,
              "otherData"         => $otherData
            )
          );
        }
      }
    }
  }
}

?>
