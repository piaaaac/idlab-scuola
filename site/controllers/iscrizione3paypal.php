<?php
date_default_timezone_set('Europe/Rome');

// kill($_POST);

$pp_details = json_decode($_POST["pp_details"], true);

$pp_data = [
  "transactionId"       => $pp_details["id"],
  "status"              => $pp_details["status"],
  
  "payerId"             => $pp_details["payer"]["payer_id"],
  "payerEmail"          => $pp_details["payer"]["email_address"],
  "payerName"           => $pp_details["payer"]["name"]["given_name"],
  "payerSurname"        => $pp_details["payer"]["name"]["surname"],

  "payee"               => $pp_details["purchase_units"][0]["payee"]["email_address"],

  "captureId"           => $pp_details["purchase_units"][0]["payments"]["captures"][0]["id"],
  "captureStatus"       => $pp_details["purchase_units"][0]["payments"]["captures"][0]["status"],
  "captureOrderId"      => $pp_details["purchase_units"][0]["payments"]["captures"][0]["invoice_id"],
  "captureTime"         => $pp_details["purchase_units"][0]["payments"]["captures"][0]["create_time"],
  "capturePayedAmount"  => $pp_details["purchase_units"][0]["payments"]["captures"][0]["amount"]["value"],

  "rawPaypalAnswer"     => $_POST["pp_details"]
];

// kill($pp_data);

return function ($site, $pages, $page) use ($pp_data) {


  // Check the payment_status is COMPLETED

  if ($pp_data["status"] != "COMPLETED") {
    echo "PayPal error: uncorrect payment status '". $pp_data["status"] ."' (Error code 54672309)";
    exit();    
  }
  if ($pp_data["captureStatus"] != "COMPLETED") {
    echo "PayPal error: uncorrect payment status '". $pp_data["captureStatus"] ."' (Error code 23548769)";
    exit();    
  }

  // Recupera pagina ordine
  
  $ordine = page("segreteria-ordini")->children()->findBy("orderId", $pp_data["captureOrderId"]);
  if(!$ordine){
    echo "Error: ordine with orderId='". $pp_data["captureOrderId"] ."' not found (Error code 09098977)";
    exit();
  }

  // Check that receiver_email is your Primary PayPal email

  if(strcmp ($pp_data["payee"], c::get('paypalBusinessEmail')) != 0){
    echo "PayPal error: receiver_email '". $pp_data["payee"] ."' does not match merchant email (Error code 19829093)";
    exit();
  }

  // Check that payment_amount/payment_currency are correct
  
  $amtExpected = (float)($ordine->importoTotale()->value());
  $amtResulting = (float)($pp_data["capturePayedAmount"]);

  if($amtResulting != $amtExpected){
    echo "PayPal error: payment amount mismatch (Error code 62759198)";
    exit();
  }

  // Check that txn_id (transactionId) has not been previously processed

  if(
    page('segreteria-ordini')->children()->findBy('txnId', $pp_data["transactionId"])
    || $ordine->pagamentoOk()->value()
  ){
    echo "Transaction already processed (Error code 75656756)";
    echo "<br><br>\n\n";
    echo $pp_data["rawPaypalAnswer"];
    exit();
  }

  // $paymentRecord = [];
  // $n = $keyarray["num_cart_items"];
  // for ($i=1; $i <= $n; $i++) { 
  //   $paymentRecord[] = $keyarray["item_number$i"] .":EUR". $keyarray["mc_gross_$i"];
  // }

  // process payment by updating ordine page
  $pageData = array(
    // sezione ordine
    'title'                 => "PAYED-". $ordine->uid() ."-". str::slug($ordine->form_cognome()->value() ."-". $ordine->form_nome()->value()),
    'pagamentoOk'           => 1,
    'dataOraPagamento'      => date('Y-m-d H:i:s'),
    'pagamentoRiferimento'  => "paypal: ". $pp_data["transactionId"],
    // 'paymentRecord'         => implode("\n", $paymentRecord),
    'pagamentoMetodo'       => "paypal",
    
    // sezione paypal
    'pppaymentStatus'       => $pp_data["captureStatus"],
    'pppaymentTxnId'        => $pp_data["transactionId"],
    'pppaymentDate'         => $pp_data["captureTime"],
    'pppaymentAmount'       => $pp_data["capturePayedAmount"] ." EUR",
    'pppaymentPayerName'    => $pp_data["payerName"] ." ". $pp_data["payerSurname"],
    'pppaymentPayerId'      => $pp_data["payerId"],
    'pppaymentOrderId'      => $pp_data["captureOrderId"],
    // 'pppaymentArticles'     => $ordine->articleNames()->value(),
    'pppaymentRawAnswer'    => $pp_data["rawPaypalAnswer"]
  );
  try {
    $ordine->update($pageData);
  } catch(Exception $e) {
    echo $e->getMessage();
  }
  
  // redirect::to(page("iscrizione2")->url() ."/order:$orderId");
  redirect::to(page("sendemail")->url() ."/emailname:confermaPagamento/order:". $pp_data["captureOrderId"]);




}

?>
