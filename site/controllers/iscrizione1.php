<?php
error_reporting(E_ALL);
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');

/* TO DO

if orderId is payed redirect to 
redirect to page showing order recap

*/

return function($site, $pages, $page) {

  $isCached = false;
  $cachedFormData = null;
  $cartArticleeeeIdsString = "";
  $fatturazioneAziendale = "";

  // we come from recap page to modify some data
  if(param("order") != null){
    $isCached = true;
    $orderId = param("order");
    $ordine = page("segreteria-ordini")->children()->findBy("orderId", $orderId);
    if(!$ordine){
      echo "Numero ordine non trovato. <a href=\"". $page->url() ."\">Esegui una nuova iscrizione</a>";
      exit();
    }
    $cachedFormData = formDataFromPage($ordine);
    $fatturazioneAziendale = $cachedFormData["fatturazioneAziendale"];
    $cartArticleeeeIdsString = $ordine->articleeeeIds()->value();
  }

  // we come from elsewhere and use the cart
  else{
    /////////////// 
    ////////
    // Server
    $cartArticleeeeIdsString = a::get($_COOKIE, c::get("cartCookieKey"));
    // Local
    // $cartArticleeeeIdsString = "corso-15~turno-3,corso-15~turno-2";
    ////////
    /////////////// 
  }

  $cartArray = cartObjectFromArticleeeeIdsString($cartArticleeeeIdsString);
  $costoTotale = 0;
  foreach ($cartArray as $item) {
    $costoTotale += $item->costoFinale;
  }
  $costoTotale = round($costoTotale, 2);

  // --- form action
  $editingExistingOrder = ($isCached && isset($orderId) && $orderId);
  $formIscrizioneAction = page('iscrizione2')->url();
  if($editingExistingOrder){
    $formIscrizioneAction .= "/order:". $orderId;
  }

  // --- select values
  $provinceStr = file_get_contents(kirby()->roots()->assets() ."/data/province.json");
  $province = json_decode($provinceStr, true);
  $cittadinanzeStr = file_get_contents(kirby()->roots()->assets() ."/data/citizenships.json");
  $cittadinanze = json_decode($cittadinanzeStr, true);
  $cittadinanze1 = array_filter($cittadinanze, function($c){return $c["group"] === "1";});
  $cittadinanze2 = array_filter($cittadinanze, function($c){return $c["group"] === "2";});

  return array(
    "cartArticleeeeIdsString"   => $cartArticleeeeIdsString,
    "cartArray"                 => $cartArray,
    "formIscrizioneAction"      => $formIscrizioneAction,
    "isCached"                  => $isCached,
    "cachedFormData"            => $cachedFormData,
    "cittadinanze1"             => $cittadinanze1,
    "cittadinanze2"             => $cittadinanze2,
    "province"                  => $province,
    "costoTotale"               => $costoTotale,
  );
}
?>
