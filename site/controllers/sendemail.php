<?php 
snippet('emailbodies');

/* -----------------------------------
    Available emailNames:

    - verificaEmail
      sendemail/emailname:verificaemail/order:XXXXXXXXXX

    - confermaPagamento
      sendemail/emailname:confermaPagamento/order:XXXXXXXXXX

----------------------------------- */

return function($site, $pages, $page) {

  $emailName = param("emailname");

  if($emailName == "verificaemail"){
    $orderId = param("order");
    $ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);
    $email = verificaEmail($ordine, $ordine->verificationCode()->value());
    if($email->send()) {
      // confirmation message to user
      $feedbackMsg = "L'email è stata inviata all'indirizzo indicato.";
      // link text
      $linkText = "OK";
      // link destination
      $linkDestination = page("iscrizione2")->url() ."/order:". $ordine->orderId()->value();
    } else {
      echo "Error sending the email. (Error code 32104684)";
      echo $email->error()->message();
      exit();
    }
  }

  else if($emailName == "confermaPagamento"){
    $orderId = param("order");
    $ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);
    $email = confermaPagamento($ordine);
    if($email->send()) {
      // confirmation message to user
      $feedbackMsg = "
Il pagamento del tuo ordine è avvenuto con successo, e ti è stata inviata la conferma via email.\n
Benvenuto/a alla Scuola SUPERiore di Arti Applicate di Milano!";
      // link text
      $linkText = "OK";
      // link destination
      $linkDestination = page("iscrizione2")->url() ."/order:". $ordine->orderId()->value();
    } else {
      echo "Error sending the email. (Error code 32104684)";
      echo $email->error()->message();
      exit();
    }
  }

  return array(
    'msg'             => $feedbackMsg,
    'linkText'        => $linkText,
    'linkDestination' => $linkDestination
  );
}

?>
