<?php

return function($site, $pages, $page) {

  $query   = get('q');
  // $results = $site->index()->visible()->search($query, 'title|text');
  $resultsDiscipline = page('discipline')->search($query)->visible();
  $resultsCorsi = page('corsi')->search($query)->visible()->filterBy('template', 'corso'); // --- filter out pages 'corso-turno'
  $resultsDocenti = page('la-scuola')->search($query)->visible();
  $results = $resultsDiscipline->merge($resultsCorsi)->merge($resultsDocenti);

  return array(
    'query'   => $query,
    'results' => $results,
  );

};
