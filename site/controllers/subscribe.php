<?php
date_default_timezone_set('Europe/Rome');

return function($site, $pages, $page) {
  
  $email = $_POST['q'];
  if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    return array(
      "success" => false,
      "email" => $email,
      "error" => "Email non valida."
    );
  }

  // --------------------------------------------
  // use mailchimp api
  // --------------------------------------------

  // --------------------------------------------
  // create new page newsletter-contact
  // --------------------------------------------

  // --- prepare values for Kirby page
  $pageUid = str::slug($email);
  $pageUri = 'newsletter-contacts/'. $pageUid;
  $pageData = [
    'title' => $email,
    'submissionDate' => date('Y-m-d H:i:s')
  ];

  // --- check if page exists
  if(page("newsletter-contacts")->children()->findBy("title", $email)){
    return array(
      "success" => false,
      "email" => $email,
      "error" => "L'email risulta già iscritta."
    );
  }

  // --- create Kirby page
  try {
    $newContact = page()->create($pageUri, 'newsletter-contact', $pageData);
  } catch(Exception $e) {
    return array(
      "success" => false,
      "email" => $email,
      "error" => "Errore di inserimento dati (Error code 65762567). ". $e->getMessage()
    );
  }

  return array(
    "success" => true,
    "email" => $email,
    "error" => ""
  );

}
?>
