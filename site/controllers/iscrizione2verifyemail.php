<?php
error_reporting(E_ALL);
date_default_timezone_set('Europe/Rome');

// a::show($_POST);

/* ----------------------------------------------

### iscrizione2verifyemail
### receiving smth like scuolaarteapplicata.com/iscrizione2Verify/order:00000003gy7tegsF and code via post
- read orderId = param(order)
- read code = param(code)
- get ordine from orderId
- make sure they all have the same code or print system error
- if code matches
  - for each matching iscrizione
    - update page iscrizione
  - redirect to iscrizione2 with orderId
    redirect::to(page(iscrizione2)->url() ."/order:$orderId");
- else print message to user offering to resend email or start a new order

----------------------------------------------- */

return function($site, $pages, $page) {

  // --- read orderId = param(order)
  $orderId = param("order");
  
  // --- read code
  $postedCode = $_POST["code"];

  // --- get ordine from orderId
  $ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);

  // --- make sure they all have the same code or print system error
  $correctCode = $ordine->verificationCode()->value();
  
  // --- if code matches
  if($postedCode == $correctCode){

    // --- update page ordine
    try {
      $ordine->update(array(
        'title'           => "OPEN-". $ordine->uid() ."-". str::slug($ordine->form_cognome()->value() ."-". $ordine->form_nome()->value()),
        'emailVerificata' => 1,
        'dataOraVerifica' => date('Y-m-d H:i:s')
      ));
    } catch(Exception $e) {
      echo "Error while updating ordine (Error code 47654856). Message: ". $e->getMessage();
      exit();
    }
    
    // --- redirect to iscrizione2 with orderId
    redirect::to(page("iscrizione2")->url() ."/order:$orderId");
  }

  // --- else print message to user offering to resend email or start a new order
  else {
    echo "
      Il codice immesso &egrave; errato.<br />
      <a href=\"". page("iscrizione2")->url() ."/order:$orderId" ."\">Riprova</a>
    ";
  }
}

?>
