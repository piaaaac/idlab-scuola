<?php

return function($site, $pages, $page) {

  $printed = get('printed', null);
  $orderId = get('orderId', null);

  if($printed !== null && $orderId !== null){
    try {
      $ordine = page("segreteria-ordini")->children()->findBy("orderId", $orderId);
      if($ordine === null){
        go(page('error')->url() ."/err:3378923729");
      }
      $ordine->update(array(
        'stampato' => $printed
      ));
      return array(); // go to template page

    } catch(Exception $e) {

      echo $e->getMessage();
      echo "printed: $printed";
      echo "<br />";
      echo "orderId: $orderId";
      echo "<br />";
      exit();

    }

  }
  return array(); // go to template page

};
