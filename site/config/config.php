<?php

/*

---------------------------------------
License Setup
---------------------------------------

Please add your license key, which you've received
via email after purchasing Kirby on http://getkirby.com/buy

It is not permitted to run a public website without a
valid license key. Please read the End User License Agreement
for more information: http://getkirby.com/license

*/

c::set('license', 'K2-PRO-a1fc11fac8fe88b3f084f3303bcc2fc2');

/*

---------------------------------------
Kirby Configuration
---------------------------------------

By default you don't have to configure anything to
make Kirby work. For more fine-grained configuration
of the system, please check out http://getkirby.com/docs/advanced/options

*/

c::set('home','homepage');

c::set('debug', true);

c::set('dev_offline', false);

c::set('kirbytext.image.figure', false);

c::set('routes', array(
  array(
    'pattern' => 'amministrazione-trasparente',
    'action' => function () {
      return go(page('amministrazione-trasparente')->children()->first()->url());
    }
  ),
  array(
    'pattern' => 'segreteria-ordini/(:any)',
    'action' => function ($orderUid) {
      $ordine = page('segreteria-ordini')->children()->findBy("uid", $orderUid);
      if(!$ordine){
        return go(page('error')->url() ."/err:3387658765");
      }
      return go(page('iscrizione2')->url() ."/order:". $ordine->orderId()->value());
    }
  ),
  // array(
  //   'pattern' => 'recap-ordine-print/(:any)',
  //   'action' => function ($orderId) {
  //     return go(kirby()->urls()->assets() ."/php/recap-ordine-print.php/order:$orderId");
  //   }
  // ),
  array(
    'pattern' => 'newsletter-contacts',
    'action' => function () {
      return go(page('segreteria-contatti-newsletter')->url());
    }
  ),
  array(
    'pattern' => 'logout',
    'action'  => function() {
      if($user = site()->user()) $user->logout();
      go('/');
    }
  ),
  array(
    'pattern' => 'registro/(:any)',
    'action' => function ($articleeeeId) {
      return go(kirby()->urls()->assets() ."/php/segreteria-registro.php/articleeeeId:$articleeeeId");
    }
  ),

  // --- Utilities

  array(
    'pattern' => 'utilities/ordini-to-xls',
    'action' => function () {
      snippet("utilities-ordini-to-xls");
    }
  ),
  array(
    'pattern' => 'utilities/archive-aa-json-raw',
    'action' => function () {
      snippet("utilities-archive-aa-json-raw");
    }
  ),
  array(
    'pattern' => 'utilities/archive-aa-json-better',
    'action' => function () {
      snippet("utilities-archive-aa-json-better");
    }
  ),
));


/*

---------------------------------------
Variables
---------------------------------------

*/

$emailSignature = <<<EOD

--

Scuola
SUPERiore
di Arti Applicate
del Castello Sforzesco
EOD;

// email
c::set('email.from', 'Scuola SUPER <super@scuolaarteapplicata.it>');
c::set('emailSignature', $emailSignature);

// cart cookie ORI
// c::set("cartCookieKey", "superCartCookie");
// c::set("cartCookieDuration", 10); // minutes
// c::set("cartCookiePath", "/super/");
// c::set("cartCookieDomain", ".scuolaarteapplicata.it");

// cart cookie TEST ONLINE
// c::set("cartCookieKey", "superCartCookie");
// c::set("cartCookieDuration", 10); // minutes
// c::set("cartCookiePath", "/super");
// c::set("cartCookieDomain", ".scuolaarteapplicata.it");

// cart cookie TEST LOCAL + ONLINE
c::set("cartCookieKey", "superCartCookie");
c::set("cartCookieDuration", 10); // minutes
c::set("cartCookiePath", "/idlab-scuola");
c::set("cartCookieDomain", "localhost");




// paypal settings
// -------------------
// $mode = "sandbox";
$mode = "production";
// -------------------
$paypalSettings = [
  "sandbox" => [
    "paypalSandboxModeOn" => true,
    "paypalBusinessEmail" => "piacentini.alex-facilitator@gmail.com",
    "paypalIdentityToken" => "8fMqqcUO2s33zXN3iTiL-ySK6-abaEDPHBE3PY_vNlSPcCGeNNrRTH7bvSa",
    "paypalFormAction"    => "https://www.sandbox.paypal.com/cgi-bin/webscr",
    "paypalClientId"      => "Aa0zf0Y-LMF7mrXj1fs-y8Kh2iHo0ttPnJ3GfiEOICEPrS1nHIg5-lFpCeg1i6NAuXf2obIhInDVfpSn"
  ],
  "production" => [
    "paypalSandboxModeOn" => false,
    "paypalBusinessEmail" => "amministrazionepaypal@scuolaarteapplicata.it",
    "paypalIdentityToken" => "4BYVcX6Y_BBGJuCvtzMKhCiXBDSNQCI4k0aTCg49gcIZVF-5NOWzL65O6m8",
    "paypalFormAction"    => "https://www.paypal.com/cgi-bin/webscr",
    // "paypalClientId"      => "ATc-g53XKMvpNPFrqPiptzvlBWuAfyyvhXCj2gZCYVdFgOSM_-1Mrlp51PIrjGDPdL6jrotZmOOweW5a"
    "paypalClientId"      => "ATYxLMJFp6iYrnPrlgNUzff2WQYFlAzvghgA89uQZV7MBgFFESK-4xo6k7ToR9daj-Bcsmakafh-19Oh"
  ]
];
c::set("paypalSandboxModeOn", $paypalSettings[$mode]["paypalSandboxModeOn"]);
c::set("paypalBusinessEmail", $paypalSettings[$mode]["paypalBusinessEmail"]);
c::set("paypalIdentityToken", $paypalSettings[$mode]["paypalIdentityToken"]);
c::set("paypalFormAction",    $paypalSettings[$mode]["paypalFormAction"]);
c::set("paypalClientId",      $paypalSettings[$mode]["paypalClientId"]);


// --- Mailchimp settings

c::set("mailchimpApikey",    "bd795785ca6d8d222a5f951b93fd1c7f-us1");
c::set("mailchimpListName",  "Interessati");
c::set("mailchimpListId",    "d3ac6b5495");
c::set("mailchimpListWebId", 1215357);


// --- GA

c::set('plugin.googleAnalytics', true);
c::set('plugin.googleAnalytics.trackingId', 'UA-131142587-1');
// c::set('plugin.googleAnalytics.anonymizeIp', false);

/*------------- fosca vvv
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131142587-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131142587-1');
</script>
-------------*/

/*

---------------------------------------
Hooks
---------------------------------------

*/

kirby()->hook('panel.page.create', function($page){

  // Incremental ids for corso.yml
  // https://forum.getkirby.com/t/auto-increment-ids-in-kirby/1899
  // update the created page with the ID after creating the page
  if($page->template() == "corso"){
    // get the actual id from an txt file in the kirby root folder
    $corsoId = intval(file_get_contents(kirby()->roots->index().'/corsoId.txt'));
    $corsoId++;
    // save the incremented id
    file_put_contents(kirby()->roots->index().'/corsoId.txt', $corsoId, LOCK_EX);
    // update the created kirby page
    $page->update(array('corsoId' => "corso-$corsoId"));
  }

});

/*

---------------------------------------
Plugins
---------------------------------------

*/

// c::set('uniform.language', 'it');

c::set('widget.backup.overwrite', false);
c::set('widget.backup.include_site', true);


/*

---------------------------------------
Users
---------------------------------------

*/

// via https://getkirby.com/docs/cookbook/authentication
c::set('roles', array(
  array(
    'id'      => 'admin',
    'name'    => 'Admin',
    'default' => true,
    'panel'   => true
  ),
  array(
    'id'      => 'editor',
    'name'    => 'Editor',
    'panel'   => true
  ),
  array(
    'id'      => 'maintenanceNormalUser',
    'name'    => 'Maintenance Normal User',
    'panel'   => true
  )
  // the two above are default
  // array(
  //   'id'      => 'studente',
  //   'name'    => 'Studente',
  //   'panel'   => false
  // )
));


