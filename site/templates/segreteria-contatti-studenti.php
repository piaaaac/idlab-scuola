<?php
snippet('logincheck-block-non-admin');
setlocale(LC_MONETARY, 'it_IT');

$sortString = param("sort");
$sortdir = param("direction");
$allOrdini = page("segreteria-ordini")->children();
$studenti = array();

foreach ($allOrdini as $ordine) {
  // $cognomeNome = $ordine->form_cognome()->value() ." ". $ordine->form_nome()->value();
  $keyStudente = strtolower($ordine->form_cognome()->value()) ."-". 
    strtolower($ordine->form_nome()->value()) ."-". 
    strtolower($ordine->form_email()->value()) ."-". 
    strtolower($ordine->form_telefono()->value());
  if(!array_key_exists($keyStudente, $studenti)){
    // create
    $ordini = new Pages();
    $ordini->add($ordine);
    $studenti["$keyStudente"] = array(
      "cognome"           => $ordine->form_cognome()->value(),
      "nome"              => $ordine->form_nome()->value(),
      "email"             => $ordine->form_email()->value(),
      "telefono"          => $ordine->form_telefono()->value(),
      "ordini"            => $ordini
    );
  } else {
    // update
    $studenti["$keyStudente"]["ordini"]->add($ordine);
  }
}

if(!$sortString) $sortString = "cognome";
$sortDirection = SORT_ASC;
if($sortString){
  if($sortdir == "desc"){
    $sortDirection = SORT_DESC;
  }
  $sortmap = array();
  foreach ($studenti as $key => $studente){
    $sortmap[$key] = $studente[$sortString];
  }
  array_multisort($sortmap, $sortDirection, $studenti);
}

// a::show($studenti);
// exit();

/*
$studenti = {
  "nome cognome": {
    nome,
    cognome,
    telefono,
    email,
    ordini: [
      ordineKYGKGkhGJKH,
      ordineKYGKGkhGJKH,
      ordineKYGKGkhGJKH,
      ordineKYGKGkhGJKH
    ]
  }
}
*/

function totalOrdini($ordini){
  $total = 0;
  $totalPayed = 0;
  foreach ($ordini as $o) {
    $total += (float)$o->importoTotale()->value();
    if($o->pagamentoOk()->value() === "1"){
      $totalPayed += (float)$o->importoTotale()->value();
    }
  }
  return [
    "total"       => $total,
    "totalAperto" => $total - $totalPayed,
    "totalPayed"  => $totalPayed
  ];
}

$cognomeLinkHref = $page->url() ."/sort:cognome/direction:". r(($sortString == "cognome" && $sortDirection != SORT_DESC), "desc", "asc");
$cognomeLinkText = "COGNOME". r(($sortString == "cognome"), r(($sortDirection != SORT_DESC), " &uarr;", " &darr;"), "");

$nomeLinkHref = $page->url() ."/sort:nome/direction:". r(($sortString == "nome" && $sortDirection != SORT_DESC), "desc", "asc");
$nomeLinkText = "NOME". r(($sortString == "nome"), r(($sortDirection != SORT_DESC), " &uarr;", " &darr;"), "");

$emailLinkHref = $page->url() ."/sort:email/direction:". r(($sortString == "email" && $sortDirection != SORT_DESC), "desc", "asc");
$emailLinkText = "EMAIL". r(($sortString == "email"), r(($sortDirection != SORT_DESC), " &uarr;", " &darr;"), "");

?>

<?php snippet('header', ["hideMenu" => true]) ?>

  <main class="main pagine-segreteria pt-0 pb-0" role="main" id="pagina-segreteria-contatti-studenti">

    <div class="container-fluid super-cont s-c-admin table-wrapper">
      <table class="outer mr-5"><tr>

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">
                <a href="<?= $cognomeLinkHref ?>"><?= $cognomeLinkText ?></a>
              </th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3"><?= $s["cognome"] ?>&nbsp;</td></tr>
              <?php endforeach ?>
            </table>
          </td>

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">
                <a href="<?= $nomeLinkHref ?>"><?= $nomeLinkText ?></a>
              </th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3"><?= $s["nome"] ?>&nbsp;</td></tr>
              <?php endforeach ?>
            </table>
          </td>

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">
                <a href="<?= $emailLinkHref ?>"><?= $emailLinkText ?></a>
              </th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3"><?= $s["email"] ?>&nbsp;</td></tr>
              <?php endforeach ?>
            </table>
          </td>  

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">TELEFONO</th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3"><?= r($s["telefono"], $s["telefono"], "-") ?></td></tr>
              <?php endforeach ?>
            </table>
          </td>  

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3 text-right">ORDINI</th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3 text-right">
                  <span class="font-color-red font-sans-ss">
                    <?php
                      $n = count($s["ordini"]->filterBy('pagamentoOk', '0'));
                      if($n) echo "+$n";
                    ?>
                  </span>
                  <span class="font-color-black font-sans-ss ml-2">
                    <?= count($s["ordini"]->filterBy('pagamentoOk', '1')) ?>
                  </span>
                </td></tr>
              <?php endforeach ?>
            </table>
          </td>  

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3 text-right">TOT PAGATO</th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3 text-right">
                  <?= totalOrdini($s["ordini"])["totalPayed"] > 0
                    ? money_format('%!.2n', totalOrdini($s["ordini"])["totalPayed"]) ." &euro;"
                    : "-"
                  ?>
                </td></tr>
              <?php endforeach ?>
            </table>
          </td>  

          <td>
            <table class="inner">
              <tr><th class="text-nowrap font-color-red header pb-2 text-right">ORDINI APERTI</th></tr>
              <?php foreach($studenti as $s): ?>
                <tr><td class="text-nowrap font-color-red font-sans-ss data text-right">
                  <?= totalOrdini($s["ordini"])["totalAperto"] > 0
                    ? money_format('%!.2n', totalOrdini($s["ordini"])["totalAperto"]) ." &euro;"
                    : "-"
                  ?>
                </td></tr>
              <?php endforeach ?>
            </table>
          </td>  

          <td>
            <table class="spacer"><tr><th class="text-nowrap px-4">&nbsp;</th></tr></table>
          </td>  

      </tr></table>

      <?php if(count($studenti) == 0): ?>
        Nessuno studente iscritto.
      <?php endif ?>
        
    </div>
  </main>

<?php snippet('footer', ["hideFooter" => true]) ?>


