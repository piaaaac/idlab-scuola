<?php
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');

$orderId = param("order");
if(!$orderId){
  echo "Pagina non trovata: nessun numero ordine ricevuto.";
  exit();
}
$page = page("segreteria-ordini")->children()->findBy("orderId", $orderId);
if(!$page){
  echo "Ordine non trovato: $orderId";
  exit();
}
$fields = $page->content()->toArray();
$formData = formDataFromPage($page);

/* ORDINI PROVE
00000239m3dml55x  New
000002386lr36obn  Pagato
0000017246nunt6z  Con uno sconto
00000166gkhzs5ok  Oneri Segreteria
0000023028jbzgu0  Bonifico
00000037mlfijcpm  Aziendale
*/

if($cartArray = json_decode($page->cartArray()->value())){
  // a::show($cartArray);
} else {
  echo "Error: no cart array data is present in order $orderId";
  exit();
}

$annoFormativoOrdine = annoFormativo($cartArray[0]->turno->dataInizio, $cartArray[0]->corso->corsoId);
foreach($cartArray as $item){
  $tmpAF = annoFormativo($item->turno->dataInizio, $item->corso->corsoId);
  if($tmpAF !== null && $tmpAF !== $annoFormativoOrdine){
    $annoFormativoOrdine = "Multipli";
  }
}

?>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="description" content="<?= $site->description()->html() ?>">

  <?= css('assets/css/index-200605.css') ?>
  <!-- <link rel="stylesheet" type="text/css" href="<?= $site->url() ?>/assets/css/index.css"> -->

  <?php snippet('load-scripts', ["loadFormScripts" => false]) ?>
</head>
<body>
  <main id="print">
    <div class="container-fluid super-cont">

      <!-- TITOLO -->

      <div class="row">
        <div class="col-12 my-5">
          <p class="my-0 font-sans-ll">Ordine <?= $page->orderId()->value() ?></p>
          <p class="my-0">A.F.: <?= $annoFormativoOrdine ?></p>
        </div>
      </div>

      <!-- DATI ANAGRAFICI -->

      <div class="row">
        <div class="col-12">
          <p>DATI ANAGRAFICI</p>
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-4">
          <p>
            <em>Cognome</em>
            <?= $formData["form_cognome"] ?>
          </p><p>
            <em>Nome completo</em>
            <?= $formData["form_nome"] ?>
          </p><p>
            <em>Luogo e data di nascita</em>
            <?= $formData["form_luogoNascita"] ?> 
            (<?= $formData["form_provinciaNascita"] ?>)
            &mdash;
            <?= $formData["form_dataNascita"] ?>
          </p><p>
            <em>Cittadinanza</em>
            <?= $formData["form_cittadinanza"] ?>
          </p><p>
            <em>Codice fiscale</em>
            <?= $formData["form_codiceFiscale"] ?>
          </p>
        </div>

        <div class="col-4">
          <p>
            <em>Residenza</em>
            <?= $formData["form_residenzaCivico"] ?>,
            <?= $formData["form_residenzaIndirizzo"] ?>
          </p><p>
            <em>CAP e citt&agrave;</em>
            <?= $formData["form_residenzaCap"] ?>
            &mdash;
            <?= $formData["form_residenzaLuogo"] ?>
            (<?= $formData["form_residenzaProvincia"] ?>)
          </p><p>
            <em>Telefono</em>
            <?= $formData["form_telefono"] ?>
          </p><p>
            <em>Email</em>
            <?= $formData["form_email"] ?>
          </p>
        </div>
        
        <div class="col-4">
          <?php if($formData["fatturazioneAziendale"]): ?>
            <p>
              <em>Fatturazione aziendale</em>
              <?= $formData["form_aziendaRagioneSociale"] ?>
              <br /><?= $formData["form_aziendaCivico"] ?>,
              <?= $formData["form_aziendaIndirizzo"] ?>
              <br /><?= $formData["form_aziendaCap"] ?>
              <?= $formData["form_aziendaLuogo"] ?>
              (<?= $formData["form_aziendaProvincia"] ?>)
            </p><p>
              <em>Codice fiscale</em>
              <?= $formData["form_aziendaCodiceFiscale"] ?>
            </p><p>
              <em>Partita IVA</em>
              <?= $formData["form_aziendaPartitaIva"] ?>
            </p>
          <?php else: ?>
            <p>
              <em>Fatturazione aziendale</em>
              &mdash;
            </p>
          <?php endif ?>

        </div>
      </div>

      <!-- CORSI ACQUISTATI -->

      <div class="row mt-5">
        <div class="col-12 d-flex justify-content-between">
          <p>
            ARTICOLI ACQUISTATI (<?=count($cartArray)?>)
          </p>
          <!-- <p>Anno Formativo: <?= $annoFormativoOrdine ?></p> -->
        </div>
        <div class="col-12">
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-2"><em>Anno formativo</em></div>
        <div class="col-2"><em>Tipologia</em></div>
        <div class="col-3"><em>Corso</em></div>
        <div class="col-3"><em>Turno/Anno</em></div>
        <div class="col-2"><em>Costo e sconti</em></div>
      </div>
      <div class="row">
        <div class="col-12"><hr class="my-2" /></div>
      </div>

      <?php foreach($cartArray as $item): ?>
        <div class="row">
          <div class="col-2">
            <?= annoFormativo($item->turno->dataInizio, $item->corso->corsoId) ?>
          </div>
          <div class="col-2">
            <?php
            $corso = page("corsi")->children()->findBy("corsoId", $item->corso->corsoId);


            // -------------
            // $titoliTipiCorso = [
            //   'serale240' => page("corsi")->titoloCorsiAnnuali()->value(),
            //   'serale60' => page("corsi")->titoloCorsi60()->value(),
            //   'pomeridiano30' => page("corsi")->titoloCorsi30()->value(),
            //   'speciali' => page("corsi")->titoloCorsiSpeciali()->value(),
            //   'officine' => page("corsi")->titoloCorsiOfficine()->value(),
            // ];
            // echo $titoliTipiCorso[$corso->tipo()->value()]; 
            // -------------
            $tipo = $corso->tipo()->value();
            $titleTipo = "";
            if (!$titleTipo && page("corsi")->dynamicCol1ATipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol1ATitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol2ATipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol2ATitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol3ATipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol3ATitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol4ATipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol4ATitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol1BTipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol1BTitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol2BTipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol2BTitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol3BTipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol3BTitle()->value();
            if (!$titleTipo && page("corsi")->dynamicCol4BTipo()->value() == $tipo) 
              $titleTipo = page("corsi")->dynamicCol4BTitle()->value();
            echo $tipo . ($titleTipo ? " ($titleTipo)" : "");
            // -------------


            $turno = page("corsi/". $corso->uid() ."/". $item->turno->uid);
            // a::show($turno);
            ?>
          </div>
          <div class="col-3">
            <?= $item->corso->title ?>
          </div>
          <div class="col-3">
            <?= $item->turno->nome ?>
            <em class="font-color-black">
              <?= turnoDates($turno, "") ?>
            </em>
          </div>
          <div class="col-2">
            <?php

            if(isset($item->sconto)){
              echo $item->turno->costo;
              echo " - ";
              echo $item->sconto->amt;
              echo " = ";
              echo "&euro; ". $item->costoFinale;
              echo "<em class=\"font-color-black\">(sconto: ". $item->sconto->description .")</em>";
            } else {
              echo "&euro; ". $item->costoFinale;
            }

            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12"><hr class="my-2" /></div>
        </div>
      <?php endforeach ?>

      <!-- ORDINE -->

      <div class="row mt-5">
        <div class="col-12">
          <p>ORDINE</p>
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-4">
          <p>
            <em>Importo totale ordine</em>
            <?= "&euro; ". $page->importoTotale()->value() ?>
          </p><p>
            <em>Pagamento</em>
            <?= $page->pagamentoOk()->value() 
                  ? $page->pagamentoMetodo()->value() 
                  : "Ordine non ancora pagato.";
            ?>
          </p><p>
            <em>Rif. pagamento</em>
            <?= $page->pagamentoRiferimento()->value() 
                  ? $page->pagamentoRiferimento()->value() 
                  : "&mdash;";
            ?>
          </p>
        </div>
        <div class="col-4">
          </p><p>
            <em>Data creazione</em>
            <?= dateTimeFormatted($page->form_dataOraOrdine()->value()) ?>
          </p><p>
            <em>Data attivazione ordine</em>
            <?php if($d = $page->dataOraVerifica()->value()){ echo dateTimeFormatted($d); } else { echo "—"; } ?>
          </p><p>
            <em>Data pagamento</em>
            <?php if($d = $page->dataOraPagamento()->value()){ echo dateTimeFormatted($d); } else { echo "—"; } ?>
        </div>
        <div class="col-4">
          <p>
            <em>Note della segreteria</em>
            <?= $page->note()->value() ? $page->note()->value() : "&mdash;" ?>
          </p>
        </div>
      </div>


    </div>
  </main>
  
</body>
</html>
