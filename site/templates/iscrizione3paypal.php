<?php
// as requested by paypal's guidelines
?>

<p>
  Thank you for your payment. Your transaction has been completed, and a receipt for your purchase has been emailed to you. Log into your PayPal account to view transaction details.
</p>
<p>
  You are being redirected to a confirmation page.
</p>