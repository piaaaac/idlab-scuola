<?php snippet('header') ?>

  <main class="main" role="main" id="pagina-amministrazione-trasparente">

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col breadcrumb-title">
          <span class="font-super-ll"><?=page("amministrazione-trasparente")->title()->value()?></a>
        </div>
      </div>
    </div>

    <div class="my-2"><br /></div>
    
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-sm-4">
          <?php foreach($page->siblings()->visible() as $subPage): ?>
            <a href="<?= $subPage->url() ?>" class="bordered-list<?= r($subPage->isOpen(), ' active') ?>">
              <?= $subPage->title()->html() ?>
            </a>
          <?php endforeach ?>    
        </div>
        <div class="col-sm-8">

          <hr class="d-block d-sm-none my-4" />

          <div class="amm-transp-body">
            <?= $page->text()->kirbytext() ?>
          </div>
        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>
