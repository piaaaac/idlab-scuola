<?php
$p1 = page('storia');
$p2 = page('manifesto');
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row mb-4">
        <div class="col-md-6">
          <div class="links-gold mb-4"><?= $page->testoChiSiamoCol1()->kirbytext() ?></div>
        </div>
        <div class="col-md-6">
          <div class="links-gold mb-4"><?= $page->testoChiSiamoCol2()->kirbytext() ?></div>
        </div>
      </div>
    </div>

    <div class="my-1"><br /></div>

    <div class="bg-white mb-5 pb-2">
      <div class="container-fluid super-cont">
        <div class="row">
          <div class="col-md-6 my-4">
            <h1 class="pb-3">La nostra Storia</h1>
            <div class="font-sans-lll font-color-red mt-4 mb-2">
              <?= $page->testoIntroCol1()->kirbytext() ?>
            </div>
            <a class="btn btn-large btn-primary reduce-xs font-sans-l mt-3 red" href="<?= $p1->url() ?>" role="button">CONTINUA</a>

          </div>
          <div class="col-md-6 my-4">
            <h1 class="pb-3">I nostri Valori</h1>
            <div class="font-sans-lll font-color-red mt-4 mb-2">
              <?= $page->testoIntroCol2()->kirbytext() ?>
            </div>
            <a class="btn btn-large btn-primary reduce-xs font-sans-l mt-3 red" href="<?= $p2->url() ?>" role="button">LEGGI</a>

          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid super-cont">
      <div class="row mb-4">
        <div class="col-12 mb-4 pb-2"><h1>Chi siamo</h1></div>
        <?php 
          snippet('showcase-docenti', [
            "docenti"       => $page->children()->visible()->filterBy("tipo", "docente"),
            "itemClass"     => "col-6 col-sm-4 col-md-3 col-lg-2",
          ]); 
        ?>
      </div>

      <div class="my-4 py-2"><br /></div>

      <div class="row">
        <div class="col-12 mb-2"><h1>Staff</h1></div>
        <?php 
          snippet('showcase-docenti', [
            "docenti"       => $page->children()->visible()->filterBy("tipo", "staff"),
            "itemClass"     => "col-6 col-sm-4 col-md-3 col-lg-2",
          ]); 
        ?>
      </div>

      <?php 
        $cd = $page->children()->visible()->filterBy("tipo", "consiglio");
        if($cd->count() > 0): ?>
  
        <div class="my-4 py-2"><br /></div>

        <div class="row">
          <div class="col-12 mb-2"><h1>Consiglio direttivo</h1></div>
          <?php snippet('showcase-docenti', [
            "docenti"       => $cd,
            "itemClass"     => "col-6 col-sm-4 col-md-3 col-lg-2",
          ]); ?>
        </div>
      <?php endif ?>

    </div>

  </main>

<?php snippet('footer') ?>