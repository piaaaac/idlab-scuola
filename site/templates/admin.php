<?php

if($user = $site->user() and $user->hasRole('admin')){
  // s::set($adminHeaderOn, true); // on localhost gives Undefined variable: adminHeaderOn
  s::set("adminHeaderOn", true); // unused?
} else {
  go('/');
}
$url = urldecode($_GET['url']);

?>

<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>

  <meta charset="utf-8">
  <!-- <meta name="viewport" content="width=device-width,initial-scale=1.0"> -->
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
  <?php snippet('load-scripts') ?>
  <?= css('assets/css/index-200605.css') ?>
  <?php include_once(kirby()->roots()->snippets() .'/load-cart-js-functions.php') ?>

  <script>
    var adminMenuIsOpen = false;
    // https://www.dyn-web.com/tutorials/iframes/refs/iframe.php  
    // https://www.dyn-web.com/tutorials/iframes/refs/parent.php
    function addSpeseSegreteria(){
      var ifrm = document.getElementById('ifrm');
      var innerWin = ifrm.contentWindow; // reference to iframe's window
      var innerDoc = ifrm.contentDocument? ifrm.contentDocument: ifrm.contentWindow.document;
      innerWin.handleEditCart('add', 'corso-70~spese-amministrative');
      toggleAdminMenu(false);
    }

    function toggleAdminMenu(openIt){
      if(openIt === true){ adminMenuIsOpen = true; }
      else if (openIt === false){ adminMenuIsOpen = false; }
      else{
        if(adminMenuIsOpen === false){ adminMenuIsOpen = true; }
        else { adminMenuIsOpen = false; }
      }
      $("#pagina-admin .hamburger").toggleClass("is-active", adminMenuIsOpen);
      $("#admin-menu").toggleClass("open", adminMenuIsOpen);
    }
  </script>

</head>
<body id="bootstrap-overrides">
  <main class="main" role="main" id="pagina-admin">
  
    <header class="fixed-top d-flex justify-content-between align-items-center">
      <div class="left d-flex justify-content-start align-items-center">

        <button class="hamburger hamburger--slider mt-2 mr-2" type="button" onclick="javascript:toggleAdminMenu();">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>
        <a href="javascript:toggleAdminMenu();" class="font-sans-ss d-none d-sm-block mt-1 mr-5">
          SEGRETERIA</a>

<!-- 
        <a href="<?= $site->url() ."/panel" ?>" class="font-sans-ss d-none d-sm-block mr-3">
          Panel home</a>

        <a href="<?= $site->url() ."/panel/pages/segreteria-ordini/edit" ?>" class="font-sans-ss d-none d-sm-block mr-3">
          Panel ordini</a>
 -->
        
        <a href="javascript:addSpeseSegreteria()" class="btn btn-small btn-primary black-light d-none d-sm-block mr-3">
          <i class="fas fa-shopping-cart d-inline-block ml-2"></i>
          + 20 &euro;
        </a>
        <a href="<?= page('segreteria-conferma-pagamenti')->url() ?>" class="btn btn-small btn-primary black-light d-none d-sm-block mr-3">
          <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
          Ordini</a>
        <a href="<?= page('segreteria-iscrizioni-per-corso')->url() ?>" class="btn btn-small btn-primary black-light d-none d-md-block mr-3">
          <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
          Iscrizioni</a>
        <a href="<?= page('segreteria-contatti-studenti')->url() ?>" class="btn btn-small btn-primary black-light d-none d-md-block mr-3">
          <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
          Studenti</a>

      </div>
      <div class="right d-flex justify-content-end align-items-center">

        <span class="font-sans-ss font-color-black50">
          <?= $site->user()->firstname() ." ". $site->user()->lastname() ?></span>

        <a href="<?= $site->url() ."/logout" ?>" class="font-sans-ss ml-3">
          Logout</a>

      </div>
    </header>

  
  </main>


  
  <!-- scroll problel on iphone -->
  <!-- <iframe id="ifrm" src="<?= $url ?>"></iframe> -->

   <!--  
   -->
  <div class="scroll-wrapper">
    <iframe id="ifrm" src="<?= $url ?>"></iframe>
  </div>


  <div id="admin-menu" class="pb-5">

    <span class="bordered-list font-color-black30">OPERAZIONI SEGRETERIA</span>
    <a href="javascript:addSpeseSegreteria()" class="bordered-list no-arrow">
      <i class="fas fa-shopping-cart d-inline-block mr-1"></i>
      Aggiungi spese amministrative</a>
    <a href="<?= page('segreteria-conferma-pagamenti')->url() ?>" class="bordered-list no-arrow">
      <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
      Gestione ordini</a>
    <a href="<?= page('segreteria-contatti-studenti')->url() ?>" class="bordered-list no-arrow">
      <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
      Contatti studenti</a>
    <a href="<?= page('segreteria-contatti-newsletter')->url() ?>" class="bordered-list no-arrow">
      <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
      Contatti newsletter</a>
    <a href="<?= page('segreteria-iscrizioni-per-corso')->url() ?>" class="bordered-list no-arrow">
      <i class="fas fa-pencil-alt d-inline-block mr-1"></i>
      Iscrizioni suddivise per corso</a>
<!--     <a href="<?= page('segreteria-contatti-studenti')->url() ?>" class="bordered-list no-arrow disabled">
      <i class="far fa-clock d-inline-block mr-1"></i>
      Contatti studenti</a>
 -->
    <br />
    
    <span class="bordered-list font-color-black30">LINK UTILI</span>
    <a href="<?= page('corsi')->url() ?>" class="bordered-list no-arrow">
      <i class="fas fa-globe-africa d-inline-block mr-1"></i>
      Sito Super / Corsi</a>
    <a href="<?= $site->url() ."/panel" ?>" class="bordered-list no-arrow">
      <i class="fas fa-sliders-h d-inline-block mr-1"></i>
      Panello</a>
    <a href="<?= $site->url() ."/panel/pages/segreteria-ordini/edit" ?>" class="bordered-list no-arrow">
      <i class="fas fa-sliders-h d-inline-block mr-1"></i>
      Panello / Ordini</a>
    <a href="<?= $site->url() ."/panel/pages/corsi/edit" ?>" class="bordered-list no-arrow">
      <i class="fas fa-sliders-h d-inline-block mr-1"></i>
      Panello / Corsi</a>

  </div>

</body>
</html>

