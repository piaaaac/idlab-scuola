<?php
$items = $page->items()->toStructure();
$allTags = [];
foreach ($items as $item) {
  foreach ($item->tags()->split() as $tag) {
    if (!in_array($tag, $allTags)) {
      $allTags[] = $tag;      
    }
  }
}
sort($allTags);
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-sm-8">
          <div class="links-gold text-sans-s pr-3 pr-sm-5">
            <?= $page->testo()->kirbytext() ?>
          </div>
        </div>
      </div>
    </div>

    <div class="container-fluid super-cont lapis-videos mt-5">
      <div class="row">
        <div class="col-12">

          <div class="header-line-1 d-flex justify-content-between">
            <h2 class="section-title font-sans-m">VIDEO IN VETRINA</h2>
            <div class="search-ui">
              <input class="" type="search" placeholder="Cerca per docente o parola chiave" aria-label="Search" id="input-search-lapis">
              <button class="btn search-icon" onclick="handleSearchIconClick();"><i class="fas fa-search"></i></button>
            </div>
          </div>

          <div class="header-line-2 collapsed">
            <div class="tags-wrapper">
              <?php foreach ($allTags as $tag): ?>
                <a class="tag font-sans-s" onclick="filterByTag('<?= $tag ?>');"><?= $tag ?></a>
              <?php endforeach ?>
            </div>
            <div class="toggle-tags" onclick="toggleTags();"></div>
          </div>

        </div>
      </div>
      <div class="row">
        <?php foreach ($items as $item): ?>
          <?php 
          $dataTags = [];
          foreach ($item->tags()->split() as $tag) {
            $dataTags[] = "#$tag#";
          }
          ?>


          <div class="item-wrapper col-sm-6 col-lg-4 align-self-stretch mb-4 mt-2" data-tags="<?= implode(", ", $dataTags) ?>">
            
            <div class="item d-flex flex-column bg-white mb-4">
              <div class="img ratio">
                <div class="clip" data-youtube="<?= $item->youtube()->value() ?>">
                  <img class="w-100" src="<?= $item->image()->toFile()->url() ?>" />
                </div>
              </div>
              <div class="texts flex-grow-1 d-flex flex-column justify-content-between p-4">
                <p class="title font-sans-m" data-youtube="<?= $item->youtube()->value() ?>"><?= $item->title()->value() ?></p>
                <div class="item-footer d-flex justify-content-between align-items-end">
                  <p class="tags">
                    <?php foreach ($item->tags()->split() as $tag): ?>
                      <a class="tag font-sans-s" onclick="filterByTag('<?= $tag ?>');"><?= $tag ?></a>
                    <?php endforeach ?>
                  </p>
                  <a class="share-video" onclick="openShare('<?= $item->youtube()->value() ?>');"><img src="<?= $site->url() ?>/assets/images/share-video.svg" /></a>
                </div>
              </div>
            </div>

          </div>
        <?php endforeach ?>
      </div>
    </div>

  </main>

  <div id="modal-share" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        
        <header class="d-flex justify-content-between align-items-center">
          <p class="font-sans-m">Condividi questo video</p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </header>
          
        <div class="modal-body">

          <input id="video-link" type="text" value="http://youtu.be" />
          
          <div class="bottom d-flex justify-content-between mb-5">
            <a class="copy-video-link cursor-pointer font-sans-s font-color-blue" 
               data-clipboard-target="#video-link"
               onclick="confirmCopy();"
            >COPIA IL LINK</a>
            <!--  
            <div>
              <a class="share-icon" onclick="copyVideoLink();">FB</a>
              <a class="share-icon" onclick="copyVideoLink();">TW</a>
            </div>
            -->
          </div>

        </div>
      </div>
    </div>
  </div>

  <script>

    // ------------------
    // vars & initialize
    // ------------------

    var currentVisible = null;
    var currentTagFilter = null;
    var currentSearch = null;    
    var defaultSectionTitle = "TUTTI I VIDEO";
    var tagsAreCollapsed = true;

    new ClipboardJS('.copy-video-link');

    // ------------------
    // events
    // ------------------

    $("[data-youtube]").click(function(event) {
      openVideo(event);
    });

    $('#modal-share').on('show.bs.modal', function (event) {
      setTimeout(function() {
        var input = $('input#video-link');
        console.log(input);
        input[0].select();
        input[0].focus();
      }, 500);
    });
    $("#input-search-lapis")
      .focus(function() {
        $(".search-ui").addClass("active");
      })
      .blur(function() {
        $(".search-ui").removeClass("active");
      })
      .keypress(function(event) {
        if (event.keyCode == 13) {
          handleSearchIconClick();
        }
      });

    // ------------------
    // functions
    // ------------------

    function toggleTags () {
      tagsAreCollapsed = !tagsAreCollapsed;
      if (tagsAreCollapsed) {
        h = 40;
      } else {
        var h = $(".tags-wrapper").height() + 40;
      }
      $(".header-line-2").css("height", h);
      $(".header-line-2").toggleClass("collapsed", tagsAreCollapsed);
    }

    function handleSearchIconClick () {
      var value = $("#input-search-lapis").val().trim();
      var searchIsActive = $(".search-ui").hasClass("active");
      if (searchIsActive || value != "") {
        // search active: do search
        $(".search-ui").removeClass("active");
        $("#input-search-lapis").blur();
        if (value != "") {
          filterSearch(value);
          $("#input-search-lapis").val("");
        }
      } else {
        // search not active: activate and focus
        resetFilters();
        $(".search-ui").addClass("active");
        $("#input-search-lapis").focus();
      }
    }

    function confirmCopy () {
      $('.copy-video-link').addClass("confirm");
      setTimeout(function() {
        $('.copy-video-link').removeClass("confirm");
      }, 1500);
    }

    function openShare (ytid) {
      var modal = $('#modal-share');
      var input = modal.find('input#video-link');
      input.val("https://youtu.be/"+ ytid);
      $('#modal-share').modal('show');
    }

    function updateUI () {
      if (currentSearch === null && currentTagFilter === null) {
        $(".section-title").text(defaultSectionTitle);
      } else {
        markup = (currentVisible > 0) ? currentVisible : "NESSUN";
        markup += " VIDEO&nbsp;&nbsp;/&nbsp;&nbsp;";
        markup += currentTagFilter 
          ? "<span class='font-color-blue font-sans-m'>#"+ currentTagFilter +"</span>"
          : currentSearch;
        markup += " <a class='cursor-pointer font-sans-m' onclick='resetFilters();'>(&times;)</a>";
        $(".section-title").html(markup);
      }
      if (currentSearch === null) {
        $(".search-ui").removeClass("active");
      }
    }
    
    function resetFilters () {
      $(".item-wrapper").show();
      currentTagFilter = null;
      currentVisible = null;
      currentSearch = null;
      updateUI();
    }

    function filterByTag (tag) {
      resetFilters();
      $(".item-wrapper").hide();
      $(".item-wrapper[data-tags*='#"+ tag +"#']").show();
      currentTagFilter = tag;
      currentVisible = $(".item-wrapper[data-tags*='#"+ tag +"#']").length;
      currentSearch = null;
      updateUI();
    }

    function filterSearch (text) {
      resetFilters();
      var searchText = cleanForComparison(text);
      console.log(text)
      var matches = 0;
      $(".item-wrapper").hide();
      $(".item-wrapper").each(function(i, el) {
        var searchIn = $(el).find(".texts").text();
        searchIn = cleanForComparison(searchIn);
        if (searchIn.indexOf(searchText) !== -1) {
          $(el).show();
          matches++;
        }
      });
      currentSearch = text;
      currentVisible = matches;
      currentTagFilter = null;
      updateUI();
    }

    function openVideo (e) {
      BigPicture({
        el: e.target,
        ytSrc: e.target.dataset.youtube,
      })
    }

    function cleanForComparison (str) {
      return str.toLowerCase()
        .replace(/[ÀÁÂÃÄÅàáâãäå]/g,"a")
        .replace(/[ÈÉÊËèéêë]/g,"e")
        .replace(/[ÌÍÏìíï]/g,"i")
        .replace(/[öóòÖÓÒ]/g,"o")
        .replace(/[üúùÜÚÙ]/g,"u")
        .replace(/[^a-z0-9]/gi,''); // final clean up
    }
  </script>

<?php snippet('footer') ?>