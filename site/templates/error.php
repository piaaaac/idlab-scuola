<?php
$messages = [
  "3387658765" => "Ordine non trovato.",
  "3316516156" => "L'ordine non è più valido (Corso non disponibile).",
  "3316516157" => "L'ordine non è più valido (Turno non disponibile).",
  "3378923729" => "Ordine non trovato.",
  "3378873643" => "Data della news assente o non corretta (Salvare nuovamente la news).",
  "3378978979" => "Il formato della data (\$dateStr) non è valida.",
];

function randomIcon(){
  $iconNames = [
    "bullhorn",
    "bullhorn",
    "glass-martini-alt",
    "grin-hearts",
    "thumbs-up",
    "crow"];
  $i = rand(0, count($iconNames) - 1);
  return $iconNames[$i];
}
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row mb-4">
        <div class="col">

          <div class="font-sans-s mb-3">
            <?php if(param("err")): ?>
              <?= $messages["".param("err")] ?>
            <?php else: ?>
              <?= $page->intro()->kirbytext() ?>
            <?php endif ?>
          </div>

          <div class="font-sans-s mb-3">
            <i class="fas fa-<?= randomIcon(); ?> mr-3"></i>
            <i class="fas fa-<?= randomIcon(); ?> mr-3"></i>
            <i class="fas fa-<?= randomIcon(); ?> mr-3"></i>
          </div>
        
          <div class="font-sans-s mb-3">
            <?= $page->text()->kirbytext() ?>
          </div>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>