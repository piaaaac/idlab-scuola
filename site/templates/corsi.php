<?php
// $stannoPerIniziare = page('corsi')->children()->visible()->chunk(2)->first();

// $stannoPerIniziare = new Pages();
// $stannoPerIniziare->add(page('corsi/vetrata-abc'));
// $stannoPerIniziare->add(page('corsi/picturebooks-corso-di-specializzazione-di-50-ore'));

$stannoPerIniziare = [[
  // "entroIl" => "entro il 1 sett 2018",
  "entroIl" => "",
  "corso" => page('corsi/'. $page->staPerIniziare1()->value())
], [
  // "entroIl" => "entro il 18 ott 2018",
  "entroIl" => "",
  "corso" => page('corsi/'. $page->staPerIniziare2()->value())
]];
$colors = [
  "capolettera" => [
    "normal"    => "font-color-blue",
    "special"   => "font-color-gold"
  ],
  "text" => [
    "normal"    => "font-color-black",
    "special"   => "font-color-gold"
  ],
  "button" => [
    "normal"    => "font-color-blue",
    "special"   => "font-color-gold"
  ],
];

$corsi = page('corsi')->children()->visible(); 
// $corsi = $page->children()->visible(); 

$col1ATipo = $page->dynamicCol1ATipo()->isNotEmpty() ? $page->dynamicCol1ATipo()->value() : null;
$col2ATipo = $page->dynamicCol2ATipo()->isNotEmpty() ? $page->dynamicCol2ATipo()->value() : null;
$col3ATipo = $page->dynamicCol3ATipo()->isNotEmpty() ? $page->dynamicCol3ATipo()->value() : null;
$col4ATipo = $page->dynamicCol4ATipo()->isNotEmpty() ? $page->dynamicCol4ATipo()->value() : null;
$col1BTipo = $page->dynamicCol1BTipo()->isNotEmpty() ? $page->dynamicCol1BTipo()->value() : null;
$col2BTipo = $page->dynamicCol2BTipo()->isNotEmpty() ? $page->dynamicCol2BTipo()->value() : null;
$col3BTipo = $page->dynamicCol3BTipo()->isNotEmpty() ? $page->dynamicCol3BTipo()->value() : null;
$col4BTipo = $page->dynamicCol4BTipo()->isNotEmpty() ? $page->dynamicCol4BTipo()->value() : null;

$corsi1A = ($col1ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col1ATipo)->sortBy('title', 'asc');
$corsi2A = ($col2ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col2ATipo)->sortBy('title', 'asc');
$corsi3A = ($col3ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col3ATipo)->sortBy('title', 'asc');
$corsi4A = ($col4ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col4ATipo)->sortBy('title', 'asc');
$corsi1B = ($col1BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col1BTipo)->sortBy('title', 'asc');
$corsi2B = ($col2BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col2BTipo)->sortBy('title', 'asc');
$corsi3B = ($col3BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col3BTipo)->sortBy('title', 'asc');
$corsi4B = ($col4BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col4BTipo)->sortBy('title', 'asc');

$corsi1ACount = $corsi1A->count();
$corsi2ACount = $corsi2A->count();
$corsi3ACount = $corsi3A->count();
$corsi4ACount = $corsi4A->count();
$corsi1BCount = $corsi1B->count();
$corsi2BCount = $corsi2B->count();
$corsi3BCount = $corsi3B->count();
$corsi4BCount = $corsi4B->count();

// kill(get_defined_vars());


// foreach (page('corsi')->children() as $corso) {
//   echo "<h1>". $corso->title()->value() ."</h1>";
//   echo "<p>[". $corso->corsoId() ."]</p>";
//   echo "<br><strong>ORARIO</strong>";
//   echo $corso->orario()->kt();
//   echo "<br><strong>SEDE</strong>";
//   echo $corso->sede()->kt();
//   echo "<br><strong>DIPLOMA</strong>";
//   echo $corso->diploma()->kt();
//   echo "<br><br><br>";
// }

// kill(null);

?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-sm-8 col-md-6">
          <div class="links-gold text-sans-s pr-3 pr-sm-5">
            <?= $page->testo()->kirbytext() ?>
          </div>
        </div>
        <div class="col-sm-4 col-md-6">
          <p class="font-sans-s">
            IN VETRINA
          </p>
          <hr class="mb-3 mt-2">
          <div class="row">
            
            <?php foreach($stannoPerIniziare as $item): ?>
              <?php 
              $corso = $item["corso"]; 


              // Old
              // $nome = (trim($corso->nome()->kirbytext()) !== "")
              //   ? $corso->nome()->kirbytext()
              //   : $corso->title()->kirbytext()
              
              // New
              $nome = $corso->nome()->isNotEmpty()
                ? $corso->nome()->value()
                : $corso->title()->value();
              $nome = "<p>". html::breaks($nome) ."</p>";


              ?>
              <?php if($corso): ?>
                <div class="col col-md-6 corso pb-3 mb-3">
                  <?php $type = $corso->disciplinaUid()->value() == "corsi-speciali" ? "special" : "normal"; ?>
                  <p class="font-supersuper-llll capolettera <?= $colors["capolettera"][$type] ?>">
                    <?= page('discipline/'. $corso->disciplinaUid()->value())->capolettera()->value() ?>
                  </p>
                  <div class="text font-super-ll <?= $colors["text"][$type] ?>"><?= $nome ?></div>
                  <a class="btn btn-primary <?= $colors["button"][$type] ?> mt-3" href="<?= $corso->url() ?>" role="button">ISCRIVITI &rarr;</a>
                  <?php if($item["entroIl"] != ""): ?>
                    <p class="font-sans-ss mt-3"><?= $item["entroIl"] ?></p>
                  <?php endif ?>
                </div>
              <?php endif ?>
            <?php endforeach ?>
            
          </div>
        </div>
      </div>
    </div>

    <div class="my-2"><br /></div>

    <?php /* CORONAVIRUS ------------------------------------ 

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <p class="font-sans-s mb-2">CORSI ATTIVI</p>
          <hr />
        </div>
        <div class="col-sm-6 col-lg-3 mb-sm-5 corsi-column first">
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsiAnnuali()->value() ?></span>
          <?php foreach($corsi240 as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsi240->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsiOfficine()->value() ?></span>
          <?php foreach($corsiOfficine as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsiOfficine->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
        <div class="col-12 d-block d-sm-none">
          <hr />
        </div>
        <div class="col-sm-6 col-lg-3 mb-sm-5 corsi-column second">
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi60()->value() ?></span>
          <?php foreach($corsi60 as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsi60->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
        <div class="col-12 d-block d-lg-none">
          <hr />
        </div>
        <div class="col-sm-6 col-lg-3 mb-sm-5 corsi-column third">
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi30()->value() ?></span>
          <?php foreach($corsi30 as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsi30->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
        <div class="col-12 d-block d-sm-none">
          <hr />
        </div>
        <div class="col-sm-6 col-lg-3 mb-sm-5 corsi-column fourth">
          <span class="bordered-list bigger-title font-color-gold"><?= $page->titoloCorsiSpeciali()->value() ?></span>
          <?php foreach($corsiSpeciali as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsiSpeciali->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
      </div>
    </div>

    ------------------------------------------------------ */?>
    


    <?php /* CORONAVIRUS ALT -----------------------------

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <!-- <p class="font-sans-s mb-2">CORSI ATTIVI</p> -->
          <p class="font-super-ll mb-4">Corsi attivi</p>
          <hr />
        </div>

        <div class="col-md-4 mb-sm-5 corsi-column first">
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi60()->value() ?></span>
          <?php foreach($corsi60 as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsi60->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
        
        <div class="col-12 d-block d-md-none">
          <hr />
        </div>
        
        <div class="col-md-4 mb-sm-5 corsi-column second">
          <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi30()->value() ?></span>
          <?php foreach($corsi30 as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsi30->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
        
        <div class="col-12 d-block d-md-none">
          <hr />
        </div>
        
        <div class="col-md-4 mb-sm-5 corsi-column third">
          <span class="bordered-list bigger-title font-color-gold"><?= $page->titoloCorsiSpeciali()->value() ?></span>
          <?php foreach($corsiSpeciali as $item): ?>
            <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
          <?php endforeach ?>
          <?php if($corsiSpeciali->count() == 0): ?><span class="bordered-list">Nessun corso</span><?php endif ?>
        </div>
      </div>
    </div>

    CORONAVIRUS ALT END ------------------------- */?>

    <?php /* Column edits 1/7/20 -----------------------------------

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <p class="font-sans-s mb-2">CORSI ATTIVI</p>
          <hr />
        </div>

        <?php if ($corsi240->count() + $corsiOfficine->count() > 0): ?>
          
          <div class="col-lg mb-5 corsi-column">
            <?php if($corsi240->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsiAnnuali()->value() ?></span>
              <?php foreach($corsi240 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
              <div class="border-bottom mt-5 mt-lg-0"></div>
            <?php endif ?>
            <?php if($corsiOfficine->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsiOfficine()->value() ?></span>
              <?php foreach($corsiOfficine as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
          
        <?php endif ?>

        <?php if ($corsi60->count() > 0): ?>

          <div class="col-lg mb-5 corsi-column">
            <hr class="d-block d-lg-none" />
            <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi60()->value() ?></span>
            <?php foreach($corsi60 as $item): ?>
              <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
            <?php endforeach ?>
          </div>

        <?php endif ?>

        <?php if ($corsi30->count() > 0): ?>

          <div class="col-lg mb-5 corsi-column">
            <hr class="d-block d-lg-none" />
            <span class="bordered-list bigger-title font-color-blue"><?= $page->titoloCorsi30()->value() ?></span>
            <?php foreach($corsi30 as $item): ?>
              <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
            <?php endforeach ?>
          </div>
   
        <?php endif ?>
   
        <?php if ($corsiSpeciali->count() > 0): ?>

          <div class="col-lg mb-5 corsi-column">
            <hr class="d-block d-lg-none" />
            <span class="bordered-list bigger-title font-color-gold"><?= $page->titoloCorsiSpeciali()->value() ?></span>
            <?php foreach($corsiSpeciali as $item): ?>
              <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
            <?php endforeach ?>
          </div>
  
        <?php endif ?>

      </div>
    </div>

    END Column edits 1/7/20 -----------------------------------*/ ?>

    <?php /* Column edits EN-IT -----------------------------------*/ ?>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <p class="font-sans-s mb-2">CORSI ATTIVI</p>
          <hr />
        </div>

        <?php if ($corsi1A->count() + $corsi1B->count() > 0): ?>
          
          <div class="col-lg mb-5 corsi-column">
            <?php if($corsi1A->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol1ATitle()->value() ?></span>
              <?php foreach($corsi1A as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
              <div class="border-bottom mt-5 mt-lg-0"></div>
            <?php endif ?>
            <?php if($corsi1B->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol1BTitle()->value() ?></span>
              <?php foreach($corsi1B as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
          
        <?php endif ?>

        <?php if ($corsi2A->count() + $corsi2B->count() > 0): ?>
          
          <div class="col-lg mb-5 corsi-column">
            <?php if($corsi2A->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol2ATitle()->value() ?></span>
              <?php foreach($corsi2A as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
              <div class="border-bottom mt-5 mt-lg-0"></div>
            <?php endif ?>
            <?php if($corsi2B->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol2BTitle()->value() ?></span>
              <?php foreach($corsi2B as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
          
        <?php endif ?>

        <?php if ($corsi3A->count() + $corsi3B->count() > 0): ?>
          
          <div class="col-lg mb-5 corsi-column">
            <?php if($corsi3A->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol3ATitle()->value() ?></span>
              <?php foreach($corsi3A as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
              <div class="border-bottom mt-5 mt-lg-0"></div>
            <?php endif ?>
            <?php if($corsi3B->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol3BTitle()->value() ?></span>
              <?php foreach($corsi3B as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
          
        <?php endif ?>

        <?php if ($corsi4A->count() + $corsi4B->count() > 0): ?>
          
          <div class="col-lg mb-5 corsi-column">
            <?php if($corsi4A->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol4ATitle()->value() ?></span>
              <?php foreach($corsi4A as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
              <div class="border-bottom mt-5 mt-lg-0"></div>
            <?php endif ?>
            <?php if($corsi4B->count() > 0): ?>
              <span class="bordered-list bigger-title font-color-blue"><?= $page->dynamicCol4BTitle()->value() ?></span>
              <?php foreach($corsi4B as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
          
        <?php endif ?>

      </div>
    </div>

    <?php /* END Column edits EN-IT -----------------------------------*/ ?>



  </main>

<?php snippet('footer') ?>