<?php
$items = $page->children()->visible();
?>

<?php snippet('header') ?>

<main class="main" role="main">
  
  <?php snippet('breadcrumb-title'); ?>
  
  <div class="my-2"><br /></div>

  <div class="container-fluid super-cont">
    <div class="row">
      <?php 
      foreach($items as $item):
        $title = $item->title()->value();
        $text = $item->text()->excerpt(30, 'words');
        $url = $item->url();
        $cover = $item->cover()->toFile()->crop(380, 380, 70)->url();
        ?>
        
        <div class="col-12 col-sm-6 col-md-4 col-lg-3 mb-4">
          <a href="<?= $url ?>">
            <img class="img-fluid" src="<?= $cover ?>"/>
            <p class="font-sans-s mt-3"><?= $title ?></p>
            <?php if($text): ?>
            <p class="font-sans-ss mt-2 d-inline-block no-underline"><?= $text ?></p>
            <?php endif ?>
            <p class="font-sans-sss mt-2 d-inline-block no-underline"><?= $item->files()->count() ?> IMMAGINI</p>
          </a>
        </div>

      <?php endforeach ?>
    </div>
  </div>
</main>

<?php snippet('footer') ?>
