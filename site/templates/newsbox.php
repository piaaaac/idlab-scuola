<?php 
function btnText($news){
  $ctas = [
    "avvisi"        => "LEGGI",
    "corsi"         => "LEGGI",
    "eventi"        => "SCOPRI",
    "mostre"        => "SCOPRI",
    "bandi"         => "PARTECIPA",
    "segnalazioni"  => "LEGGI"
  ];
  return $ctas[$news->tipo()->value()];
}
?>
<?php snippet('header') ?>

  <main class="main newsbox" role="main">

    <div class="pb-2">
      <div class="container-fluid super-cont mb-1">
        <div class="row">
          <div class="col-12">
            <p class="font-super-ll mb-3"><?= page('newsbox')->title()->value() ?> — Archivio</p>
          </div>
        </div>
      
        <div class="row pt-5 mt-5 d-flex align-items-stretch">
          <?php foreach ($page->children()->visible()->sortBy("dataNews", "desc") as $n): ?>
            <div class="col-sm-6 col-md-4 mb-4 pt-2">
              <div class="bg-white h-100 d-flex flex-column p-4">
                <p class="font-sans-ss mb-4"><?= newsDate($n) ?></p>
                <div class="d-flex mb-4">
                  <a href="<?= $n->url() ?>" class="font-sans-l font-color-red"><?= $n->title() ?></a>
                </div>
                <div class="flex-grow-1"></div>
                <div>
                  <a class="btn btn-primary btn-small red" href="<?= $n->url() ?>" role="button"><?= btnText($n) ?> &rarr;</a>
                </div>
              </div>
            </div>
          <?php endforeach ?>
        </div>

      </div>
    </div>

  </main>

<?php snippet('footer') ?>