<?php
snippet('logincheck-block-non-admin');
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');

// --- settings via url
$corsoSelezionato = null;
if(isset($_GET["corsoid"])){ 
  $corsoSelezionato = $_GET["corsoid"]; 
  if($corsoSelezionato === ""){
    $corsoSelezionato = null;
  }
}
// var_dump($corsoSelezionato);
$mode = "simple"; // simple|complete
if(isset($_GET["mode"])){ 
  $mode = $_GET["mode"];
}
if($corsoSelezionato !== null){
  $mode = null;
}


// --- Prepare data
$allCorsi = page("corsi")->children();
if($corsoSelezionato !== null){ 
  $allCorsi = page("corsi")->children()->filterBy("corsoId", $corsoSelezionato); 
}





// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// BEGIN NEW VERSION
// -------------------------------------------------------------------------

// ----------------------
// flat array of iscritti
// ----------------------

$iscritti = [];

foreach (page("segreteria-ordini")->children() as $ordine) {

  if ($ordine->iscrizioni()->isEmpty()) {
    
    // --- vecchi ordini senza iscrizioni
    $cartArticleeeeIdsString = $ordine->articleeeeIds()->value();
    $articleeeeIds = explode(",", $cartArticleeeeIdsString);
    foreach ($articleeeeIds as $articleeeeId) {
      $acquirente = [
        "articleeeeId"  => $articleeeeId,
        "cognome"       => $ordine->form_cognome()->value(),
        "nome"          => $ordine->form_nome()->value(),
        "email"         => $ordine->form_email()->value(),
        "telefono"      => $ordine->form_telefono()->value(),
        "ordine_uid"    => $ordine->orderId()->value(),
        "ordine_titolo" => $ordine->title()->value(),
        "ordine_pagato" => $ordine->pagamentoOk()->value(),
        "nuovo_form"    => 0,
      ];
      $iscritti[] = $acquirente;
    }
  } else {

    // --- nuovi ordini con iscrizioni
    $iscrizioni = $ordine->iscrizioni()->toStructure();
    foreach ($iscrizioni as $iscrizione) {
      $articleeeeId = $iscrizione->iscrizione_articleeeeId()->value();
      $iscrittoa = [
        "articleeeeId"  => $articleeeeId,
        "cognome"       => $iscrizione->iscrizione_cognome()->value(),
        "nome"          => $iscrizione->iscrizione_nome()->value(),
        "email"         => $iscrizione->iscrizione_email()->value(),
        "telefono"      => $iscrizione->iscrizione_telefono()->value(),
        "ordine_uid"    => $ordine->orderId()->value(),
        "ordine_titolo" => $ordine->title()->value(),
        "ordine_pagato" => $ordine->pagamentoOk()->value(),
        "nuovo_form"    => 1,
      ];
      $iscritti[] = $iscrittoa;
    }
  }
}

// -------------------------------------------
// --- save metadata traversing all corsi once
// -------------------------------------------

$corsiAndTurniMeta = [];
/*
[
  "corsoId" => [
    "title" => "Titolo Corso",
    "turni" => [
      "turnoUid" => "Titolo Turno",
    ]
  ]
]
*/
foreach (page("corsi")->children() as $corso) {
  $corsoId = $corso->corsoId()->value();
  if (!array_key_exists($corsoId, $corsiAndTurniMeta)) {
    $corsiAndTurniMeta[$corsoId] = [
      "uri"     => $corso->uri(),
      "title"   => $corso->title()->value(),
      "visible" => $corso->isVisible(),
      "turni"   => [],
    ];
    foreach ($corso->children() as $turno) {
      $turnoUid = $turno->uid();
      $corsiAndTurniMeta[$corsoId]["turni"][$turnoUid] = [
        "uri"     => $turno->uri(),
        "title"   => $turno->title()->value(),
        "stato"   => $turno->stato()->value(),
        "visible" => $turno->isVisible(),
        "dates"   => turnoDates($turno, ""),
      ];
    }
  }
}

$displayStructure = [];
/*
[
  corso
  corso
  corso: {
    title
    count
    turni: [
      turno
      turno
      turno: {
        title
        count
        iscritti: [
          iscrittoa
          iscrittoa
          iscrittoa: {
            articleeeeId
            cognome
            nome
            email
            telefono
            ordine_uid
            ordine_titolo
            ordine_pagato
            nuovo_form
          }
        ]
      }
    ]
  }
]
*/
foreach ($iscritti as $isc) {
  $articleeeeId = $isc["articleeeeId"];
  $corsoId = explode("~", $articleeeeId)[0];
  $turnoUid = explode("~", $articleeeeId)[1];

  if(!isset($corsiAndTurniMeta[$corsoId])){
    $titleCorso = "Corso non più presente ($articleeeeId)";
    $titleTurno = "";
    $corsoMeta = null;
    $turnoMeta = null;
  } else {
    $titleCorso = $corsiAndTurniMeta[$corsoId]["title"];
    $corsoMeta = $corsiAndTurniMeta[$corsoId];
    $turnoExists = isset($corsiAndTurniMeta[$corsoId]["turni"][$turnoUid]);
    $titleTurno = $turnoExists
      ? $corsiAndTurniMeta[$corsoId]["turni"][$turnoUid]["title"]
      : "Turno non più presente ($turnoUid)";
    $turnoMeta = $turnoExists 
      ? $corsiAndTurniMeta[$corsoId]["turni"][$turnoUid]
      : null;
  }
  if (!array_key_exists($corsoId, $displayStructure)) {
    $displayStructure[$corsoId] = [
      "title" => $titleCorso,
      "meta"  => $corsoMeta,
      "count" => 0,
      "turni" => [],
    ];
  }
  if (!array_key_exists($turnoUid, $displayStructure[$corsoId]["turni"])) {
    $displayStructure[$corsoId]["turni"][$turnoUid] = [
      "title" => $titleTurno,
      "meta"  => $turnoMeta,
      "count" => 0,
      "iscritti" => [],
    ];
  }
  if ($isc["ordine_pagato"] == "1") {
    $displayStructure[$corsoId]["count"]++;
    $displayStructure[$corsoId]["turni"][$turnoUid]["count"]++;
  }
  $displayStructure[$corsoId]["turni"][$turnoUid]["iscritti"][] = $isc;
}
// kill($displayStructure);


// --- Sort

// $tests = [
//   ["pippo", "pollo"],
//   ["aaaaa", "zzzzz",],
//   ["a", "A",],
//   ["a", "ZUZZURELLONE",],
//   ["A", "zuzzurellone",]
// ];

// kill(array_map(function ($d) {
//   return 
//     implode(", ", $d) 
//     . " => "
//     . strcmp($d[0], $d[1]);
// }, $tests));

uasort($displayStructure, function($a, $b){
  $titleA = $a["title"];
  $titleB = $b["title"];
  $sortValA = $a["meta"]["visible"] ? strtoupper($titleA) : strtolower($titleA);
  $sortValB = $b["meta"]["visible"] ? strtoupper($titleB) : strtolower($titleB);
  return strcmp($sortValA, $sortValB);
});

?>

  <?php snippet('header', ["hideMenu" => true]) ?>
  
  <style>
    [data-corso-id] { cursor: pointer; }
    [data-corso-id]:hover h2 { text-decoration: underline; }
  </style>

  <main class="main pagine-segreteria pt-5" role="main" id="pagina-segreteria-iscrizioni-per-corso">
    <div class="container-fluid super-cont s-c-admin">
      <div class="row">
        <div class="col-12">
          <?php foreach ($displayStructure as $corsoId => $corso):/* kill($corso);*/ ?>
            <hr />
            <div>
              <div data-corso-id="<?= $corsoId ?>" class="d-flex justify-content-between align-items-baseline">
                <div class="pt-1">
                  <h2 class="font-sans-l d-inline-block mr-2 <?= (!$corso["meta"]["visible"]) ? "font-color-black20" : "" ?>">
                    <?php if(!$corso["meta"]["visible"]): ?>
                      <i class="fas fa-eye-slash font-color-black20"></i>
                    <?php endif ?>
                    <?= $corso["title"] ?>
                  </h2>
                  <span class="font-sans-ss font-color-black20"><?= $corsoId ?></span>
                </div>
                <p class="font-sans-l font-color-blue"><?= $corso["count"] ?> iscritti</p>
              </div>
              <div class="collapsible-container" id="content-corso-<?= $corsoId ?>">

                <?php foreach ($corso["turni"] as $turnoUid => $t): ?>
                  <div class="row">
                    <div class="col-12">
                      <hr />
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-3 col-sm-6 mb-4 pr-3">
                      <p class="my-3">
                        <a class="font-sans-l" target="_blank"
                          href="<?= $site->url() ."/panel/pages/". $t["meta"]["uri"] ."/edit" ?>" 
                        ><?= $t["title"] ?></a>
                        <?php if($t["meta"]["stato"] === "chiuso"): ?>
                          <span class="font-color-blue ml-1">Chiuso</span>
                        <?php endif ?>
                        <?php if($t["meta"]["stato"] === "pieno"): ?>
                          <span class="font-color-red ml-1">Pieno</span>
                        <?php endif ?>
                      </p>
                      <p class="font-sans-ss"><?= $t["meta"]["dates"] ?></p>
                      <p class="font-sans-ss font-color-blue"><?= $t["count"] ?> iscritti</p>
                      <p class="font-sans-ss">
                        <a href="<?= $site->url() ."/registro/$corsoId---$turnoUid" ?>" 
                          target="_blank"
                        >Genera registro &rarr;</a>
                      </p>
                    </div>

                    <div class="col-md-9 bordered-left">

                      <table class="turni-expanded">

                          <?php foreach ($t["iscritti"] as $isc):
                            $payed = true;
                            $colorPayed = "";
                            $colorLink = "font-color-blue";
                            if($isc["ordine_pagato"] != "1"){
                              $payed = false;
                              $colorPayed = "font-color-black20";
                              $colorLink = "font-color-red";
                            }
                            ?>
                            <tr>
                              <td class="c1"><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                                <?= ($isc["nuovo_form"] ? "[FORM] " : "") . $isc["cognome"] ." ". $isc["nome"] ?></p></td>
                              <td class="c2"><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                                <?= $isc["email"] ?></p></td>
                              <td class="c3"><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                                <?= $isc["telefono"] ?></p></td>
                              <td class="c4"><p class="font-sans-ss text-nowrap mt-2 mb-2 text-right">
                                <a class="font-sans-ss <?=$colorLink?>" target="_blank" href="<?= page('iscrizione2')->url() ."/order:". $isc["ordine_uid"] ?>">
                                  <!-- <i class="fas fa-external-link-alt d-inline-block mr-1"></i> -->
                                  <!-- <i class="fas fa-list-alt d-inline-block mr-1"></i> -->
                                  Ordine
                                </a>
                              </p></td>
                            </tr>
                          <?php endforeach ?>

                      </table>

                    </div>
                  </div>
                <?php endforeach ?>

              </div>
            </div>
          <?php endforeach ?>
        </div>
      </div>
    </div>
  </main>

  <script>
    var selected = null;
    $(document).ready(function(){
      $(".collapsible-container").hide();
      $("[data-corso-id]").click(function () {
        var corsoId = this.dataset.corsoId;
        $(".collapsible-container").slideUp();
        if (corsoId !== selected) {
          $("#content-corso-"+ corsoId).slideToggle(function () {
            $("html, body").animate({scrollTop: $(this).offset().top - 50}, 400);
          });
          selected = corsoId;
        } else {
          selected = null;
        }
      });
    });
  </script>

  <?php snippet('footer', ["hideFooter" => true]) ?>

<?php
exit();

// -------------------------------------------------------------------------
// END NEW VERSION
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------
// -------------------------------------------------------------------------






$corsiArray = [];
foreach ($allCorsi as $corso) {
  $turni = [];
  foreach($corso->children()->sortBy("title", "asc") as $turno){
    $articleeeeId = $corso->corsoId()->value() ."~". $turno->uid();
    $ordiniArticleeeeId = page("segreteria-ordini")->children()->filter(function($ordine) use ($articleeeeId) {
      $articleeeeIds = explode(",", $ordine->articleeeeIds()->value());
      return(in_array($articleeeeId, $articleeeeIds));
    });
    
    // $ordiniArticleeeeId->sortBy("pagamentoOk", "desc", "form_dataOraOrdine", "desc");
    $ordiniArticleeeeId->sortBy("pagamentoOk", "desc");
    $ordiniPagati = $ordiniArticleeeeId->filterBy("pagamentoOk", "1");
    $turnoObj = (object)[
      "kirbyObj" => $turno,
      "fields" => $turno->content()->toArray(),
      "articleeeeId" => $articleeeeId,
      "ordini" => $ordiniArticleeeeId,
      "ordiniPagati" => $ordiniPagati
    ];
    $turni[] = $turnoObj;
  }
  $corsoObj = (object) [
    "kirbyObj" => $corso,
    "fields" => $corso->content()->toArray(),
    "turni" => $turni
  ];
  $corsiArray[] = $corsoObj;
}

// kill($corsiArray);

$corsiPerTipo = [];
foreach ($corsiArray as $item) {
  if (!array_key_exists($item->fields["tipo"], $corsiPerTipo)) {
    $corsiPerTipo[$item->fields["tipo"]] = [];
  }
  $corsiPerTipo[$item->fields["tipo"]][] = $item;
}

// kill($corsiPerTipo);

$maxIscrittiXTurno = 0;
$maxIscrittiXCorso = 0;
$maxOrdiniXTurno = 0;
$maxOrdiniXCorso = 0;
foreach($corsiArray as $item){ 
  $nIscrittiCorso = 0;
  $nOrdiniCorso = 0;
  foreach($item->turni as $t){ 
    $nIscritti = count($t->ordiniPagati);
    $nOrdini = count($t->ordini);
    $nIscrittiCorso += $nIscritti;
    $nOrdiniCorso += $nOrdini;
    if($nIscritti > $maxIscrittiXTurno){ $maxIscrittiXTurno = $nIscritti; }
    if($nOrdini > $maxOrdiniXTurno){ $maxOrdiniXTurno = $nOrdini; }
  }
  if($nIscrittiCorso > $maxIscrittiXCorso){ $maxIscrittiXCorso = $nIscrittiCorso; }
  if($nOrdiniCorso > $maxOrdiniXCorso){ $maxOrdiniXCorso = $nOrdiniCorso; }
}

// print_r_tree($corsiArray240);
// print_r_tree($corsiArraySpecial);

?>

<?php snippet('header', ["hideMenu" => true]) ?>

  <main class="main pagine-segreteria pt-5" role="main" id="pagina-segreteria-iscrizioni-per-corso">

    <div class="container-fluid super-cont s-c-admin">

      <div class="row">
        <div class="col-12 mb-4">
          <a href="?mode=simple" class="btn btn-primary btn-small <?=
            (($mode === "simple") ? "black" : "black-light") 
          ?>">MOSTRA CORSI</a>
          <a href="?mode=complete" class="btn btn-primary btn-small <?=
            (($mode === "complete") ? "black" : "black-light") 
          ?> ml-2">MOSTRA CORSI E TURNI</a>
        </div>
      </div>

      <?php if($corsoSelezionato === null): ?>

        <?php foreach($corsiPerTipo as $tipo => $items): ?>

          <?php
          $titleTipo = "";
          if (!$titleTipo && page("corsi")->dynamicCol1ATipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol1ATitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol2ATipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol2ATitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol3ATipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol3ATitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol4ATipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol4ATitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol1BTipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol1BTitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol2BTipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol2BTitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol3BTipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol3BTitle()->value();
          if (!$titleTipo && page("corsi")->dynamicCol4BTipo()->value() == $tipo) 
            $titleTipo = page("corsi")->dynamicCol4BTitle()->value();
          
          $numPerCol = ceil(count($items) / 2);
          $col1Items = array_splice($items, 0, $numPerCol);
          $col2Items = array_splice($items, 0, $numPerCol);
          ?>

          <div class="row">

            <div class="col-12">
              <h2><?= $tipo . ($titleTipo ? " ($titleTipo)" : "") ?></h2>
              <hr />
            </div>

            <div class="col-sm-6">
              <?php foreach($col1Items as $item): ?>
                <?php snippet('segreteria-item-corso', [
                  "item" => $item,
                  "mode" => $mode,
                  "maxIscrittiXTurno" => $maxIscrittiXTurno,
                  "maxIscrittiXCorso" => $maxIscrittiXCorso,
                  "maxOrdiniXCorso" => $maxOrdiniXCorso,
                  "maxOrdiniXTurno" => $maxOrdiniXTurno
                ]) ?>
              <?php endforeach ?>
            </div>

            <div class="col-sm-6">
              <?php foreach($col2Items as $item): ?>
                <?php snippet('segreteria-item-corso', [
                  "item" => $item,
                  "mode" => $mode,
                  "maxIscrittiXTurno" => $maxIscrittiXTurno,
                  "maxIscrittiXCorso" => $maxIscrittiXCorso,
                  "maxOrdiniXCorso" => $maxOrdiniXCorso,
                  "maxOrdiniXTurno" => $maxOrdiniXTurno
                ]) ?>
              <?php endforeach ?>
            </div>
              
          </div>
          <div class="spacer my-5"></div>

        <?php endforeach ?>


      <?php else: ?>

        <div class="row">
          <div class="col-12">
            <hr />
          </div>
        </div>

        <div class="row">
          <div class="col-md-3 col-sm-6 mb-4">
            <?php snippet('segreteria-item-corso', [
              "item" => $corsiArray[0],
              "mode" => "completeLarge",
              "maxIscrittiXTurno" => $maxIscrittiXTurno,
              "maxIscrittiXCorso" => $maxIscrittiXCorso,
              "maxOrdiniXCorso" => $maxOrdiniXCorso,
              "maxOrdiniXTurno" => $maxOrdiniXTurno
            ]) ?>
          </div>

          <div class="col-md-9 bordered-left">
            <table class="turni-expanded mt-3 mb-1">
              <?php $i=0; ?>
              <?php foreach($corsiArray[0]->turni as $t): ?>

                <tr>
                  <td colspan="42">
                    <header class="mb-2 <?= r($i>0, "mt-5", "") ?>">
                      <p class="font-sans-s">
                        <a href="<?= $site->url() ."/panel/pages/". $t->kirbyObj->uri() ."/edit" ?>" target="_blank">
                          <?= $t->fields["title"] ?>
                        </a>
                        <?php if($t->fields["stato"] === "chiuso"): ?>
                          <span class="font-color-blue ml-1">Chiuso</span>
                        <?php endif ?>
                        <?php if($t->fields["stato"] === "pieno"): ?>
                          <span class="font-color-red ml-1">Pieno</span>
                        <?php endif ?>
                      </p>
                      <p class="font-sans-sss">
                        <?= count($t->ordiniPagati) ?> iscritti <?= turnoDates($t->kirbyObj, "—") ?>
                        <?= /*$t->fields["datainizio"] ?>&mdash;<?= $t->fields["datainizio"] */ ""?>

                        <?php
                        $students = [];
                        foreach($t->ordini->sortBy("pagamentoOk", "desc") as $o){
                          $students[] = [
                            "cognome"   => $o->form_cognome()->value(),
                            "nome"      => $o->form_nome()->value(),
                            "email"     => $o->form_email()->value(),
                            "telefono"  => $o->form_telefono()->value(),
                          ];
                        }
                        ?>
                        <a class="font-sans-sss ml-3" 
                        href="<?= $site->url() ."/registro/". $corsiArray[0]->kirbyObj->corsoId()->value() ."---". $t->kirbyObj->uid() ?>" 
                        target="_blank"
                        title="<?= print_r($students)?>">
                          Genera registro &rarr;
                        </a>

                      </p>
                    </header>
                  </td>
                </tr>

                <?php foreach($t->ordini->sortBy("pagamentoOk", "desc") as $o): ?>
                  <?php
                    $payed = true;
                    $colorPayed = "";
                    $colorLink = "font-color-blue";
                    if($corsiArray[0]->fields["tipo"] === "speciali"){
                      $colorLink = "font-color-gold";
                    }
                    if($o->pagamentoOk()->value() != "1"){
                      $payed = false;
                      $colorPayed = "font-color-black20";
                      $colorLink = "font-color-red";
                    }
                  ?>
                  <tr>
                    <td><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                      <?=$o->form_cognome()->value()?></p></td>
                    <td><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                      <?=$o->form_nome()->value()?></p></td>
                    <td><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                      <?=$o->form_email()->value()?></p></td>
                    <td><p class="font-sans-ss text-nowrap <?=$colorPayed?> mt-2 mb-2">
                      <?=$o->form_telefono()->value()?></p></td>
                    <td><p class="font-sans-ss text-nowrap mt-2 mb-2 text-right">
                      <a class="font-sans-ss <?=$colorLink?>" target="_blank" href="<?= page('iscrizione2')->url() ."/order:". $o->orderId()->value() ?>">
                        <!-- <i class="fas fa-external-link-alt d-inline-block mr-1"></i> -->
                        <!-- <i class="fas fa-list-alt d-inline-block mr-1"></i> -->
                        Ordine
                      </a>
                    </p></td>
                  </tr>
                <?php endforeach ?>

                <?php $i++; ?>
              <?php endforeach ?>
            </table>
          </div>

        </div>

      <?php endif ?>

    </div>
  </main>

<?php snippet('footer', ["hideFooter" => true]) ?>
