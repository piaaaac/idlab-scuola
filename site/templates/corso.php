<?php
snippet('commonfunctions');

$isAdmin = false;
if($user = $site->user() and $user->isAdmin()){
  $isAdmin = true;
}

$labels = [
  "prossimiTurni" => [ "it" => "PROSSIMI TURNI",                "en" => "NEXT UP"],
  "modalitaIscr"  => [ "it" => "MODALITÀ DI ISCRIZIONE",        "en" => "ENROLMENT OPTIONS"],
  "docenti"       => [ "it" => "DOCENTI",                       "en" => "TEACHERS"],
  "informati"     => [ "it" => "INFORMATI",                     "en" => "GET MORE INFO"],
  "iscriviti"     => [ "it" => "ISCRIVITI",                     "en" => "ENROL"],
  "modalitaAcq"   => [ "it" => "Modalit&agrave; di acquisto",   "en" => "Purchasing options"],
];
$l = $page->isEnglish()->bool() ? "en" : "it";

// $items = $page->children()->visible()->sortBy("title", "asc");
$items = $page->children()->visible();
$itemsAperti = $items->filterBy("stato", "aperto");

$itemsBySubtitle = [];
$defaultSubtitle = $labels["prossimiTurni"][$l];
foreach($itemsAperti as $item){
  if($item->adminOnly() == '1' && !$isAdmin){
    continue;
  } 
  $subtitle = $defaultSubtitle;
  if(!$item->sezione()->isEmpty()){
    $subtitle = $item->sezione()->upper()->value();
  }
  if(!isset($itemsBySubtitle[$subtitle])){
    $itemsBySubtitle[$subtitle] = [];
  }
  $itemsBySubtitle[$subtitle][] = $item;
}

$itemsBySubtitlePagina = [];
$defaultSubtitlePagina = $labels["prossimiTurni"][$l];
foreach($items as $item){
  if($item->nascondiInPagina() == '1'){
    continue;
  } 
  if($item->adminOnly() == '1' && !$isAdmin){
    continue;
  } 
  $subtitle = $defaultSubtitlePagina;
  if(!$item->sezione()->isEmpty()){
    $subtitle = $item->sezione()->upper()->value();
  }
  if(!isset($itemsBySubtitlePagina[$subtitle])){
    $itemsBySubtitlePagina[$subtitle] = [];
  }
  $itemsBySubtitlePagina[$subtitle][] = $item;
}
?>

<?php /* --- functions to generate turno-item in popup */?>

<?php function corsoTurnoPopup($item, $articleeeeId) { ob_start(); ?>
  <a href="javascript:addToCartAndCloseModal('<?= $articleeeeId ?>')"
  data-articleeeeId="<?= $articleeeeId ?>"
  class="turno-item d-flex justify-content-between align-items-center mb-1<?=
    ($item->adminOnly() == '1') ? " turno-admin-only" : "" ?>" >
    <div>
      <p class="name"><?= $item->title()->value() ?></p>
      <p class="details">
        <?= money_format('%!.2n', (float)$item->itemCost()->value()) ." &euro;" ?>
        <?= turnoDates($item) ?>
      </p>
    </div>
    <div class="ml-1 pt-1">&rarr;</div>
  </a>
<?php return ob_get_clean(); } ?>

<?php function corsoLinkPopup($item) { ob_start(); ?>
  <a href="<?= $item->linkUrl()->value() ?>" target="_blank"
  class="turno-item d-flex justify-content-between align-items-center mb-1<?=
    ($item->adminOnly() == '1') ? " turno-admin-only" : "" ?>" >
    <div>
      <p class="name"><?= $item->title()->value() ?></p>
      <p class="details"><?= $item->text()->value() ?></p>
    </div>
    <div class="ml-1 pt-1">&rarr;</div>
  </a>
<?php return ob_get_clean(); } ?>


<?php /* --- functions to generate turno-item in page */?>

<?php function corsoTurnoPagina($item) { 
  $statoTurno = [
    "aperto" => ["font-color-black", "", ""],
    "chiuso" => ["font-color-black30", "Iscrizioni chiuse", "font-color-blue"],
    "pieno" =>  ["font-color-black30", "Posti esauriti!", "font-color-red"]
  ];
  ob_start(); ?>
  <p class="font-sans-ss <?=$statoTurno[$item->stato()->value()][0]?>">
    <?= $item->title()->value() ?>
    <br />
    <?= money_format('%!.2n', (float)$item->itemCost()->value()) ." &euro;" ?>
    <?= turnoDates($item) ?>
  </p>
  <?php if($item->stato()->value() !== "aperto"): ?>
    <p class="font-sans-ss <?=$statoTurno[$item->stato()->value()][2]?>">
      <?=$statoTurno[$item->stato()->value()][1]?>
    </p>
  <?php endif ?>
<?php return ob_get_clean(); } ?>

<?php function corsoLinkPagina($item) { ob_start(); ?>
  <p class="font-sans-ss">
    <?=$item->title()->value()?>
    <br/>
    <?=$item->text()->value()?>
  </p>
<?php return ob_get_clean(); } ?>

<?php snippet('header') ?>
<?php include_once(kirby()->roots()->snippets() .'/load-cart-js-functions.php') ?>
  
  <script>
    $(document).ready(function(){
      var ajaxCallUrl = '<?= $site->url() ."/assets/php/cart-json-from-articles-string.php" ?>';
      $.ajax({
        url: ajaxCallUrl,
        success: function(result){
          console.log('cart-json-from-articles-string', JSON.parse(result));
          updateButton(JSON.parse(result));
        }
      });
    });

    // update button to disable link added
  </script>

  <main class="main" role="main">
    
    <?php 
      // snippet('breadcrumb-title', ["textColor" => $textColor]);
      snippet('breadcrumb-title') ;
    ?>
    
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">

      <div class="row">
        <div class="d-none d-sm-block col-sm-8 col-md-6">
          <a class="btn btn-large reduce-xs btn-primary font-sans-l font-color-blue" href="<?= page('info')->url() ?>" role="button"><?= $labels["informati"][$l] ?> <i class="fas fa-info-circle d-inline-block ml-1"></i></a>
        </div>
        <div class="col-12 col-sm-4 col-md-6">
          <a class="d-inline-block d-sm-none mr-2 mr-sm-0 btn btn-large reduce-xs btn-primary font-sans-l font-color-blue" href="<?= page('info')->url() ?>" role="button"><?= $labels["informati"][$l] ?> <i class="fas fa-envelope d-inline-block ml-1"></i></a>

          <button id="page-corso-dropdown-btn" 
          type="button" 
          class="btn btn-primary btn-large reduce-xs font-color-gold" 
          data-toggle="modal" data-target="#modal-turni"
          aria-haspopup="true" 
          aria-expanded="false" 
          <?= r($itemsAperti->count()==0, "disabled") ?>>
            <?= $labels["iscriviti"][$l] ?> <i class="fas fa-pencil-alt d-inline-block ml-1"></i>
          </button>

        </div>
      </div>

      <div class="row">
        <div class="col-sm-8 col-md-6">
          <p class="my-2"><br /></p>
          <div class="description text-sans-s pr-3 pr-sm-3">
            <?= $page->testo()->kirbytext() ?>
          </div>
          <?php /* if($page->tipo()->value() === "serale240"): ?>
            <a class="btn btn-primary mt-3 mb-3" href="<?= page('atelier')->url() ?>" role="button">SCOPRI GLI ATELIER &rarr;</a>
          <?php endif */ ?>

          <?php 
          /*
          var_dump($page->programma()->value());
          var_dump($page->programma()->toStructure());
          */
          ?>

          <?php if($page->programma()->value()): ?>
            <div id="programma-container">
              <?php snippet('programma', ['programmaStructure' => $page->programma()->toStructure()]) ?>
            </div>
          <?php endif ?>







          <?php if($page->galleryImages()->value()): ?>
            <p class="font-sans-s font-color-red mt-4 mb-2">GALLERY</p>
            <hr class="mb-4" />
            <div class="row">
            <?php $i = 0; ?>
            <?php foreach($page->galleryImages()->toStructure() as $item): ?>
              <?php if($item->giimage() && $image = $item->giimage()->toFile()): ?>
                <div class="col-6 col-md-4 mb-4 gallery-image">
                  <a href="<?= "javascript:openGallery($i);" ?>">
                    <img class="img-fluid" src="<?= $image->url() ?>"/>
                  </a>
                </div>
              <?php endif ?>
              <?php $i++; ?>
            <?php endforeach ?>
            </div>
          <?php endif ?>









          <?php if($page->partners()->value()): ?>
            <p class="font-sans-s font-color-red mt-4 mb-2">PARTNER</p>
            <hr class="mb-4" />
            <div class="row">
            <?php foreach($page->partners()->toStructure() as $item): ?>
              <div class="col-6 col-md-4 mb-4">
                <?php if($item->pimage() && $image = $item->pimage()->toFile()): ?>
                  <div class="partner-item" style="
                  background-image: url(<?=$image->url()?>);
                  background-size: contain;
                  background-repeat: no-repeat;
                  background-position: center center;
                  ">
                    <img class="img-fluid" src="<?=$site->url() ."/assets/images/square-spacer.png"?>"/>
                  </div>
                <?php endif ?>
                <p class="font-sans-sss text-center mt-2"><?= $item->ptext()->value() ?></p>
              </div>
            <?php endforeach ?>
            </div>
          <?php endif ?>

        </div>
        <div class="col-sm-4 col-md-6">
          <p class="my-2"><br /></p>
          <div class="row">
            <div class="col-md-6 col-xl-4 mb-4">
              
              <!-- elenco turni in pagina -->
              <?php if($page->testoAlternativoTurni()->value() != ""): ?>
                <div class="corso-info-item mb-4">
                  <p class="font-sans-s font-color-red mb-2"><?= $labels["modalitaIscr"][$l] ?></p>
                  <hr class="mb-2" />
                  <p class="font-sans-ss"><?=nl2br($page->testoAlternativoTurni()->value())?></p>
                  <hr class="mb-1 mt-2" />
                </div>
              <?php else: ?>
                <?php if($items->count() == 0): ?>
                <?php /*if($itemsAperti->count() == 0):*/ ?>
                  <div class="corso-info-item mb-4">
                    <p class="font-sans-s font-color-red mb-2"><?= $labels["prossimiTurni"][$l] ?></p>
                    <hr class="mb-2" />
                    <p class="font-sans-ss">Nessun turno disponibile.</p>
                    <!-- <hr class="mb-1 mt-2" /> -->
                  </div>
                <?php else: ?>
                  <?php foreach($itemsBySubtitlePagina as $subtitle => $subtitleItems): ?>
                    <div class="corso-info-item mb-4">
                      <p class="font-sans-s font-color-red mb-2"><?= $subtitle ?></p>
                      <hr class="mb-1 " />
                      <?php 
                      foreach($subtitleItems as $item){
                        if($item->template() == "corso-turno"){
                          $articleeeeId = $page->corsoId()->value() .'~'. $item->uid();
                          echo corsoTurnoPagina($item, $articleeeeId);
                        } else {
                          echo corsoLinkPagina($item);
                        }
                        echo '<hr class="mb-2 mt-1"/>';
                      }
                      ?>
                    </div>
                  <?php endforeach ?>
                <?php endif ?>
              <?php endif ?>
              <!-- END elenco turni in pagina -->








              <?php /*
              $orario = $page->orario()->exists() && $page->orario()->isNotEmpty();
              $sede = $page->sede()->exists() && $page->sede()->isNotEmpty();
              $diploma = $page->diploma()->exists() && $page->diploma()->isNotEmpty();
              if($orario || $sede || $diploma): ?>
                <div class="corso-info-item">
                  <p class="font-sans-s font-color-red mb-2">INFO</p>
                  <hr />

                  <?php if($orario): ?>
                    <div class="font-sans-ss mt-2 mb-2">
                      <p>ORARIO</p>
                      <?= $page->orario()->kirbytext() ?>
                    </div>
                    <hr />
                  <?php endif ?>

                  <?php if($sede): ?>
                    <div class="font-sans-ss mt-2 mb-2">
                      <p>SEDE</p>
                      <?= $page->sede()->kirbytext() ?>
                    </div>
                    <hr />
                  <?php endif ?>

                  <?php if($diploma): ?>
                    <div class="font-sans-ss mt-2 mb-2">
                      <p>ATTESTATO</p>
                      <?= $page->diploma()->kirbytext() ?>
                    </div>
                  <?php endif ?>
                
                </div>
              <?php endif */ ?>



              <?php if ($page->infoCorso()->toStructure()->count() > 0): ?>
                <div class="corso-info-item">
                  <p class="font-sans-s font-color-red mb-2">INFO</p>
                  <hr />
  
                  <?php foreach ($page->infoCorso()->toStructure() as $item): ?>
                    <div class="font-sans-ss mt-2 mb-2">
                      <p><?= $item->itemLabel()->upper() ?></p>
                      <?= $item->itemContent()->kt() ?>
                    </div>
                    <hr />
                  <?php endforeach ?>

                </div>
              <?php endif ?>








            </div>
            <?php 
              $docenti = new Pages();
              for($i = 1; $i <= 9; $i++){
                $field = "docente". $i;
                $dUid = $page->$field()->value();
                if($docente = page('la-scuola')->children()->findBy('uid', $dUid)){
                  if(!$docenti->has($docente)){
                    $docenti->add($docente);
                  }
                }
              }
              // $docenti->sortBy('title', 'asc');
            ?>
            <?php if($page->testoAlternativoDocenti()->value() != ""): ?>
              <div class="col-md-6 col-xl-8">
                <p class="font-sans-s font-color-red mb-2"><?= $labels["docenti"][$l]; ?></p>
                <hr />
                <div class="row">
                  <div class="docente-alt my-2 col-6 col-sm-12 col-xl-6 font-sans-ss"><?=$page->testoAlternativoDocenti()->kirbytext()?></div>
                </div>
              </div>
            <?php else: ?>

              <?php if($docenti->count() > 0): ?>
                <div class="col-md-6 col-xl-8">
                  <p class="font-sans-s font-color-red mb-2"><?= $labels["docenti"][$l]; ?></p>
                  <hr />
                  <div class="row">
                    <?php 
                      snippet('showcase-docenti', [
                        "docenti"       => $docenti,
                        "itemClass"     => "col-6 col-sm-12 col-xl-6",
                        "smaller"       => ($docenti->count() > 6),
                      ]); 
                    ?>
                  </div>
                </div>
              <?php endif ?>
              
            <?php endif ?>

          </div>
        </div>
      </div>
    </div>

  </main>

  <div id="modal-turni" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="modal" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
        
        <header class="d-flex justify-content-between align-items-center">
          <p class="font-color-gold font-sans-m"><?= $labels["modalitaAcq"][$l] ?></p>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        </header>
          
        <div class="modal-body">

          <?php $iTendina = 0; ?>
          <?php foreach($itemsBySubtitle as $subtitle => $subtitleItems): ?>
            <?= ($iTendina > 0) ? "<br/>" : "" ?>
            <span class="font-color-grey<?=(($iTendina>0) ? " pt-4" : "")?>"><?= $subtitle ?></span>
            <hr class="mb-1 " />
            <?php 
            foreach($subtitleItems as $item){
              if($item->template() == "corso-turno"){
                $articleeeeId = $page->corsoId()->value() .'~'. $item->uid();
                echo corsoTurnoPopup($item, $articleeeeId);
              } else {
                echo corsoLinkPopup($item);
              }
              $iTendina++;
              echo '<hr class="mb-1"/>';
            }
            ?>
          <?php endforeach ?>
          <br/>

        </div>
      </div>
    </div>
  </div>

  <script>
    function addToCartAndCloseModal(articleeeeId){
      handleEditCart('add', articleeeeId);
      $('#modal-turni').modal('hide');

      // force bg to disappear
      $('body').removeClass('modal-open');
      $('.modal-backdrop').remove();

    }
  </script>


<?php
$galleryItems = $page->galleryImages()->toStructure();
snippet('gallery-scripts');
snippet('gallery-corso', ["items" => $galleryItems]);
?>

<?php snippet('footer') ?>