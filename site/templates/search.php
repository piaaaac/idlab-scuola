<?php snippet('header') ?>

  <main class="main" role="main">
    
    <div class="container-fluid super-cont">
      <div class="row">

        <div class="col-sm-12 my-5">
          <p class="font-sans-m">
            
            <?php if(count($results) > 0): ?>
              <?= count($results) ?> risultati per <span class="font-sans-m font-color-gold"><?= $query ?></span>
            <?php else: ?>
              Nessun risultato per <span class="font-sans-m font-color-gold"><?= $query ?></span>
            <?php endif ?>

          </p>
        </div>

        <?php foreach($results as $result): ?>
          <div class="col-sm-6 col-lg-4 my-3">
            <p class="font-sans-s font-color-blue mb-1"><?= $result->template() ?></p> 
            <a href="<?= $result->url() ?>" class="font-super-ll">
              <?= $result->title()->html() ?>&nbsp;&rarr;
            </a>
          </div>
        <?php endforeach ?>

      </div>
    </div>

  </main>

<?php snippet('footer') ?>
