<?php 
$textColor = "black-text";
$bg = "";
if(true){
// if(rand(0, 10) > 5){
  $textColor = "white-text";
  $bg = $page->files()->shuffle()->first()->url();
}
$textColorString = [
  "black-text" => "",
  "white-text" => "font-color-white"
];

$discipline = $page->children()->visible()->not('corsi-speciali');
$spec = page('discipline/corsi-speciali');

?>

<?php snippet('header') ?>

  <main class="main-big-logo" role="main" style="
    background-image: url(<?= $bg ?>);
    background-position: center center;
    background-repeat: no-repeat;
    background-size: cover;
    ">

    <?php snippet('menu-big-logo', ["textColor" => $textColor]) ?>

    <div class="my-2"><br /></div>
      
    <?php snippet('breadcrumb-title', ["textColor" => $textColor]) ?>
    
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col d-float">

          <?php foreach($discipline as $item): ?>
            <a class="link-disciplina d-inline-block <?= $textColor ?>" href="<?= $item->url() ?>">
              <span class="font-supersuper-lll capolettera mr-1 font-color-blue"><?= $item->capolettera()->value() ?></span>
              <span class="font-super-ll text"><?= $item->title()->value() ?></span>
              <span class="font-super-ll mx-2">/</span>
            </a>
          <?php endforeach ?>

          <a class="link-disciplina d-inline-block" href="<?= $spec->url() ?>">
            <span class="font-supersuper-lll capolettera mr-1 font-color-gold"><?= $spec->capolettera()->value() ?></span>
            <span class="font-super-ll text font-color-gold"><?= $spec->title()->value() ?></span>
          </a>

        </div>
      </div>
  
      <div class="my-3"><br /></div>

      <div class="row">
        <div class="col-md-6">
          <div class="links-gold mb-4 <?= $textColorString[$textColor] ?>"><?= $page->testoCol1()->kirbytext() ?></div>
        </div>
        <div class="col-md-6">
          <div class="links-gold mb-4 <?= $textColorString[$textColor] ?>"><?= $page->testoCol2()->kirbytext() ?></div>
        </div>
      </div>

    </div>

  </main>

<?php snippet('footer') ?>