<?php

// debugger test
// include(kirby()->roots()->assets() .'/php/ChromePhp.php');
// ChromePhp::log('Hello console!');
// ChromePhp::log($_SERVER);
// ChromePhp::warn('For example, if something went wrong…');


if($page->uid() === "corsi-speciali"){
  $corsiSpecDisc = page('corsi')->children()->visible()->filterBy('disciplinaUid', "corsi-speciali"); 
  $corsiSpecTipo = page('corsi')->children()->visible()->filterBy('tipo', "speciali"); 
  $corsi = $corsiSpecDisc->merge($corsiSpecTipo)->sortBy('title', 'asc');
} else {
  $corsi = page('corsi')->children()->visible()->filterBy('disciplinaUid', $page->uid()); 
  $corsi240 = $corsi->filterBy('tipo', 'serale240')->sortBy('title', 'asc');
  $corsi60 = $corsi->filterBy('tipo', 'serale60')->sortBy('title', 'asc');
  $corsi30 = $corsi->filterBy('tipo', 'pomeridiano30')->sortBy('title', 'asc');
  $corsiSpeciali = $corsi->filterBy('tipo', 'speciali')->sortBy('title', 'asc');
  $corsiOfficine = $corsi->filterBy('tipo', 'officine')->sortBy('title', 'asc');
}

// ChromePhp::log($corsi->toArray());

$docenti = new Pages();
foreach ($corsi as $c) {
  for($i = 1; $i <= 6; $i++){
    $field = "docente". $i;
    $dUid = $c->$field()->value();
    if($docente = page('la-scuola')->children()->findBy('uid', $dUid)){
      if(!$docenti->has($docente)){
        $docenti->add($docente);
      }
    }
  }
}
// a::show($docenti);
// $docenti->sortBy('title', 'asc');
// a::show($docenti);

$docenteRappresentante = page('la-scuola')->children()->findBy('uid', $page->docenteRappresentante()->value());

// $lists = [
//   [
//     "title" => "CORSI"
//   ],
// ];


?>


<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">

        <div class="col-sm-8 col-md-6">

          <?php if($docenteRappresentante): ?>
            <a class="btn btn-large btn-primary font-sans-l font-color-blue" href="mailto:<?=$docenteRappresentante->email()->value()?>?subject=Richiesta informazioni <?=$page->title()->value()?>" role="button">
              INFORMATI <i class="fas fa-envelope d-inline-block ml-1"></i>
            </a>
          <?php else: ?>
            <a class="btn btn-large btn-primary font-sans-l font-color-blue disabled" href="#" role="button" disabled title="Nessun docente rappresentante">
              INFORMATI <i class="fas fa-envelope d-inline-block ml-1"></i>
            </a>
          <?php endif ?>

          <p class="my-1"><br /></p>
          <div class="text-sans-s pr-3 pr-sm-5"><?= $page->testo()->kirbytext() ?></div>
        </div>

        <div class="col-sm-4 col-md-6">
          <div class="row">
            <div class="col-12 col-xl-8 offset-xl-4">
              <?php
              if($page->imgDisciplina() && $image = $page->imgDisciplina()->toFile()){
                $imgUrl = $image->url();
              } else {
                $imgUrl = $site->url() ."/assets/images/fallback-sn.png";
              }
              ?>
              <img class="img-disciplina img-fluid" src="<?= $imgUrl ?>" />
            </div>
          </div>
        </div>

      </div>

      
      <?php if($page->uid() === "corsi-speciali"): ?>
        
        <!-- Disciplina corsi-speciali -->
        
        <?php
        $corsiChunks = $corsi->chunk(ceil($corsi->count()/2));
        $docentiChunks = $docenti->chunk(ceil($docenti->count()/2));
        ?>
  
        <div class="row">

          <div class="col-md">
            <span class="bordered-list font-color-blue">CORSI ATTIVI</span>
            <?php if($corsi->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>
            <?php if($corsiChunks->nth(0)->count() > 0): ?>
              <?php foreach($corsiChunks->nth(0) as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
            
          <div class="col-md">
            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <?php if($corsiChunks->nth(1)->count() > 0): ?>
              <?php foreach($corsiChunks->nth(1) as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>

          <div class="col-md">
            <span class="bordered-list font-color-red">DOCENTI</span>
            <?php if($docenti->count() == 0): ?>
              <span class="bordered-list">Nessun docente.</span>
            <?php endif ?>
            <?php if($docentiChunks->nth(0)->count() > 0): ?>
              <?php foreach($docentiChunks->nth(0) as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
            
          <div class="col-md">
            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <?php if($docentiChunks->nth(1)->count() > 0): ?>
              <?php foreach($docentiChunks->nth(1) as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>

        </div>

      <?php else: ?>

        <!-- Discipline normali -->


        <div class="row">
          <div class="col-md">

            <span class="bordered-list font-color-blue">CORSI ATTIVI</span>
            

            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsiAnnuali()->value() ?></span>

            <?php if($corsi240->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi240->count() > 0): ?>
              <?php foreach($corsi240 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
            
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsiOfficine()->value() ?></span>

            <?php if($corsiOfficine->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsiOfficine->count() > 0): ?>
              <?php foreach($corsiOfficine as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>



          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi60()->value() ?></span>

            <?php if($corsi60->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi60->count() > 0): ?>
              <?php foreach($corsi60 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
            
          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi30()->value() ?></span>

            <?php if($corsi30->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi30->count() > 0): ?>
              <?php foreach($corsi30 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-gold"><?= page("corsi")->titoloCorsiSpeciali()->value() ?></span>

            <?php if($corsiSpeciali->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsiSpeciali->count() > 0): ?>
              <?php foreach($corsiSpeciali as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

          </div>
        </div>

        <!-- docenti -->
        <div class="row">
          <div class="col-md-3">
            <span class="bordered-list font-color-red">DOCENTI</span>
            <?php if($docenti->count() == 0): ?>
              <span class="bordered-list">Nessun docente.</span>
            <?php endif ?>
            <?php if($docenti->count() > 0): ?>
              <?php foreach($docenti as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
          </div>
        </div>

      <?php endif ?>

    </div>
  </main>

<?php snippet('footer') ?>