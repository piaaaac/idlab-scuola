<?php
$valori = $page->children()->visible();
?>

<?php snippet('header') ?>

  <main class="main" role="main" id="pagina-manifesto">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-sm-6 testo">
          <?= $page->testoCol1()->kirbytext() ?>
        </div>
        <div class="col-sm-6 testo">
          <?= $page->testoCol2()->kirbytext() ?>

          <h1 class="mb-2 mt-5">I nostri valori</h1>
          <?php $i = 1; ?>
          <?php foreach ($valori as $item): ?>
            <h1 class="bordered-top">
              <a href="#<?= $item->uid() ?>" data-toggle="collapse"><?= $i ." ". $item->title()->value() ?></a>
            </h1>
            <div class="collapse" id="<?= $item->uid() ?>">
              <?= $item->text()->kirbytext() ?>
            </div>
            <?php $i++; ?>
          <?php endforeach ?>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>