<?php
$docs = $page->files()->filterBy('type', '==', 'document');
$n = $docs->count();
$half = ceil($n/2);
$docsCol1 = $docs->slice(0, $half);
$docsCol2 = $docs->slice($half + 1);
?>

<?php snippet('header') ?>

  <main class="main" role="main" id="pagina-info">

    <?php snippet('breadcrumb-title') ?>

    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">

      <div class="row">

        <div class="col-sm-12">
          <p class="font-size-m font-color-red"><?= $page->titleFormat()->upper() ?></p>
          <!-- <hr class="mt-2 mb-4" /> -->
        </div>

        <?php
        $items = $page->formatCorsi()->toStructure();
        foreach($items as $item){
          $hasTable = $item->formatlines()->isNotEmpty();
          $lines = str::lines($item->formatlines()->value());
          ?>
          <div class="col-sm-12">
            <hr class="mb-4 mt-2" />
          </div>
          <div class="<?= $hasTable ? "col-lg-2" : "col-lg-6" ?>">
            <h1 class="mb-5"><?= $item->formattitle()->value() ?></h1>
          </div>
          <?php if($hasTable): ?>
          <div class="col-lg-6 format-table mb-5">
            <!-- <p>FORMAT</p> -->
            <?php $i = -1; ?>
            <?php foreach($lines as $line): ?>
              <?php
              $lineLabel = str::split($line, "$$")[0];
              $lineText = str::split($line, "$$")[1];
              $i++;
              ?>
              <?php if($i > 0): ?>
                <hr class="thin mt-1 mb-2" />
              <?php endif ?>
              <div class="d-flex justify-content-start align-items-center flex-nowrap">
                <p class="small-title font-sans-sss"><?= $lineLabel ?></p><p class="value"><?= $lineText ?></p>
              </div>
            <?php endforeach ?>
          </div>
          <?php endif ?>
          <div class="col-lg-4 mb-4">
            <p class="font-sans-s"><?= $item->formattesto()->value() ?></p>
            <div class="d-flex justify-content-end">
              <a class="btn btn-primary mt-4" href="<?= $item->formatbtnurl()->toUrl() ?>" role="button"><?= $item->formatbtntxt()->upper() ?> &rarr;</a>
            </div>
          </div>
        <?php } ?>
      </div>
      
      <div class="my-2"><br /></div>

      <?php /* CORONAVIRUS ------------------------------------

      <div class="row arrows" onclick="javascript:scrollToId('pagina-info-calendario');">
        <div class="col-12 d-flex justify-content-around">
          <span>&darr;</span>
          <span>&darr;</span>
          <span>&darr;</span>
          <span class="d-none d-sm-block">&darr;</span>
          <span class="d-none d-md-block">&darr;</span>
          <span class="d-none d-md-block">&darr;</span>
        </div>
      </div>
      ------------------------------------------------------ */?>

    </div>

    <?php /* CORONAVIRUS ------------------------------------
    
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">

      <div class="row" id="pagina-info-calendario">

        <div class="col-sm-12">
          <p class="font-size-m font-color-red"><?= $page->titleCalendario()->upper() ?></p>
          <hr class="mt-2 mb-4" />
        </div>
      
        <div class="col-sm-12">
          <?php
          if($page->imgCalendarioSm() && $image = $page->imgCalendarioSm()->toFile()){
            $imgCalSm = $image->url();
          } else kill("Immagine calendario SM mancante o non valida.");
          if($page->imgCalendarioMd() && $image = $page->imgCalendarioMd()->toFile()){
            $imgCalMd = $image->url();
          } else kill("Immagine calendario MD mancante o non valida.");
          if($page->imgCalendarioXl() && $image = $page->imgCalendarioXl()->toFile()){
            $imgCalXl = $image->url();
          } else kill("Immagine calendario XL mancante o non valida.");
          ?>
          <img class="img-fluid d-block d-sm-none" src="<?= $imgCalSm ?>" />
          <img class="img-fluid d-none d-sm-block d-lg-none" src="<?= $imgCalMd ?>" />
          <img class="img-fluid d-none d-lg-block" src="<?= $imgCalXl ?>" />
          <hr class="mt-4 mb-4" />
        </div>
      
      </div>

      <div class="my-2"><br /></div>

    </div>
    ------------------------------------------------------ */?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">

      <div class="row" id="pagina-info-iscrizioni">

        <div class="col-sm-12">
          <p class="font-size-m font-color-red"><?= $page->titleIscrizioni()->upper() ?></p>
          <hr class="mt-2 mb-4" />
        </div>

        <div class="col-sm-8 offset-sm-2">
          <p class="font-sans-s"><?= $page->testoIscrizioni()->kirbytext() ?></p>
        </div>
      </div>
    </div>
      
    <div class="my-2"><br /></div>

  </main>

<?php snippet('footer') ?>