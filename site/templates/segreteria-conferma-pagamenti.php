<?php
snippet('logincheck-block-non-admin');
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');
date_default_timezone_set('Europe/Rome');

$mode = "mixed"; // mixed|grouped
if(isset($_GET["mode"])){ 
  $mode = $_GET["mode"];
}




// $allSortedOrdini = page("segreteria-ordini")->children()->sortBy("dataOraPagamento", "desc", "dataOraVerifica",  "desc", "form_dataOraOrdine", "desc");

$allOrdini = page("segreteria-ordini")->children()->map(function($item) {
  $datetime1 = DateTime::createFromFormat('Y-m-d H:i:s', $item->form_dataOraOrdine()->value());
  $datetime2 = DateTime::createFromFormat('Y-m-d H:i:s', $item->dataOraVerifica()->value());
  $datetime3 = DateTime::createFromFormat('Y-m-d H:i:s', $item->dataOraPagamento()->value());

  if (!($datetime1 || $datetime2 || $datetime3)) {
    kill("L'ordine '". $item->uid() ."' non ha data. Si prega di verificare.");
  }

  $mostRecentDate = $datetime1;
  if($datetime2 && $datetime2 > $datetime1){
    if($datetime3 && $datetime3 > $datetime2){
      $mostRecentDate = $datetime3;
    }
    $mostRecentDate = $datetime2;
  }
  $item->mostRecentDate = $mostRecentDate->format('Y-m-d H:i:s');;
  return $item;
});
$allSortedOrdini = $allOrdini->sortBy('mostRecentDate', 'desc');



$ordiniNew =    $allSortedOrdini->filterBy("pagamentoOk", "0")->filterBy("emailVerificata", "0");
$ordiniOpen =   $allSortedOrdini->filterBy("pagamentoOk", "0")->filterBy("emailVerificata", "1");
$ordiniPayed =  $allSortedOrdini->filterBy("pagamentoOk", "1");
// $ordiniNew =    $ordiniNew->sortBy("form_dataOraOrdine", "desc");
// $ordiniOpen =   $ordiniOpen->sortBy("dataOraVerifica",  "desc", "form_dataOraOrdine", "desc");
// $ordiniPayed =  $ordiniPayed->sortBy("dataOraPagamento", "desc", "form_dataOraOrdine", "desc");

function dateFormat($ordine){
  $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $ordine->form_dataOraOrdine()->value());
  return $myDateTime->format('d/m/Y');
}
function dateFormatStr($str){
  $myDateTime = DateTime::createFromFormat('Y-m-d H:i:s', $str);
  return $myDateTime->format('d/m/Y');
}

?>

<?php snippet('header', ["hideMenu" => true]) ?>

  <script>
    $(document).ready(function(){
      $(function () {
        $('[data-toggle="tooltip"]').tooltip()
      })
      $('#modalConferma').on('show.bs.modal', function (event) {
        console.log(event);
        var button = $(event.relatedTarget); // Button that triggered the modal
        var orderid = button.data('orderid'); // Extract info from data-* attributes
        var modal = $(this);
        modal.find('.modal-field-orderid').text(orderid);

        function updateAction(){
          var method = $("#method-select").val();
          var action = "<?= page('iscrizione3segreteria')->url() ?>/order:" + orderid + "/method:" + method
          $('#paymentConfirmation').attr('action', action);
          console.log("form action updated", action);
        }

        updateAction();

        $("#method-select").change(function() {
          updateAction();
        });
      });
    });
  </script>

  <main class="main pagine-segreteria pt-5" role="main" id="pagina-segreteria-conferma-pagamenti">

    <div class="container-fluid super-cont s-c-admin">

      <div class="row">
        <div class="col-12 mb-4">
          <a href="?mode=mixed" class="btn btn-primary btn-small <?=
            (($mode === "mixed") ? "black" : "black-light") 
          ?>">LISTA UNICA</a>
          <a href="?mode=grouped" class="btn btn-primary btn-small <?=
            (($mode === "grouped") ? "black" : "black-light") 
          ?> ml-2">RAGGRUPPATI PER STATO</a>
        </div>
      </div>

      <?php if($mode === "mixed"): ?>
        
        <div class="row mt-4">
          <div class="col">

            <p class="my-2 pt-1">
              <span class="d-flex justify-content-between align-items-center">
                <span class="font-sans-m flex-grow-1">Tutti gli ordini</span>
                <?php if($allSortedOrdini->count() == 0): ?>
                  Nessun ordine.
                <?php else: ?>
                  <span>(<?= $allSortedOrdini->count() ?>)</span>
                <?php endif ?>
              </span>
            </p>
            <hr />
            <div class="mb-5 pb-2" id="list-all">
              <?php $processedOrders = 0; ?>
              
              <?php foreach ($allSortedOrdini as $ordine): ?>
                <?php snippet('segreteria-item-ordine', [
                  "ordine" => $ordine, 
                  "returnMode" => $mode,
                  "displayDate" => dateFormatStr($ordine->mostRecentDate), 
                  "processedOrders" => $processedOrders
                ]) ?>
              <?php endforeach ?>
            </div>
          </div>
        </div>

      <?php elseif($mode === "grouped"): ?>

        <div class="row mt-4">
          <div class="col">

            <p class="my-2 pt-1">
              <a href="#list-new" class="no-hover d-flex justify-content-between align-items-center" data-toggle="collapse">
                <span class="badge badge-new mr-2">&nbsp;</span>
                <span class="font-sans-m flex-grow-1">NEW — Email non verificata</span>
                <?php if($ordiniNew->count() == 0): ?>
                  Nessun ordine.
                <?php else: ?>
                  <span>(<?= $ordiniNew->count() ?>) &darr;</span>
                <?php endif ?>
              </a>
            </p>
            <hr />
            <div class="collapse mb-5 pb-2" id="list-new">
              <?php $processedOrders = 0; ?>
              
              <?php foreach ($ordiniNew as $ordine): ?>
                <?php snippet('segreteria-item-ordine', [
                  "ordine" => $ordine, 
                  "returnMode" => $mode,
                  "displayDate" => dateFormatStr($ordine->mostRecentDate), 
                  "processedOrders" => $processedOrders
                ]) ?>
              <?php endforeach ?>
            </div>

            <p class="my-2 pt-1 mt-5">
              <a href="#list-open" class="no-hover d-flex justify-content-between align-items-center" data-toggle="collapse">
                <span class="badge badge-open mr-2">&nbsp;</span>
                <span class="font-sans-m flex-grow-1">OPEN — In attesa di pagamento</span>
                <?php if($ordiniOpen->count() == 0): ?>
                  Nessun ordine.
                <?php else: ?>
                  <span>(<?= $ordiniOpen->count() ?>) &darr;</span>
                <?php endif ?>
              </a>
            </p>
            <hr />
            <div class="collapse mb-5 pb-2" id="list-open">
              <?php $processedOrders = 0; ?>
              
              <?php foreach ($ordiniOpen as $ordine): ?>
                <?php snippet('segreteria-item-ordine', [
                  "ordine" => $ordine, 
                  "returnMode" => $mode,
                  "displayDate" => dateFormatStr($ordine->mostRecentDate), 
                  "processedOrders" => $processedOrders
                ]) ?>
              <?php endforeach ?>
            </div>

            <p class="my-2 pt-1 mt-5">
              <a href="#list-payed" class="no-hover d-flex justify-content-between align-items-center" data-toggle="collapse">
                <span class="badge badge-payed mr-2">&nbsp;</span>
                <span class="font-sans-m flex-grow-1">PAYED — Pagati</span>
                <?php if($ordiniPayed->count() == 0): ?>
                  Nessun ordine.
                <?php else: ?>
                  <span>(<?= $ordiniPayed->count() ?>) &darr;</span>
                <?php endif ?>
              </a>
            </p>
            <hr />
            <div class="collapse mb-5 pb-2 show" id="list-payed">
              <?php $processedOrders = 0; ?>

              <?php foreach ($ordiniPayed as $ordine): ?>
                <?php snippet('segreteria-item-ordine', [
                  "ordine" => $ordine, 
                  "returnMode" => $mode,
                  "displayDate" => dateFormatStr($ordine->mostRecentDate),
                  "processedOrders" => $processedOrders
                ]) ?>
              <?php endforeach ?>
            </div>
          </div>
        </div>

      <?php endif ?>

    </div>
  </main>

  <div class="modal fade" id="modalConferma" tabindex="-1" role="dialog" aria-labelledby="modalConfermaLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
        <form id="paymentConfirmation" name="paymentConfirmation" method="get" action="">
        
          <div class="modal-header">
            <strong class="modal-title" id="modalConfermaLabel">Conferma di pagamento ordine <span class="modal-field-orderid"></span></strong>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        
          <div class="modal-body">
            <p class="my-2">Utente: <?= $site->user()->firstname() ." ". $site->user()->lastname() ?></p>
            <p class="my-3">
               
              <label for="method-select">Metodo:</label>

              <select id="method-select" name="method-select">
                <option value="contanti" selected>Contanti</option>
                <option value="bonifico">Bonifico bancario</option>
                <option value="pos">POS</option>
                <option value="assegno">Assegno</option>
                <option value="paypal">Paypal</option>
              </select>

            </p>
            <div class="form-group">
              <textarea class="form-control" id="message-text" name="message-text" placeholder="Note relative al pagamento" id="modal-notes-textarea"></textarea>
            </div>
          </div>
        
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-small" data-dismiss="modal">Annulla</button>
            <button type="submit" class="btn btn-primary blue btn-small">Conferma pagamento&nbsp;&rarr;</button>
          </div>
        
        </form>
      </div>
    </div>
  </div>


<?php snippet('footer', ["hideFooter" => true]) ?>


