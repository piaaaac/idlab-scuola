<?php
$items = $page->children()->visible()->sortBy("sort", "asc");
$fallbackCoverUrl = $site->url() ."/assets/images/fallback-cover.png";
?>

<?php snippet('header') ?>

  <main class="main" role="main" id="pagina-orientamento">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-sm-8">
          <div class="links-gold text-sans-s pr-3 pr-sm-5">
            <?= $page->text()->kt() ?>
          </div>
        </div>
      </div>
    </div>

    <div class="spacer my-5"></div>
    
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <p class="font-sans-s font-color-red">TUTTE LE DISCIPLINE</p>
          <hr class="mb-3 mt-2">
        </div>
      </div>
      <?php foreach ($items as $item): 
        $imageUrl = ($item->cover() && $im = $item->cover()->toFile()) ? $im->url() : $fallbackCoverUrl;
        $disciplina = page("discipline/". $item->dpage()->value()) ?? null;
        if (!$disciplina) {
          kill("error: disciplina '". $item->dpage() ."'' not found.");
        }
        $corsi = page('corsi')->children()->visible()->filterBy('disciplinaUid', $disciplina->uid()); 
        $galleryId = "disciplina-". $disciplina->uid();
        ?>

        <div class="disciplina" data-id="<?= $disciplina->uid() ?>">
          <div class="row">
            <div class="col-12">
              <h2 class="font-super-ll">
                <?= $item->title()->value() ?>
              </h2>
            </div>

            <div class="col-12 spacer my-2"></div>
            
            <div class="col-md-6 col-lg-4">
              <div class="cover img-framed" style="background-image: url('<?= $imageUrl ?>');"></div>
              <div class="section-gallery" id="<?= $galleryId ?>">
                <?php if ($item->galleryImages()->value()): ?>
                  <h4 class="font-sans-s font-color-blue">GALLERY</h4>
                  <hr class="mb-3 mt-2">
                  <div class="row">
                  <?php $i = 0; ?>
                  <?php foreach($item->galleryImages()->toStructure() as $gitem): ?>
                    <?php if($gitem->giimage() && $image = $gitem->giimage()->toFile()): ?>
                      <div class="col-6 col-md-4 mb-4 gallery-image">
                        <a href="<?= "javascript:openGallery($i);" ?>">
                          <img class="img-fluid w-100" src="<?= $image->url() ?>"/>
                        </a>
                      </div>
                    <?php endif ?>
                    <?php $i++; ?>
                  <?php endforeach ?>
                  </div>
                  <?php
                  $galleryItems = $item->galleryImages()->toStructure();
                  snippet('gallery-corso', ["items" => $galleryItems, "id" => $galleryId]);
                endif ?>
              </div>
            </div>
            <div class="col-md-6 col-lg-4">
              <div class="section-text-intro mb-4">
                <h4 class="font-sans-s font-color-blue">COS'&Egrave;</h4>
                <hr class="mb-3 mt-2">
                <div class="">
                  <?php
                  $kktt = $item->text();
                  if ($item->textFull()->isNotEmpty()) {
                    $kktt .= " <a class='font-color-black30 hover-black pointer' onclick='toggleDisciplina(\"". $disciplina->uid() ."\");'>Continua</a>";
                  }
                  ?>
                  <?= kirbytext($kktt) ?>
                </div>
              </div>
              <div class="section-text-full mb-4">
                <h4 class="font-sans-s font-color-blue">COS'&Egrave;</h4>
                <hr class="mb-3 mt-2">
                <div class=""><?= $item->textFull()->kt() ?></div>
              </div>
            </div>
            <div class="col-md-6 col-lg-4">
              <div class="section-corsi-intro">
                <div class="flexitem-corsi">
                  <h4 class="font-sans-s font-color-blue">
                    <?= $corsi->count() > 0 
                      ? $corsi->count() ." CORSI ATTIVI"
                      : "NESSUN CORSO ATTIVO";
                    ?>
                  </h4>
                  <hr class="mt-2">
                  <?php if ($corsi->count() > 0): 
                    $maxCorsiIntro = 4;
                    $corsiIntroI = 0;
                    foreach($corsi as $item): ?>
                      <?php 
                      $corsiIntroI++;
                      if ($corsiIntroI > $maxCorsiIntro): ?>
                        <a class="bordered-list font-color-black30 pointer hover-black no-arrow" onclick="toggleDisciplina('<?= $disciplina->uid() ?>');">
                          Altri <?= $corsi->count() - $corsiIntroI + 1 ?>&hellip;
                        </a>
                        <?php 
                        break;
                      endif ?>
                      <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                    <?php endforeach ?>
                    <div class="border-bottom"></div>
                  <?php endif ?>
                </div>
                <div class="flexitem-btn">
                  <div class="d-flex justify-content-end my-3">
                    <a class="btn btn-primary section-btn-open" onclick="toggleDisciplina('<?= $disciplina->uid() ?>');">LEGGI TUTTO &darr;</a>
                  </div>
                </div>
              </div>
              <div class="section-corsi-full">
                <h4 class="font-sans-s font-color-blue">
                  <?= $corsi->count() > 0 
                    ? $corsi->count() ." CORSI ATTIVI"
                    : "NESSUN CORSO ATTIVO";
                  ?>
                </h4>
                <hr class="mt-2">
                <?php if ($corsi->count() > 0): ?>
                  <?php foreach($corsi as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                  <div class="border-bottom"></div>
                <?php endif ?>
              </div>
            </div>
            <div class="col-12">
              <a class="btn btn-primary section-btn-close w-100 my-3" onclick="toggleDisciplina('<?= $disciplina->uid() ?>');">CHIUDI &uarr;</a>
            </div>
            <div class="col-12 mb-3">
              <hr />
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>

  </main>

  <?php
  snippet('gallery-scripts');
  ?>

  <script>
    function toggleDisciplina (id) {
      disciplina = $(".disciplina[data-id='" + id + "']");
      var isOpening = !disciplina.hasClass("expanded");
      $(".disciplina").removeClass("expanded");
      if (isOpening) {
        var galleryId = "disciplina-"+ id;
        initGallery(galleryId); // from gallery-scripts
        disciplina.addClass("expanded");
      }
    }
  </script>

<?php snippet('footer') ?>

