<?php
$perPage = 24;
$items = $page->files()->sortBy("sort", "asc")->paginate($perPage);
$pagination = $items->pagination();
$from = ($pagination->page() - 1) * $perPage + 1;
$to = $from + $perPage - 1;
$n = $page->files()->count();
if($to > $n){ $to = $n; }

function navigation($pgg, $from, $to, $n){ ?>
    <div class="row pagination">
      <div class="col-12 d-flex justify-content-between">
        <?php if($pgg->hasPrevPage()): ?>
        <a class="" href="<?= $pgg->prevPageURL() ?>">&larr; <span class="d-none d-sm-inline">Precedenti</span></a>
        <?php else: ?>
        <span class="font-color-black30">&larr; <span class="d-none d-sm-inline">Precedenti</span></span>
        <?php endif ?>

        <span class=""><?= "$from — $to di $n" ?></span>

        <?php if($pgg->hasNextPage()): ?>
        <a class="" href="<?= $pgg->nextPageURL() ?>"><span class="d-none d-sm-inline">Successive</span> &rarr;</a>
        <?php else: ?>
        <span class="font-color-black30"><span class="d-none d-sm-inline">Successive</span> &rarr;</span>
        <?php endif ?>
      </div>
    </div>
<?php }

?>

<?php snippet('header') ?>

<main class="main" role="main">
  
  <?php snippet('breadcrumb-title'); ?>
  
  <div class="my-2"><br /></div>

  <div class="container-fluid super-cont">
    
    <div class="row">
      <div class="col-12 col-lg-8 mt-2 mb-5">
        <p class="font-sans-s d-inline-block no-underline"><?= $page->text()->kt() ?></p>
      </div>
    </div>

    <?php navigation($pagination, $from, $to, $n); ?>
    <div class="row">
      <div class="col-12">
        <hr class="mb-3 mt-2" />
      </div>
    </div>

    <div class="row">
      <?php 
      $i = 0;
      foreach($items as $item):
        $url = $item->crop(300, 300, 70)->url();
        ?>

        <div class="col-6 col-sm-4 col-md-3 col-lg-2 my-3 gallery-image">
          <a href="<?= "javascript:openGallery($i);" ?>">
            <img class="img-fluid" src="<?= $url ?>"/>
          </a>
        </div>
      
        <?php
        $i++;
      endforeach ?>
    </div>

    <div class="row">
      <div class="col-12">
        <hr class="mb-3 mt-3" />
      </div>
    </div>
    <?php navigation($pagination, $from, $to, $n); ?>

  </div>
</main>

<?php
$galleryItems = $page->galleryImages()->toStructure();
snippet('gallery-scripts', [
  "itemsForSaving" => $items,
]);
snippet('gallery-gallery-item', [
  "items" => $items, 
  "startIndex" => $from, 
  "total" => $page->files()->count(),
]);
?>

<?php snippet('footer') ?>

