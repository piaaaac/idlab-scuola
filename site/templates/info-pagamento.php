<?php
$urlBack = param("order")
  ? page("iscrizione2")->url() ."/order:". param("order")
  : page("homepage");
?>

<?php snippet('header') ?>

  <main class="main" role="main">
      
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col pt-3">
          
          <?= $page->text()->kirbytext() ?>

          <p class="my-3"><br /></p>

          <a class="btn btn-primary" href="<?= $urlBack ?>" role="button">OK</a>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>
