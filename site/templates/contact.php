<?php snippet('header') ?>

  <main class="main" role="main">
      
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col">
          <?= $page->text()->kirbytext() ?>
        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>
