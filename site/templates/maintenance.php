<?php 
// redirect home if here by mistake (not maintenance any more)
if($site->maintenance()->value() == "false"){
  go("/");
}

// redirect home if user admin or maintenance
if($user = $site->user()){
  if($user->hasRole('admin') || $user->hasRole('maintenanceNormalUser')){
    go("/");
  } 
}

?>

<!DOCTYPE html>
<html>
  <head>
    <title>SUPER Maintenance</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
      <meta name="description" content="SUPER Maintenance">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <style type="text/css">
      html, body{
        background-color: #F2F0ED;
      }
      img{
        max-width: 100%;
        width: 420px;
      }
    </style>
  </head>
  <body>
    <div class="container-fluid">
      <div class="row">
        <div class="col">
          <img src="assets/images/manutenzione.png">
        </div>
      </div>
    </div>
  </body>
</html>
