<?php 
include_once(kirby()->roots()->snippets() .'/commonfunctions.php'); // for newsDate
?>

<?php snippet('header') ?>

  <main class="main pb-0" role="main">

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-12">
          <p class="font-super-ll mb-5"><?= page('newsbox')->title()->value() ?></p>
        </div>

        <div class="col-sm-10 offset-sm-1 col-md-8 offset-md-2">
          <div class="text-center"><?= newsDate($page) ?></div>
          <p class="font-sans-lll font-color-red mt-2 mb-5"><?= $page->title()->value() ?></p>
          <div class="news-body">
            <?= $page->testo()->kirbytext() ?>
          </div>
        </div>
      </div>
    </div>

    <!--
    <div class="bg-white pb-2">
      <div class="container-fluid super-cont mb-4 mt-5 pt-3" id="showcase-news-anchor">
        <div class="row">
          <div class="col-12 d-flex justify-content-between align-items-center">
            <span class="font-sans-s font-color-red"><?= page('newsbox')->title()->upper() ?></span>
            <a class="font-sans-sss ml-5" href="<?= page('newsbox')->url() ?>">&rarr; ARCHIVIO</a>
          </div>
        </div>
      </div>
      <?php /*snippet('showcase-news', ["limit" => 3, "exclude" => $page->uid()])*/ ?>
    </div>

    <div class="py-3 bg-white" />
    -->

    <!-- COSA SUCCEDE -->
    <div class="container-fluid super-cont mb-4 mt-4 pt-5">
      <div class="row">
        <div class="col-12">
          <span class="font-super-ll"><?= page('newsbox')->title()->value() ?></span>
        </div>
      </div>
    </div>

    <?php snippet('showcase-news', ["limit" => 9, "exclude" => $page->uid()]) ?>

  </main>

<?php snippet('footer') ?>