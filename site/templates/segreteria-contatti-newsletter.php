<?php
snippet('logincheck-block-non-admin');
snippet('commonfunctions');

$allContacts = page("newsletter-contacts")->children();

?>

<?php snippet('header', ["hideMenu" => true]) ?>

  <main class="main pagine-segreteria pt-0 pb-0" role="main" id="pagina-segreteria-contatti-newsletter">

    <div class="container-fluid super-cont s-c-admin table-wrapper">
      <table class="outer mr-5"><tr>

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">EMAIL</th></tr>
              <?php foreach($allContacts as $c): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3"><?= $c->title()->value() ?>&nbsp;</td></tr>
              <?php endforeach ?>
            </table>
          </td>

          <td>
            <table class="inner">
              <tr><th class="text-nowrap header pb-2 pr-3">DATA ISCRIZIONE</th></tr>
              <?php foreach($allContacts as $c): ?>
                <tr><td class="text-nowrap font-sans-ss data pr-3">
                  <?= dateTimeFormatted($c->submissionDate()->value()) ?>&nbsp;
                </td></tr>
              <?php endforeach ?>
            </table>
          </td>

          <td>
            <table class="spacer"><tr><th class="text-nowrap px-4">&nbsp;</th></tr></table>
          </td>  

      </tr></table>

      <?php if(count($allContacts) == 0): ?>
        Nessun contatto da mostrare.
      <?php endif ?>
        
    </div>
  </main>

<?php snippet('footer', ["hideFooter" => true]) ?>


