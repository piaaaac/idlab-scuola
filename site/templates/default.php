<?php
/*
function randomIcon(){
  $iconNames = [
    "bullhorn",
    "bullhorn",
    "glass-martini-alt",
    "grin-hearts",
    "thumbs-up",
    "crow"];
  $i = rand(0, count($iconNames) - 1);
  return $iconNames[$i];
}
*/
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row mb-4">
        <div class="col-lg-8">

          <div class="frame-images text-sans-s mb-3">
            <?= $page->text()->kirbytext() ?>
          </div>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>