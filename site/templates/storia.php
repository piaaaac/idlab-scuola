<?php snippet('header') ?>

  <main class="main" role="main" id="pagina-storia">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col-md-4 col-lg-6 testo col1 d-none d-md-block">
          <?= $page->testoCol1()->kirbytext() ?>
        </div>
        <div class="col-md-7 offset-md-1 offset-lg-0 col-lg-6 testo col2">
          <?= $page->testoCol2()->kirbytext() ?>
          <a class="btn btn-primary my-4 respect-margins" href="<?= $page->files()->find('scuola_del_castello.pdf')->url() ?>" role="button">APPROFONDISCI &rarr;</a>
        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>

<?php exit(); /*?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="container-fluid super-cont">
      <?php foreach($page->children() as $section): ?>
        <div class="row">
          <div class="col-4">
            <?= $section->title()->kirbytext() ?>
          </div>
          <div class="col-8">
            <?= $section->text()->kirbytext() ?>
          </div>
        </div>
      <?php endforeach ?>
    </div>

  </main>

<?php snippet('footer') */ ?>