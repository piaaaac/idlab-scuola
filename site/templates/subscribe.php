<?php 
a::show($post);
?>


<?php snippet('header') ?>

  <main class="main" role="main">
    
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col">

          <h1><?= $page->title()->value() ?></h1>

          <div class="my-2">&nbsp;</div>

          <?php if($success): ?>
            <p><?= $page->textSuccess()->kirbytext() ?></p>
          <?php else: ?>
            <p><?= $page->textFailure()->kirbytext() ?></p>
            <p><?= $error ?></p>
          <?php endif ?>

          <div class="my-2">&nbsp;</div>

          <a class="btn btn-primary" href="<?=page('homepage')->url()?>" role="button">
            OK
          </a>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>