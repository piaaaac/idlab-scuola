<?php snippet('header') ?>

  <main class="main" role="main">
    
    <div class="container-fluid super-cont">
      <div class="row">
        <div class="col">

          <p>
            <?= kirbytext($msg) ?>
          </p>

          <a class="btn btn-primary" href="<?= $linkDestination ?>" role="button">
            <?= $linkText ?>
          </a>

        </div>
      </div>
    </div>

  </main>

<?php snippet('footer') ?>