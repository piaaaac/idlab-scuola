<?php
  
  $corsi = new Pages();
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente1', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente2', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente3', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente4', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente5', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente6', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente7', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente8', $page->uid())); 
  $corsi->add(page('corsi')->children()->visible()->filterBy('docente9', $page->uid())); 

  // $corsi240 = $corsi->filterBy('tipo', 'serale240')->sortBy('title', 'asc');
  // $corsi60 = $corsi->filterBy('tipo', 'serale60')->sortBy('title', 'asc');
  // $corsi30 = $corsi->filterBy('tipo', 'pomeridiano30')->sortBy('title', 'asc');
  // $corsiSpeciali = $corsi->filterBy('tipo', 'speciali')->sortBy('title', 'asc');
  // $corsiOfficine = $corsi->filterBy('tipo', 'officine')->sortBy('title', 'asc');

  $pc = page("corsi");

  $col1ATipo = $pc->dynamicCol1ATipo()->isNotEmpty() ? $pc->dynamicCol1ATipo()->value() : null;
  $col2ATipo = $pc->dynamicCol2ATipo()->isNotEmpty() ? $pc->dynamicCol2ATipo()->value() : null;
  $col3ATipo = $pc->dynamicCol3ATipo()->isNotEmpty() ? $pc->dynamicCol3ATipo()->value() : null;
  $col4ATipo = $pc->dynamicCol4ATipo()->isNotEmpty() ? $pc->dynamicCol4ATipo()->value() : null;
  $col1BTipo = $pc->dynamicCol1BTipo()->isNotEmpty() ? $pc->dynamicCol1BTipo()->value() : null;
  $col2BTipo = $pc->dynamicCol2BTipo()->isNotEmpty() ? $pc->dynamicCol2BTipo()->value() : null;
  $col3BTipo = $pc->dynamicCol3BTipo()->isNotEmpty() ? $pc->dynamicCol3BTipo()->value() : null;
  $col4BTipo = $pc->dynamicCol4BTipo()->isNotEmpty() ? $pc->dynamicCol4BTipo()->value() : null;

  $corsi1A = ($col1ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col1ATipo)->sortBy('title', 'asc');
  $corsi2A = ($col2ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col2ATipo)->sortBy('title', 'asc');
  $corsi3A = ($col3ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col3ATipo)->sortBy('title', 'asc');
  $corsi4A = ($col4ATipo === null) ? new Pages() : $corsi->filterBy('tipo', $col4ATipo)->sortBy('title', 'asc');
  $corsi1B = ($col1BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col1BTipo)->sortBy('title', 'asc');
  $corsi2B = ($col2BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col2BTipo)->sortBy('title', 'asc');
  $corsi3B = ($col3BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col3BTipo)->sortBy('title', 'asc');
  $corsi4B = ($col4BTipo === null) ? new Pages() : $corsi->filterBy('tipo', $col4BTipo)->sortBy('title', 'asc');

  $corsi1ACount = $corsi1A->count();
  $corsi2ACount = $corsi2A->count();
  $corsi3ACount = $corsi3A->count();
  $corsi4ACount = $corsi4A->count();
  $corsi1BCount = $corsi1B->count();
  $corsi2BCount = $corsi2B->count();
  $corsi3BCount = $corsi3B->count();
  $corsi4BCount = $corsi4B->count();

  $image = $page->hasImages() 
    ? $page->images()->first()->url()
    : kirby()->urls()->assets() ."/images/defaults/docente-xxx.png";

?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">
      <div class="row">

        <div class="col-sm-8 col-md-6">
          <?php if(!$page->showContactBtn()->bool()): ?>
            <a class="btn btn-large btn-primary font-sans-l font-color-blue" href="mailto:<?= $page->email()->value() ?>" role="button">
              SCRIVI A <?= strtoupper($page->nome()->value()) ?> <i class="fas fa-envelope d-inline-block ml-1"></i>
            </a>
          <?php endif ?>
          <p class="my-1"><br /></p>
          <div class="text-sans-s links-gold pr-3 pr-sm-5"><?= $page->testo()->kirbytext() ?></div>
        </div>

        <div class="col-sm-4 col-md-6">
          <div class="row">
            <div class="col-6 col-sm-12 col-md-8 offset-md-4">
              <img class="img-disciplina img-fluid" src="<?= $image ?>" />
            </div>
          </div>
        </div>
      
      </div>
      

  
      <?php /* CORONAVIRUS ------------------------------------

      <?php if($page->tipo()->value() === "docente"): ?>
        <div class="row">
          <div class="col-md">

            <span class="bordered-list font-color-blue">CORSI ATTIVI</span>



            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsiAnnuali()->value() ?></span>

            <?php if($corsi240->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi240->count() > 0): ?>
              <?php foreach($corsi240 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>


            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsiOfficine()->value() ?></span>

            <?php if($corsiOfficine->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsiOfficine->count() > 0): ?>
              <?php foreach($corsiOfficine as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
            
          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi60()->value() ?></span>

            <?php if($corsi60->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi60->count() > 0): ?>
              <?php foreach($corsi60 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
            
          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi30()->value() ?></span>

            <?php if($corsi30->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi30->count() > 0): ?>
              <?php foreach($corsi30 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

            <!-- <div class="my-2"><br /></div> -->

          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-gold"><?= page("corsi")->titoloCorsiSpeciali()->value() ?></span>

            <?php if($corsiSpeciali->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsiSpeciali->count() > 0): ?>
              <?php foreach($corsiSpeciali as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

            <!-- <div class="my-2"><br /></div> -->

          </div>
        </div>
      <?php endif ?>
      ------------------------------------------------------ */?>



      <?php /* CORONAVIRUS ALT ----------------------------- 

      <?php if($page->tipo()->value() === "docente"): ?>
        <div class="row">
          
          <div class="col-md">

            <span class="bordered-list font-color-blue">CORSI ATTIVI</span>

            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi60()->value() ?></span>

            <?php if($corsi60->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi60->count() > 0): ?>
              <?php foreach($corsi60 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>
            
          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-blue"><?= page("corsi")->titoloCorsi30()->value() ?></span>

            <?php if($corsi30->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsi30->count() > 0): ?>
              <?php foreach($corsi30 as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

            <!-- <div class="my-2"><br /></div> -->

          </div>
          <div class="col-md">

            <span class="bordered-list d-none d-md-block">&nbsp;</span>
            <span class="bordered-list font-color-gold"><?= page("corsi")->titoloCorsiSpeciali()->value() ?></span>

            <?php if($corsiSpeciali->count() == 0): ?>
              <span class="bordered-list">Nessun corso attivo.</span>
            <?php endif ?>

            <?php if($corsiSpeciali->count() > 0): ?>
              <?php foreach($corsiSpeciali as $item): ?>
                <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
              <?php endforeach ?>
            <?php endif ?>

            <!-- <div class="my-2"><br /></div> -->

          </div>
        </div>
      <?php endif ?>

      <?php /* CORONAVIRUS ALT END ------------------------- */?>




    </div>

    <?php if($page->tipo()->value() === "docente"): ?>

      <?php /*kill($corsi);*/ ?>

      <div class="container-fluid super-cont">
        <div class="row">
          <div class="col-12">
            <p class="font-sans-s mb-2">CORSI ATTIVI</p>
            <hr />
          </div>

          <?php if($corsi->count() == 0): /* --- If there are no courses --- */ ?>
            
            <div class="col-lg mb-5 corsi-column">
              <span class="bordered-list">Nessun corso attivo.</span>
            </div>

          <?php else: /* --- If there are courses --- */ ?>

            <?php if ($corsi1A->count() + $corsi1B->count() > 0): ?>
              
              <div class="col-lg mb-5 corsi-column">
                <?php if($corsi1A->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol1ATitle()->value() ?></span>
                  <?php foreach($corsi1A as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                  <div class="border-bottom mt-5 mt-lg-0"></div>
                <?php endif ?>
                <?php if($corsi1B->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol1BTitle()->value() ?></span>
                  <?php foreach($corsi1B as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                <?php endif ?>
              </div>
              
            <?php endif ?>

            <?php if ($corsi2A->count() + $corsi2B->count() > 0): ?>
              
              <div class="col-lg mb-5 corsi-column">
                <?php if($corsi2A->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol2ATitle()->value() ?></span>
                  <?php foreach($corsi2A as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                  <div class="border-bottom mt-5 mt-lg-0"></div>
                <?php endif ?>
                <?php if($corsi2B->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol2BTitle()->value() ?></span>
                  <?php foreach($corsi2B as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                <?php endif ?>
              </div>
              
            <?php endif ?>

            <?php if ($corsi3A->count() + $corsi3B->count() > 0): ?>
              
              <div class="col-lg mb-5 corsi-column">
                <?php if($corsi3A->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol3ATitle()->value() ?></span>
                  <?php foreach($corsi3A as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                  <div class="border-bottom mt-5 mt-lg-0"></div>
                <?php endif ?>
                <?php if($corsi3B->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol3BTitle()->value() ?></span>
                  <?php foreach($corsi3B as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                <?php endif ?>
              </div>
              
            <?php endif ?>

            <?php if ($corsi4A->count() + $corsi4B->count() > 0): ?>
              
              <div class="col-lg mb-5 corsi-column">
                <?php if($corsi4A->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol4ATitle()->value() ?></span>
                  <?php foreach($corsi4A as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                  <div class="border-bottom mt-5 mt-lg-0"></div>
                <?php endif ?>
                <?php if($corsi4B->count() > 0): ?>
                  <span class="bordered-list bigger-title font-color-blue"><?= $pc->dynamicCol4BTitle()->value() ?></span>
                  <?php foreach($corsi4B as $item): ?>
                    <a class="bordered-list" href="<?= $item->url() ?>"><?= $item->title()->value() ?></a>
                  <?php endforeach ?>
                <?php endif ?>
              </div>
              
            <?php endif ?>

          <?php endif /* --- If there are courses --- */ ?>

        </div>
      </div>

    <?php endif ?>

  </main>

<?php snippet('footer') ?>