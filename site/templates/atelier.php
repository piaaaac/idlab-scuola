<?php

// Il Disegno visto dai grafici
$docenti1 = new Pages();
$docenti1->add(page('la-scuola/alessandro-busseni'));
$docenti1->add(page('la-scuola/antonio-bonanno'));
$docenti1->add(page('la-scuola/claudia-neri'));
// Il Disegno visto dagli illustratori A
$docenti2 = new Pages();
$docenti2->add(page('la-scuola/silvio-boselli'));
$docenti2->add(page('la-scuola/caterina-giorgetti'));
$docenti2->add(page('la-scuola/matteo-cattaneo'));
// Il Disegno visto dagli illustratori B
$docenti3 = new Pages();
$docenti3->add(page('la-scuola/angela-allegretti'));
$docenti3->add(page('la-scuola/maurizio-andreolli'));
$docenti3->add(page('la-scuola/cecilia-marra'));
// Il Disegno visto dai fumettisti
$docenti4 = new Pages();
$docenti4->add(page('la-scuola/salvo-dagostino'));
$docenti4->add(page('la-scuola/graziano-barbaro'));
$docenti4->add(page('la-scuola/lorenzo-sartori'));
// Il Disegno visto dai pittori
$docenti5 = new Pages();
$docenti5->add(page('la-scuola/giannalisa-digiacomo'));
$docenti5->add(page('la-scuola/francesco-dossena'));
$docenti5->add(page('la-scuola/pietro-nimis'));
?>

<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>
      
    <div class="my-2"><br /></div>

    <div class="container-fluid super-cont">

      <div class="row">
        <div class="col-md-6">
          <div class="links-gold mb-4"><?= $page->testoCol1()->kirbytext() ?></div>
        </div>
        <div class="col-md-6">
          <div class="links-gold mb-4"><?= $page->testoCol2()->kirbytext() ?></div>
        </div>
      </div>
      
      <!-- NEW VERSION -->
      <div class="row">
        <div class="col-sm-12">
          <hr class="mt-2 mb-4" />
        </div>
        <div class="col-xl-9 mt-2 my-5">
          <p class="font-sans-s"><?= $page->testoProspettoOrarioLinea1()->value() ?></p>
          <p class="font-sans-lll"><?= $page->testoProspettoOrarioLinea2()->value() ?></p>
          <p class="font-sans-sss mt-1 mt-xl-3"><?= $page->testoProspettoOrarioLinea3()->value() ?></p>
        </div>
      </div>
  
      <div class="mt-0"><br /></div>

      <div class="row">
        <div class="col-sm-12">
          <p class="font-size-m font-color-blue">ATELIER</p>
          <hr class="mt-2 mb-4" />
        </div>

        <?php
        $items = $page->ateliers()->toStructure();
        $n = 1;
        foreach($items as $item){
          $atelierDocenti = new Pages();
          if($item->atelierdocente1()->isNotEmpty()){ 
            $atelierDocenti->add(page('la-scuola/' . $item->atelierdocente1()->value())); }
          if($item->atelierdocente2()->isNotEmpty()){ 
            $atelierDocenti->add(page('la-scuola/' . $item->atelierdocente2()->value())); }
          if($item->atelierdocente3()->isNotEmpty()){ 
            $atelierDocenti->add(page('la-scuola/' . $item->atelierdocente3()->value())); }

          $borderClass = "";
          $lines2col = false;
          $lines3col = false;
          if($n % 2 == 0){ 
            $borderClass .= "bl-md";
            $lines2col = true;
          }
          if($n % 3 == 0){ 
            $lines3col = true;
          }
          if(
            ($n + 1) % 3 == 0
            || ($n % 3 == 0)
          ){ 
            $borderClass .= " bl-xl"; 
          }

          ?>
          <div class="col-md-6 col-xl-4 atelier mb-0 <?= $borderClass ?>">
            <p class="font-sans-lll"><?= multiline($item->ateliertitle()->value()) ?></p>
            <a class="btn btn-primary btn-large reduce-xs font-color-gold mb-2 mt-3" href="<?= $item->atelierbtnurl()->toUrl() ?>" role="button">
              <?= $item->atelierbtntxt()->upper() ?> &rarr;</a>
            <div class="row">
              <?php snippet('showcase-docenti', [
                "docenti"   => $atelierDocenti,
                "itemClass" => "col-4 col-sm-3 col-md-4",
              ]); ?>
            </div>
          </div>
          <div class="col-12 d-block d-md-none"><hr class="mt-4 mb-4" /></div>
          <?php if($lines2col): ?>
            <div class="col-12 d-none d-md-block d-xl-none"><hr class="mt-4 mb-4" /></div>
          <?php endif ?>
          <?php if($lines3col): ?>
            <div class="col-12 d-none d-xl-block"><hr class="mt-4 mb-4" /></div>
          <?php endif ?>
          <?php 
          $n++;
        } 
        ?>

      </div>

    </div>

  </main>

<?php snippet('footer') ?>
