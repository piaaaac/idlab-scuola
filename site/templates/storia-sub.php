<?php snippet('header') ?>

  <main class="main" role="main">

    <?php snippet('breadcrumb-title') ?>

    <div class="container-fluid super-cont">

      <div class="row">
        <div class="col-4"></div>
        <div class="col-8">
          <?= $page->text()->kirbytext() ?>
        </div>
      </div>
    
    </div>


  </main>

<?php snippet('footer') ?>