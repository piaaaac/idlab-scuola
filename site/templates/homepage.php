<?php 
$fasciaNews = null;
if($page->fasciaNews()->value() != ""){
  $fasciaNews = $page->fasciaNews();
}

// prepare img name
if($page->imgOpening() && $image = $page->imgOpening()->toFile()){
  $bgUrl = $image->url();
} else {
  $bgUrl = "none";
}
if($page->imgOpeningR() && $image = $page->imgOpeningR()->toFile()){
  $bgUrlR = $image->url();
} else {
  $bgUrlR = "none";
}

if($page->imgOpeningMobile() && $image = $page->imgOpeningMobile()->toFile()){
  $imgUrlMobile = $image->url();
} else {
  $imgUrlMobile = $site->url() ."/assets/images/fallback-home-mobile.svg";
}
if($page->imgOpeningMobileR() && $image = $page->imgOpeningMobileR()->toFile()){
  $imgUrlMobileR = $image->url();
} else {
  $imgUrlMobileR = $site->url() ."/assets/images/fallback-home-mobile.svg";
}


$ctaUrl = "";
$ctaTxtDsk = "";
$ctaTxtMob = "";
$ctaBg = "red";
if($page->btnTextDsk()->exists() && $page->btnTextDsk()->isNotEmpty()){
  $ctaTxtDsk = $page->btnTextDsk()->value();
}
if($page->btnTextMob()->exists() && $page->btnTextMob()->isNotEmpty()){
  $ctaTxtMob = $page->btnTextMob()->kt();
}
if($page->btnLink()->exists() && $page->btnLink()->isNotEmpty()){
  if(str::substr($page->btnLink()->value(), 0, 4) === "http"){
    $ctaUrl = $page->btnLink()->value();
  } else {
    $ctaUrl = page($page->btnLink()->value())->url();
  }
}
if($page->btnBg()->exists() && $page->btnBg()->isNotEmpty()){
  $ctaBg = $page->btnBg()->value();
}

?>


<?php snippet('header', ["fasciaNews" => $fasciaNews]) ?>

  <script>
    function closeCookie() {
      document.cookie = 'cookie-note=1;path=/;max-age=864000'; // 10 days
      $('#cookie-banner').addClass("hidden");
    }
    $(document).ready(function(){
      // via https://github.com/schnti/kirby-cookie/blob/master/src/cookie.js
      if (document.cookie.indexOf('cookie-note=1') == -1) {
        $('#cookie-banner').removeClass("hidden");
      }
    });
  </script>

  <div id="cookie-banner" class="hidden">
    <div class="d-block d-sm-flex">
      <div class="flex-grow-1">
        <?= $page->cookieBanner()->kirbytext() ?>
      </div>
      <div class="ml-sm-3 mt-3 mt-sm-0 text-center">
        <a class="btn btn-primary btn-small font-color-black hover-gold-dark" href="javascript:;" onclick="closeCookie()" role="button">
          <?= $page->cookieBannerBtn()->valaue() ?>
        </a>
      </div>
    </div>
  </div>

  <main class="main-big-logo" role="main">

    <!-- Opening XS to SM -->

    <div class="container-fluid super-cont d-block d-md-none">
      <div class="row">
        <div class="col-12">

          <div class="pt-3 pb-4 d-flex justify-content-between align-items-start">
            <a class="" href="<?= page('homepage')->url() ?>">
              <img class="logo-h-60 pr-3" src="<?= $site->logoTwoLinesBlack()->toFile()->url() ?>" />
            </a>

            <button class="mt-2 hamburger hamburger--slider text-color-black" type="button" onclick="javascript:toggleXsMenu();">
              <span class="hamburger-box">
                <span class="hamburger-inner"></span>
              </span>
            </button>
          </div>

          <!-- Super -->
          <img class="img-fluid img-framed mt-5 mb-4 link-hp-super" src="<?= $imgUrlMobile ?>" />
          <div class="copy mb-4">
            <?= $page->text()->kirbytext() ?>
          </div>

          <!-- Lapis -->
          <div id="crowdfunding-button-trigger"></div>
          <img class="img-fluid img-framed mt-5 mb-4 link-hp-lapis" src="<?= $imgUrlMobileR ?>" />
          <div class="copy mb-4">
            <?= $page->textR()->kirbytext() ?>
          </div>

        </div>
      </div>
    </div>

    <?php if($ctaTxtMob): ?>
    <a id="crowdfunding-btn-mobile" class="d-md-none text-center <?= $ctaBg ?>" href="<?= $ctaUrl ?>">
      <?= $ctaTxtMob ?>
    </a>
    <?php endif ?>

    <!-- Opening MD up -->

    <div class="container-fluid super-cont landing-dsk d-none d-md-flex">
        
      <div class="row">
        <div class="col-12 d-flex justify-content-between align-items-start pt-3 pb-3 mb-5">
          <a class="" href="<?= page('homepage')->url() ?>">
            <img class="logo-h-60 pr-3" src="<?= $site->logoTwoLinesBlack()->toFile()->url() ?>" />
          </a>
          <div class="pt-3"><?php snippet('menu', ["textColor" => "black-text"]) ?></div>
        </div>
      </div>

      <div class="row flex-grow-1">
        <div class="col-12 d-flex flex-column justify-content-center position-relative">

          <div class="row images-v3 flex-grow-1">  <!-- Super & Lapis images -->
            <!--  
            <div class="col-6 position-relative">
              <div class="hp-illustration link-hp-super" style="background-image: url(<?= $bgUrl ?>);"></div>
            </div>
            <div class="col-6 position-relative">
              <div class="hp-illustration link-hp-lapis" style="background-image: url(<?= $bgUrlR ?>);"></div>
            </div>
            -->
            <!--  
            <div class="col-6">
              <div class="hp-illustration-wrapper-v2 link-hp-super">
                 <img class="img-framed" src="<?= $bgUrl ?>" />
              </div>
            </div>
            <div class="col-6 position-relative">
              <div class="hp-illustration-wrapper-v2 link-hp-lapis">
                 <img class="img-framed" src="<?= $bgUrlR ?>" />
              </div>
            </div>
            -->
            <div class="col-6 position-relative">
              <div class="hp-illustration-v3 img-framed link-hp-super" style="background-image: url(<?= $bgUrl ?>);"></div>
            </div>
            <div class="col-6 position-relative">
              <div class="hp-illustration-v3 img-framed link-hp-lapis" style="background-image: url(<?= $bgUrlR ?>);"></div>
            </div>
          </div>

          <div class="row mt-4 mb-4">  <!-- Super & Lapis texts -->
            <div class="col-6">
              <div class="font-sans-ss copy"><?= $page->text()->kirbytext() ?></div>
            </div>
            <div class="col-6">
              <div class="font-sans-ss copy"><?= $page->textR()->kirbytext() ?></div>
            </div>
          </div>
          
          <?php if($ctaTxtDsk): ?>
          <div class="row mb-4">  <!-- Crowdfunding btn dsk -->
            <div class="col-12 pt-4">
              <a class="huge-btn <?= $ctaBg ?>" href="<?= $ctaUrl ?>"><?= $ctaTxtDsk ?></a>
            </div>
          </div>
          <?php endif ?>

        </div>
      </div>

      <div class="row">  
        <!-- Cosa succede title dsk -->
        <div class="col-12 d-none d-md-block mb-4 mt-4">
          <span class="font-super-ll"><?= page('newsbox')->title()->value() ?></span>
        </div>
      </div>

    </div>

    <!-- Cosa succede title mobile -->
    <div class="container-fluid super-cont d-md-none mb-4 mt-4 pt-5">
      <div class="row">
        <div class="col-12">
          <span class="font-super-ll"><?= page('newsbox')->title()->value() ?></span>
        </div>
      </div>
    </div>

    <!-- Cosa succede Slideshow -->
    <?php snippet('showcase-news', ["limit" => 9]) ?>
    
    <?php snippet('blue-bar') ?>
  
    <div class="my-4"><br /></div>

    <!-- DISCIPLINE -->
    <div class="container-fluid super-cont mb-4">
      <div class="row">
        <div class="col-12 d-flex justify-content-between justify-content-sm-start align-items-center">
          <span class="font-super-ll">In vetrina</span>
          <!-- <a class="font-sans-ss ml-5" href="<?= page('discipline')->url() ?>">Vedi tutte</a> -->
        </div>
      </div>
    </div>

    <?php snippet('showcasediscipline') ?>

  </main>

<?php snippet('footer') ?>