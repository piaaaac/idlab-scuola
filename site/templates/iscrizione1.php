<?php 
date_default_timezone_set('Europe/Rome');
?>

<?php snippet('header') ?>

<!-- 
DATA FROM CONTROLLER
  cartArticleeeeIdsString
  cartArray
  formIscrizioneAction
  isCached
  cachedFormData
  cittadinanze1
  cittadinanze2
  province
  costoTotale
 -->

  <main class="main" role="main" id="pagine-checkout">

    <div class="container-fluid super-cont">

      <div class="alert alert-danger alert-dismissible fade show" id="form-alert" role="alert">
        <span class="msg"></span>
        <button type="button" onclick="$('#form-alert').hide()" class="close" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <div class="row">
        <div class="col-xl-4">
          <p class="font-sans-s font-color-blue mt-5 pt-2">CORSI SELEZIONATI / SELECTED COURSES</p>
          <hr class="mt-1 mb-0"/>

          <div class="cart-recap">
            <?php foreach($cartArray as $item): ?>
              <?php 
                $articleeeeIdUrl = urlencode($item->articleeeeId);
                $deleteLink = $site->url() ."/assets/php/cart-delete-item.php?articleeeeId=". $articleeeeIdUrl;
                $corso = page("corsi")->children()->findBy("corsoId", $item->corso->corsoId);
                if(!$corso){
                  go(page('error')->url() ."/err:3316516166");
                } 
                $turno = $corso->children()->findBy("uid", $item->turno->uid);
                if(!$turno){
                  go(page('error')->url() ."/err:3316516167");
                }
                $turnoDatesString = turnoDates($turno);
              ?>
              <div class="cart-recap-item d-flex justify-content-between align-items-center pt-3 pb-2">
              
                <div class="title">
                  <span class="d-inline-block font-sans-s mr-2"><?= $item->corso->title ?></span>
                  <span class="d-inline-block font-sans-sss"><?= $item->turno->nome . $turnoDatesString ?></span>
                </div>

                <div class="sconto font-sans-sss">
                  <?php if(property_exists($item, "sconto")): ?>
                    <?= $item->sconto->description ?>
                  <?php endif ?>
                </div>

                <div class="costo">
                  &euro;&nbsp;<?= $item->costoFinale ?>
                </div>

                <a class="removeCartItem" href="<?= $deleteLink ?>" id="remove-<?= $item->articleeeeId ?>">
                  <i class="fas fa-trash d-block mr-2 mb-1"></i>
                </a>

              </div>
            <?php endforeach ?>

            <div class="d-flex justify-content-between my-3">
              <?php
              $linkAddText = count($cartArray)>0 ? "Aggiungi altri corsi / Add courses" : "Aggiungi un corso / Add courses";
              ?>
              <a class="flex-fill font-sans-s font-color-gold" href="<?= page("corsi")->url() ?>"><?=$linkAddText?> &rarr;</a>
              <p class="text-right">Totale &euro;&nbsp;<span id="total-cart-price"><?= $costoTotale ?></span></p>
              <p class="trash-spacer"></p>
            </div>

          </div>
        </div>

        <div class="col-xl-8">
          <form id="form-iscrizione" action="<?= $formIscrizioneAction ?>" method="POST" novalidate>

            <input type="hidden" id="dataOraOrdine" name="dataOraOrdine" value="<?= date('Y-m-d H:i:s') ?>" readonly="readonly">
            <input type="hidden" id="cartarticleeeeIds" name="cartarticleeeeIds" readonly="readonly" value="<?= $cartArticleeeeIdsString ?>">

            <div class="row">
              <div class="col-12">
                <p class="font-sans-s font-color-blue mt-5 pt-2">DATI PERSONALI / PERSONAL DETAILS</p>
                <hr class="mt-1 mb-4"/>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="nome" name="nome" placeholder="* Nome / Name"
                  value="<?= r($isCached, $cachedFormData["form_nome"], "") ?>">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="cognome" name="cognome" placeholder="* Cognome / Surname" 
                  value="<?= r($isCached, $cachedFormData["form_cognome"], "") ?>">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input id="dataNascita" name="dataNascita" placeholder="* Data di nascita / Date of birth" onclick="$datepicker.open()" >
                  <span id="dataNascita-custom-error-placement"></span>
                  <script>
                    var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
                    var $datepicker = $('#dataNascita').datepicker({
                      uiLibrary: 'bootstrap4',
                      format: 'yyyy-mm-dd',
                      value: "<?= r(
                        $isCached && (strcmp($cachedFormData["form_dataNascita"], "") != 0),
                        $cachedFormData["form_dataNascita"], 
                        '1990-01-01'
                      ) ?>",
                      maxDate: today
                    });
                    var eraseField = <?= r(!$isCached || !$cachedFormData["form_dataNascita"], "true", "false") ?>;
                    if (eraseField){
                      $datepicker.value("");
                    }
                  </script>
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <select class="custom-select mr-sm-2" id="cittadinanza" name="cittadinanza">
                    <!-- <optgroup>...</optgroup>-->
                    <option value="" disabled 
                      <?= r(!$isCached || !$cachedFormData["form_cittadinanza"], "selected", "") ?>
                    >* Cittadinanza / Nationality</option>
                    <option disabled>──────</option>
                    <?php foreach($cittadinanze1 as $c): ?>
                      <option value="<?= $c["country"] ?>" 
                        <?= r($isCached && strcmp($cachedFormData["form_cittadinanza"], $c["country"]) == 0, "selected", "") ?>
                      ><?= $c["country"] ?></option>
                    <?php endforeach ?>
                    <option disabled>──────</option>
                    <?php foreach($cittadinanze2 as $c): ?>
                      <option value="<?= $c["country"] ?>" 
                        <?= r($isCached && strcmp($cachedFormData["form_cittadinanza"], $c["country"]) == 0, "selected", "") ?>
                      ><?= $c["country"] ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="luogoNascita" name="luogoNascita" placeholder="* Luogo di nascita / Place of birth"
                  value="<?= r($isCached, $cachedFormData["form_luogoNascita"], "") ?>">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <select class="custom-select mr-sm-2" id="provinciaNascita" name="provinciaNascita">
                    <!-- <optgroup>...</optgroup>-->
                    <option value="" disabled 
                      <?= r(!$isCached || !$cachedFormData["form_provinciaNascita"], "selected", "") ?>
                    >* Provincia / Province</option>
                    <option disabled>──────</option>
                    <option value="EE"
                    <?= r($isCached && strcmp($cachedFormData["form_provinciaNascita"], "EE") == 0, "selected", "") ?>
                    >EE – Stato estero</option>
                    <option disabled>──────</option>
                    <?php foreach($province as $provincia): ?>
                      <option value="<?= $provincia["sigla"] ?>"
                      <?= r($isCached && strcmp($cachedFormData["form_provinciaNascita"], $provincia["sigla"]) == 0, "selected", "") ?>>
                        <?= "$provincia[sigla] – $provincia[provincia]" ?>
                      </option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="codiceFiscale" name="codiceFiscale" placeholder="Codice fiscale / Only for Italian residents"
                  value="<?= r($isCached, $cachedFormData["form_codiceFiscale"], "") ?>">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Numero di telefono / Phone number"
                  value="<?= r($isCached, $cachedFormData["form_telefono"], "") ?>">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="email" class="form-control" id="email" name="email" placeholder="* Email / email"
                  value="<?= r($isCached, $cachedFormData["form_email"], "") ?>">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <p class="font-sans-s font-color-blue mt-5 pt-2">RESIDENZA / ADDRESS</p>
                <hr class="mt-1 mb-4"/>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-9">
                <div class="form-group">
                  <input type="text" class="form-control" id="residenzaIndirizzo" name="residenzaIndirizzo" placeholder="Indirizzo / Street name"
                  value="<?= r($isCached, $cachedFormData["form_residenzaIndirizzo"], "") ?>">
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <input type="text" class="form-control" id="residenzaCivico" name="residenzaCivico" placeholder="N° civico / Nr"
                  value="<?= r($isCached, $cachedFormData["form_residenzaCivico"], "") ?>">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-sm-6">
                <div class="form-group">
                  <input type="text" class="form-control" id="residenzaLuogo" name="residenzaLuogo" placeholder="* Citt&agrave; (o stato estero) / City (or state if abroad)"
                  value="<?= r($isCached, $cachedFormData["form_residenzaLuogo"], "") ?>">
                </div>
              </div>
              <div class="col-sm-6">
                <div class="form-group">
                  <select class="custom-select mr-sm-2" id="residenzaProvincia" name="residenzaProvincia">
                    <!-- <optgroup>...</optgroup>-->
                    <option value="" disabled
                    <?= r(!$isCached || !$cachedFormData["form_residenzaProvincia"], "selected", "") ?>
                    >* Provincia / Province</option>
                    <option disabled>──────</option>
                    <option value="EE"
                    <?= r($isCached && strcmp($cachedFormData["form_residenzaProvincia"], "EE") == 0, "selected", "") ?>
                    >EE – Stato estero</option>
                    <option disabled>──────</option>
                    <?php foreach($province as $provincia): ?>
                      <option value="<?= $provincia["sigla"] ?>"
                      <?= r($isCached && strcmp($cachedFormData["form_residenzaProvincia"], $provincia["sigla"]) == 0, "selected", "") ?>
                      ><?= "$provincia[sigla] – $provincia[provincia]" ?></option>
                    <?php endforeach ?>
                  </select>
                </div>
              </div>
              <div class="col-sm-3">
                <div class="form-group">
                  <input type="text" class="form-control" id="residenzaCap" name="residenzaCap" placeholder="CAP / ZIP code"
                  value="<?= r($isCached, $cachedFormData["form_residenzaCap"], "") ?>">
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <p class="font-sans-s font-color-blue mt-5 pt-2">DATI DI FATTURAZIONE AZIENDALI / INVOICE DETAILS</p>
                <hr class="mt-1 mb-4"/>
                <div class="form-group">
                  <input class="form-check-input" type="checkbox" id="fatturazioneAziendale" name="fatturazioneAziendale" value="azienda"
                  <?= r($isCached && strcmp($cachedFormData["fatturazioneAziendale"], "azienda") == 0, "checked", "") ?>>
                  <label class="form-check-label" for="fatturazioneAziendale">Sono un'azienda o libero professionista / I am a company or a freelance</label>
                </div>
              </div>
            </div>

            <div id="section-azienda">
              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaRagioneSociale" name="aziendaRagioneSociale" placeholder="Ragione sociale / Name"
                    value="<?= r($isCached, $cachedFormData["form_aziendaRagioneSociale"], "") ?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaCodiceFiscale" name="aziendaCodiceFiscale" placeholder="Codice fiscale / Only for Italian businesses"
                    value="<?= r($isCached, $cachedFormData["form_aziendaCodiceFiscale"], "") ?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaPartitaIva" name="aziendaPartitaIva" placeholder="Partita Iva / Tax number"
                    value="<?= r($isCached, $cachedFormData["form_aziendaPartitaIva"], "") ?>">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-9">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaIndirizzo" name="aziendaIndirizzo" placeholder="Indirizzo / Street name"
                    value="<?= r($isCached, $cachedFormData["form_aziendaIndirizzo"], "") ?>">
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaCivico" name="aziendaCivico" placeholder="N° civico / Nr"
                    value="<?= r($isCached, $cachedFormData["form_aziendaCivico"], "") ?>">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-6">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaLuogo" name="aziendaLuogo" placeholder="Citt&agrave; / Place"
                    value="<?= r($isCached, $cachedFormData["form_aziendaLuogo"], "") ?>">
                  </div>
                </div>
                <div class="col-sm-6">
                  <div class="form-group">
                    <select class="custom-select mr-sm-2 input-azienda" id="aziendaProvincia" name="aziendaProvincia">
                      <!-- <optgroup>...</optgroup>-->
                      <option value="" disabled 
                      <?= r(!$isCached || !$cachedFormData["form_aziendaProvincia"], "selected", "") ?>
                      >Provincia / Province...</option>
                      <option disabled>──────</option>
                      <option value="EE"
                      <?= r($isCached && strcmp($cachedFormData["form_aziendaProvincia"], "EE") == 0, "selected", "") ?>
                      >EE – Stato estero</option>
                      <option disabled>──────</option>
                      <?php foreach($province as $provincia): ?>
                        <option value="<?= $provincia["sigla"] ?>"
                        <?= r($isCached && strcmp($cachedFormData["form_aziendaProvincia"], $provincia["sigla"]) == 0, "selected", "") ?>
                        ><?= "$provincia[sigla] – $provincia[provincia]" ?></option>
                      <?php endforeach ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-3">
                  <div class="form-group">
                    <input type="text" class="form-control input-azienda" id="aziendaCap" name="aziendaCap" placeholder="CAP / ZIP code"
                    value="<?= r($isCached, $cachedFormData["form_aziendaCap"], "") ?>">
                  </div>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-12">
                <p class="font-sans-s font-color-blue mt-5 pt-2">
                  ISCRIZIONE AI TURNI / ENROLMENT
                </p>
                <hr class="mt-1 mb-4"/>
              </div>
            </div>

            <?php 
              $n = 1;
              foreach($cartArray as $item):
                $corso = page("corsi")->children()->findBy("corsoId", $item->corso->corsoId);
                if(!$corso){
                  go(page('error')->url() ."/err:3316526166");
                } 
                $turno = $corso->children()->findBy("uid", $item->turno->uid);
                if(!$turno){
                  go(page('error')->url() ."/err:3316526167");
                }
                $turnoDatesString = turnoDates($turno);
                $line1 = $item->corso->title;
                $line2 = $item->turno->nome . $turnoDatesString;
              ?>

              <!-- Iscritto/a Checkbox -->

              <div class="row">
                <div class="col-12 mt-0">
                  <p class="font-sans-s mr-2 mb-3">
                    <?= "Iscrizione / Enrolment #$n" ?> &mdash; <?= $item->corso->title ?>
                    <span class="font-sans-sss my-2"><?= $item->turno->nome . $turnoDatesString ?></span>
                  </p>
                  
                  <div class="form-check pl-0">
                    <input class="form-check-input radio" 
                      type="radio" 
                      name="iscrittoa__<?=$n?>"
                      id="iscrittoa__<?=$n?>-a"
                      value="use_buyer_data" 
                      data-iscrittoa-number="<?=$n?>"
                      checked
                    >
                    <label class="form-check-label" for="iscrittoa__<?=$n?>-a">
                      Iscrivi l'aquirente / Enrol the buyer <span class="name-surname-buyer"></span>
                    </label>
                  </div>
                  <div class="form-check pl-0">
                    <input class="form-check-input radio"
                      type="radio"
                      name="iscrittoa__<?=$n?>"
                      id="iscrittoa__<?=$n?>-b"
                      value="use_new_data"
                      data-iscrittoa-number="<?=$n?>"
                    >
                    <label class="form-check-label" for="iscrittoa__<?=$n?>-b">
                      Iscrivi qualcun'altro / Enrol someone else
                    </label>
                  </div>
                  
                  <div class="spacer my-3"></div>

                </div>
              </div>

              <!-- Iscritto/a form -->

              <div id="section-iscrittoa__<?=$n?>">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="form-control"
                        type="text" 
                        id="iscrittoa__<?=$n?>-nome"
                        name="iscrittoa__<?=$n?>-nome"
                        placeholder="* Nome / Name"
                        value="<?= r($isCached, $cachedFormData["iscrittoa__$n-nome"], "") ?>"
                      >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="form-control"
                        type="text" 
                        id="iscrittoa__<?=$n?>-cognome"
                        name="iscrittoa__<?=$n?>-cognome"
                        placeholder="* Cognome / Surname" 
                        value="<?= r($isCached, $cachedFormData["iscrittoa__$n-cognome"], "") ?>"
                      >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="form-control" 
                        type="email" 
                        id="iscrittoa__<?=$n?>-email" 
                        name="iscrittoa__<?=$n?>-email" 
                        placeholder="* Email / email"
                        value="<?= r($isCached, $cachedFormData["iscrittoa__$n-email"], "") ?>"
                      >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-group">
                      <input class="form-control" 
                        type="text" 
                        id="iscrittoa__<?=$n?>-telefono" 
                        name="iscrittoa__<?=$n?>-telefono" 
                        placeholder="Numero di telefono / Phone number"
                        value="<?= r($isCached, $cachedFormData["iscrittoa__$n-telefono"], "") ?>"
                      >
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-12 mt-0">
                  <hr class="mt-1 mb-4"/>
                </div>
              </div>

              <?php
              $n++;
            endforeach ?>

            <div class="row">
              <div class="col-12">
                <p class="font-sans-s font-color-blue mt-5 pt-2">REGOLAMENTO CORSI E TRATTAMENTO DATI / COURSES' REGULATION AND PERSONAL DATA PROCESSING</p>
                <hr class="mt-1 mb-4"/>
                <h4></h4>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <input class="form-check-input" type="checkbox" id="checkbox1" name="checkbox1" value="check1"
                  <?= r($isCached && ($cachedFormData["form_checkbox1"] == true), "checked", "") ?>>
                  <label class="font-sans-ss form-check-label" for="checkbox1">
                    Dichiaro di aver preso visione e di accettare il <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/7-servizi-erogati/regolamento_corsi.pdf" target="_blank">Regolamento dei corsi</a> e quanto contenuto nell'<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/12-emergenza-covid-19/informativa_covid_19_terzi.pdf" target="_blank">Informativa Covid-19</a> (obbligatorio per poter proseguire)
                    <br />
                    / I have read and accept the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/7-servizi-erogati/regolamento_corsi.pdf" target="_blank">Courses' Regulations</a> and the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/12-emergenza-covid-19/informativa_covid_19_terzi.pdf" target="_blank">Covid-19 informative report</a> (required in order to continue)
                  </label>
                  <span id="checkbox1-custom-error-placement" class="checkbox-label-error"></span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <input class="form-check-input" type="checkbox" id="checkbox2" name="checkbox2" value="check2"
                  <?= r($isCached && $cachedFormData["form_checkbox2"], "checked", "") ?>>
                  <label class="font-sans-ss form-check-label" for="checkbox2">
                    Dichiaro di aver preso visione dell’<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Informativa Privacy</a> e acconsento al trattamento dei miei dati personali (obbligatorio per poter proseguire)
                    <br />
                    / I have read the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Privacy Policy</a> and consent to the processing of my personal data (required in order to continue)
                  </label>
                  <span id="checkbox2-custom-error-placement" class="checkbox-label-error"></span>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col">
                <div class="form-group">
                  <input class="form-check-input" type="checkbox" id="checkbox3" name="checkbox3" value="check3"
                  <?= r($isCached && $cachedFormData["form_checkbox3"], "checked", "") ?>>
                  <label class="font-sans-ss form-check-label" for="checkbox3">
                    Dichiaro di aver preso visione dell’<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Informativa Privacy</a> a acconsento al trattamento dei miei dati personali per ricevere newsletter o comunicazioni relative ai servizi offerti dalla Scuola Superiore d’Arte Applicata (facoltativo) 
                    <br />
                    / I have read the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Privacy Policy</a> and consent to the processing of my personal data to receive newsletters or communications relating to the services offered by the Higher School of Applied Art (optional)
                  </label>
                </div>
              </div>
            </div>

            <div class="my-2"><br /></div>

            <div class="row">
              <div class="col text-center text-xl-left">
                <button type="submit" id="submitt" class="btn btn-primary">CONTINUA L'ISCRIZIONE &rarr;</button>
                <!-- <a href="javascript:;" onclick="fillForm()">Fill form for testing</a> -->
                <!-- <a href="javascript:;" onclick="logAllFormData()">Log form data</a> -->
                <!-- <a href="javascript:;" onclick="testEmail()">Test email</a> -->
              </div>
            </div>

          </form>
        </div>
      </div>
    </div>

    <script>

      /*
      // https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5/Constraint_validation

      $( document ).ready(function() {
      // window.addEventListener('load', function() {
        var form = document.getElementById('form-iscrizione');
        var errorMsg = document.getElementById('error-message');
        form.addEventListener('submit', function(event) {
          if (form.checkValidity() === false) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add('was-validated');
          $('#form-alert').show();
          $("html, body").animate({ scrollTop: 0 }, 600);
        }, false);
      // }, false);
      });
      */

/* ALL FIELDS
dataOraOrdine
nomeCorso
cognome
nome
dataNascita
cittadinanza
luogoNascita
provinciaNascita
codiceFiscale
telefono
email
residenzaIndirizzo
residenzaCivico
residenzaCap
residenzaLuogo
residenzaProvincia
aziendaRagioneSociale
aziendaCodiceFiscale
aziendaPartitaIva
aziendaIndirizzo
aziendaCivico
aziendaCap
aziendaLuogo
aziendaProvincia
checkbox1
checkbox2
checkbox3
*/

      var aziendaOn = <?= r($isCached && strcmp($cachedFormData["fatturazioneAziendale"], "azienda") == 0, "true", "false") ?>

      $( document ).ready(function() {

        // --- cambia placeholder di campi "condizionalmente obbligatori"

        $("#cittadinanza").change(function(){
          if($(this).val() == "Italiana"){
            $("#codiceFiscale")[0].placeholder = "* Codice fiscale"
            $("#residenzaCap")[0].placeholder = "* CAP"
            $("#residenzaIndirizzo")[0].placeholder = "* Indirizzo"
            $("#residenzaCivico")[0].placeholder = "* N° civico"
          } else {
            $("#codiceFiscale")[0].placeholder = "Codice fiscale"
            $("#residenzaCap")[0].placeholder = "CAP"
            $("#residenzaIndirizzo")[0].placeholder = "Indirizzo"
            $("#residenzaCivico")[0].placeholder = "N° civico"
          }
        })

        // --- section azienda
        function activateAzienda(turnOn){
          if(turnOn) $('#section-azienda').show()
          else $('#section-azienda').hide()
        }
        activateAzienda(aziendaOn)
        document.getElementById('fatturazioneAziendale').onchange = function() {
          aziendaOn = this.checked
          activateAzienda(aziendaOn)
        };
  
        // --- section iscrittoa
        function activateIscrittoa(n, show){
          if(show) $('#section-iscrittoa__'+ n).show()
          else $('#section-iscrittoa__'+ n).hide()
        }
        $("[id^='section-iscrittoa__'").hide();
        $(".radio[id^='iscrittoa__'").change(function () {
          var n = this.dataset.iscrittoaNumber;
          var isOn = this.value === "use_new_data";
          activateIscrittoa(n, isOn);
        });
  
        // --- Replicate name in enrol form labels
        $("#nome").change(function () {
          handleNomeCognomeChange();
        });
        $("#cognome").change(function () {
          handleNomeCognomeChange();
        });
        function handleNomeCognomeChange () {
          var fullName = $("#nome").val() +" "+ $("#cognome").val();
          var txt = fullName ? "("+ fullName +")" : "";
          $(".name-surname-buyer").text(txt);
        }

        // --- force to check date on change after error
        $("#dataNascita").change(function(){
          console.log("dataNascitaValid", $(this).valid())
        })

        // --- validation settings
        $("#form-iscrizione").validate({
          rules: {
            cognome: "required",
            nome: "required",
            dataNascita: {
              required: true,
              pattern: /^[0-9]{4}-[0-9]{2}-[0-9]{2}$/
            },
            cittadinanza: "required",
            luogoNascita: "required",
            provinciaNascita: "required",
            codiceFiscale: {
              required: {
                depends: function(element) {
                  // console.log($("#cittadinanza").val());
                  // console.log($("#cittadinanza").val() == "Italiana");
                  return $("#cittadinanza").val() == "Italiana";
                }
              },
              pattern: /^[A-Za-z]{6}[0-9]{2}[A-Za-z][0-9]{2}[A-Za-z][0-9]{3}[A-Za-z]$/
            },
            email: {
              required: true,
              email: true
            },
            
            residenzaIndirizzo: {
              required: {
                depends: function(element) {
                  return $("#cittadinanza").val() == "Italiana";
                }
              },
            },
            residenzaCivico: {
              required: {
                depends: function(element) {
                  return $("#cittadinanza").val() == "Italiana";
                }
              },
            },
            residenzaCap: {
              required: {
                depends: function(element) {
                  return $("#cittadinanza").val() == "Italiana";
                }
              },
              pattern: /^[0-9]{5}$/
            },
            
            residenzaLuogo: "required",
            residenzaProvincia: "required",
            aziendaRagioneSociale: { required: {depends: function(element) {return aziendaOn }}},
            aziendaCodiceFiscale: { required: {depends: function(element) {return aziendaOn }}},
            aziendaPartitaIva: { 
              required: {depends: function(element) {return aziendaOn }},
              pattern: /^[0-9]{11}$/
            },
            aziendaIndirizzo: { required: {depends: function(element) {return aziendaOn }}},
            aziendaCivico: { required: {depends: function(element) {return aziendaOn }}},
            aziendaCap: { 
              required: {depends: function(element) {return aziendaOn }},
              pattern: /^[0-9]{5}$/
            },
            aziendaLuogo: { required: {depends: function(element) {return aziendaOn }}},
            aziendaProvincia: { required: {depends: function(element) {return aziendaOn }}},


            <?php 
            $n = 1;
            foreach($cartArray as $item): ?>

              "iscrittoa__<?=$n?>-nome": { required: {
                depends: function(element) {
                  return $(".radio#iscrittoa__<?=$n?>-b").is(":checked"); // "b" are checkboxes with "use_new_data"
                }},
              },
              "iscrittoa__<?=$n?>-cognome": { required: {
                depends: function(element) {
                  return $(".radio#iscrittoa__<?=$n?>-b").is(":checked"); // "b" are checkboxes with "use_new_data"
                }},
              },
              "iscrittoa__<?=$n?>-email": { required: {
                depends: function(element) {
                  return $(".radio#iscrittoa__<?=$n?>-b").is(":checked"); // "b" are checkboxes with "use_new_data"
                }},
              },

              <?php 
              $n++;
            endforeach;
            ?>

            checkbox1: "required",
            checkbox2: "required",
          },
          messages: {
            cognome:                "Campo necessario",
            nome:                   "Campo necessario",
            dataNascita: {
              required:             "Campo necessario",
              pattern:              "Formato data aaaa-mm-gg"
            },
            cittadinanza:           "Campo necessario",
            luogoNascita:           "Campo necessario",
            provinciaNascita:       "Campo necessario",
            codiceFiscale: {
              required:             "Campo necessario per i cittadini italiani",
              pattern:              "Controlla il formato del codice fiscale"
            },
            email: {
              required:             "Per iscriversi è necessario indicare un indirizzo email valido",
              email:                "Per iscriversi è necessario indicare un indirizzo email valido"
            },
            residenzaIndirizzo:     "Campo necessario per i cittadini italiani",
            residenzaCivico:        "Campo necessario per i cittadini italiani",
            residenzaCap: {
              required:             "Campo necessario per i cittadini italiani",
              pattern:              "Controlla il formato del CAP"
            },
            residenzaLuogo:         "Campo necessario",
            residenzaProvincia:     "Campo necessario",
            aziendaRagioneSociale:  "Campo necessario",
            aziendaCodiceFiscale:   "Campo necessario",
            aziendaPartitaIva: { 
              required:             "Campo necessario",
              pattern:              "Controlla il formato della partita Iva"
            },
            aziendaIndirizzo:       "Campo necessario",
            aziendaCivico:          "Campo necessario",
            aziendaCap:{
              required:             "Campo necessario",
              pattern:              "Controlla il formato del CAP"
            },
            aziendaLuogo:           "Campo necessario",
            aziendaProvincia:       "Campo necessario",

            <?php 
            $n = 1;
            foreach($cartArray as $item): ?>

              "iscrittoa__<?=$n?>-nome": "Campo necessario",
              "iscrittoa__<?=$n?>-cognome": "Campo necessario",
              "iscrittoa__<?=$n?>-email": "Campo necessario",

              <?php 
              $n++;
            endforeach;
            ?>

            checkbox1:              "Campo necessario",
            checkbox2:              "Campo necessario"
          },
          errorPlacement: function(error, element) {
            var n = element.attr("name");
            if (n == "dataNascita"){ error.appendTo( $('#dataNascita-custom-error-placement') ); }
            else if (n == "checkbox1"){ error.appendTo( $('#checkbox1-custom-error-placement') ); }
            else if (n == "checkbox2"){ error.appendTo( $('#checkbox2-custom-error-placement') ); }
            else{ error.insertAfter( element ); }
          },
          invalidHandler: function(form) {
            showAlert("Sono presenti degli errori, controlla i campi evidenziati in rosso.");
          },
          submitHandler: function(form) {
            
            // console.log($('#cartarticleIds').val());
            // console.log($('#cartarticleIds').val() == "");

            if(!$('#cartarticleeeeIds').val() || ($('#cartarticleeeeIds').val() == "")){
              showAlert("Seleziona uno o più corsi a cui iscriverti.");
            } else {
              $("#submitt").addClass("disabled");
              form.submit();
            }

          }
        });
      });

      function showAlert(msg){
        $('#form-alert .msg').text(msg);
        $('#form-alert').show();
        $("html, body").animate({ scrollTop: 0 }, 600);
      }
      function fillForm(){
        function randomValue(field) {
          var values = {
            cognome: ["Bianchi", "Mimmotti", "Galli", "Rossi", "Russo", "Ferrari", "Esposito", "Bianchi", "Romano", "Colombo", "Ricci", "Marino", "Greco", "Bruno", "Gallo", "Conti", "Deluca", "Mancini", "Costa", "Giordano", "Rizzo", "Lombardi", "Moretti"],
            nome: ["Alex", "Mimmo", "Franca", "Maria", "Anna", "Giuseppina", "Rosa", "Angela", "Giovanna", "Teresa", "Lucia", "Carmela", "Caterina", "Francesca", "Anna Maria", "Antonietta", "Carla", "Elena", "Concetta", "Rita", "Margherita", "Franca", "Paola", "Giuseppe", "Giovanni", "Antonio", "Mario", "Luigi", "Francesco", "Angelo", "Vincenzo", "Pietro", "Salvatore", "Carlo", "Franco", "Domenico", "Bruno", "Paolo", "Michele", "Giorgio", "Aldo", "Sergio", "Luciano"],
            dataNascita: ["1979-07-04", "1999-11-11", "1987-01-12", "1992-03-22"],
            cittadinanza: ["Italiana", "Italiana", "Italiana", "Italiana", "Italiana", "Colombia"],
            luogoNascita: ["Milano", "Viterbo", "Caldiero", "Verona"],
            provinciaNascita: ["SO", "VR", "MI", "NA", "RO"],
            codiceFiscale: ["PCNLXA85P04D222D", "MIUFDE99D13C909G", "MMMFRC78F11X098D"],
            email: ["piacentini.alex@gmail.com"],
            // email: ["info@mammamia.com", "kikko123@asdasd.it", "mail@email.it", "ciao@mamma.com"],
            residenzaIndirizzo: ["Piazza Archinto", "via Maiocchi", "Via G. Mazzini", "Via Roma"],
            residenzaCivico: ["12", "1A", "889", "34"],
            residenzaCap: ["20125", "37030", "76578"],
            residenzaLuogo: ["Cremona", "Agrigento", "Santa Maria di Leuca", "Bormio"],
            residenzaProvincia: ["SO", "VR", "MI", "NA", "RO"],
            aziendaRagioneSociale: [],
            aziendaCodiceFiscale: [],
            aziendaPartitaIva: [],
            aziendaIndirizzo: [],
            aziendaCivico: [],
            aziendaCap: [],
            aziendaLuogo: [],
            aziendaProvincia: [],
          }
          var i = Math.floor(Math.random() * values[field].length);
          return values[field][i];
        }
        $('#cognome').val(randomValue("cognome"));
        $('#nome').val(randomValue("nome"));
        $('#dataNascita').val(randomValue("dataNascita"));
        $('#cittadinanza').val(randomValue("cittadinanza"));
        $('#luogoNascita').val(randomValue("luogoNascita"));
        $('#provinciaNascita').val(randomValue("provinciaNascita"));
        $('#codiceFiscale').val(randomValue("codiceFiscale"));
        $('#email').val(randomValue("email"));
        $('#residenzaIndirizzo').val(randomValue("residenzaIndirizzo"));
        $('#residenzaCivico').val(randomValue("residenzaCivico"));
        $('#residenzaCap').val(randomValue("residenzaCap"));
        $('#residenzaLuogo').val(randomValue("residenzaLuogo"));
        $('#residenzaProvincia').val(randomValue("residenzaProvincia"));
        // aziendaRagioneSociale.val(randomValue("aziendaRagioneSociale"));
        // aziendaCodiceFiscale.val(randomValue("aziendaCodiceFiscale"));
        // aziendaPartitaIva.val(randomValue("aziendaPartitaIva"));
        // aziendaIndirizzo.val(randomValue("aziendaIndirizzo"));
        // aziendaCivico.val(randomValue("aziendaCivico"));
        // aziendaCap.val(randomValue("aziendaCap"));
        // aziendaLuogo.val(randomValue("aziendaLuogo"));
        // aziendaProvincia.val(randomValue("aziendaProvincia"));
        $('#checkbox1').prop('checked', true);
        $('#checkbox2').prop('checked', true);
      }

      function getAllFormIscrizioneData(){
        var formElements = $("#form-iscrizione input, #form-iscrizione select")
        var formValues = []
        formElements.each(function(){
          var val = ""
          if($(this).attr("type") == "checkbox"){
            if(this.checked) val = $(this).val()
          } else {
            val = $(this).val()
          }
          formValues.push({
            "name": $(this).attr("name"),
            "value": val
          })
        })
        return formValues
      }

      function logAllFormData(){
        getAllFormIscrizioneData().forEach(function(e){
          console.log(e.name +" = "+ e.value)
        })
      }

      function addHiddenFormData(formId){
        getAllFormIscrizioneData().forEach(function(e){
          if(e.name != "cartarticleeeeIds"){
            var postIndexName = e.name == "fatturazioneAziendale"
              ? e.name
              : "form_"+ e.name
            $('<input />').attr('type', 'hidden')
              .attr('name', postIndexName)
              .attr('value', e.value)
              .appendTo('#' + formId);
          }
        })
      }

      function testEmail(){
        var tempForm = $('<form method="post" id="mailTest" action="<?= page('iscrizione2')->url() ?>"></form>').appendTo('body')
        addHiddenFormData("mailTest")
        tempForm.submit();
      }

    </script>

  </main>

<?php snippet('footer') ?>