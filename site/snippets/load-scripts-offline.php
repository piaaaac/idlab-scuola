  <!-- Bootstrap -->
  
  <link rel="stylesheet" href="<?= $site->url() ?>/assets/offline/bootstrap.min.css">

  <script src="<?= $site->url() ?>/assets/offline/jquery-3.3.1.slim.min.js"></script>
  <script src="<?= $site->url() ?>/assets/offline/jquery.3.3.1.min.js"></script>

  <script src="<?= $site->url() ?>/assets/offline/popper.min.js"></script>

  <script src="<?= $site->url() ?>/assets/offline/bootstrap.min.js"></script>

  <!-- icons -->

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">  

  <!-- mailchimp css -->
  
<link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
<style type="text/css">#mc_embed_signup{ clear:left; }</style>

  <?php if (isset($loadFormScripts) && $loadFormScripts === true): ?>

    <!-- gijgo datepicker -->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap-validate -->
    <script src="<?= $site->url() ?>/assets/offline/bootstrap-validate.js"></script>
    <!-- jQuery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js" type="text/javascript"></script>
  
  <?php endif ?>

