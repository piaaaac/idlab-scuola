<?php /* ----------------------------------
input array
  $docenti      => pages collection,
  $itemClass    => string,
  $smaller      => bool
------------------------------------- */ ?>

<?php
$sorted = $docenti->sortBy("cognome", "asc");
$smallerStyle = (isset($smaller) && $smaller === true) ? 'style="width:70%"' : "";
?>

<?php foreach($sorted as $docente): ?>

  <?php
    $image = $docente->hasImages() 
      ? $docente->images()->first()->url()
      : kirby()->urls()->assets() ."/images/defaults/docente-xxx.png";
  ?>

  <div class="docente-container my-2 <?= $itemClass ?>">
    <a href="<?= $docente->url() ?>" class="font-sans-ss">
      <img class="docente-portrait img-fluid mb-2" <?= $smallerStyle ?> src="<?= $image ?>" />
      <p class="docente-name"><?= $docente->title()->value() ?>&nbsp;&rarr;</p>
    </a>
  </div>
<?php endforeach ?>
