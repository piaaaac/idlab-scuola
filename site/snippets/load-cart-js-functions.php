<script>
  var cartCookieKey = '<?= c::get("cartCookieKey") ?>'
  var cartCookieDuration = '<?= c::get("cartCookieDuration") ?>'
  var cartCookiePath = '<?= c::get("cartCookiePath") ?>'
  var cartCookieDomain = '<?= c::get("cartCookieDomain") ?>'
  var ajaxCallUrl = '<?= $site->url() ."/assets/php/cart-json-from-articles-string.php" ?>'
</script>

<script src="<?= $site->url() ."/assets/js/cartFunctions.js" ?>"></script>