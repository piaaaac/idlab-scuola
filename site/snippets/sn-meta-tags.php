<meta property="og:title" content="<?= $title ?>" />
<meta property="og:type" content="website" />
<meta property="og:image" content="<?= $snImgFile ?>" />
<meta property="og:url" content="<?= $currentUrl; ?>" />
<meta property="og:description" content="<?= $desc ?>" />
<meta property="og:site_name" content="Scuola Super" />
<!-- <meta property="fb:app_id" content="" /> -->
<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@" />
<meta name="twitter:title" content="<?= $title ?>" />
<meta name="twitter:text:title" content="<?= $title ?>" />
<meta name="twitter:description" content="<?= $desc ?>" />
<meta name="twitter:image" content="<?= $snImgFile ?>" />


