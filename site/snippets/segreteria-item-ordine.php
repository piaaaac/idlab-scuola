<?php
/* ----------------------------
<< 
  $ordine
  $displayDate
  $processedOrders
  $returnMode

>> HTML
---------------------------- */

$state = null;



if($ordine->emailVerificata() == "0" && $ordine->pagamentoOk() == "0"){ 
  $state = "new";
} elseif($ordine->emailVerificata() == "1" && $ordine->pagamentoOk() == "0"){ 
  $state = "open";
} elseif($ordine->pagamentoOk() == "1"){ 
  $state = "payed";
}

// all
$zIndex = 1;
// conditional
if($state === "new"){

} elseif($state === "open"){

  // $zIndex = 1000 - ($processed++) + count($ordiniDaPagare); // error undefined
  $zIndex = 10000 - ($processedOrders++);

} elseif($state === "payed"){

  $tooltipHtml = "<small>". $ordine->pagamentoRiferimento()->value() ."<br />-<br />". $ordine->note()->kirbytext() ."</small>";
  
}

$idCode = $ordine->orderId()->value();
?>
  
  <div class="d-md-flex justify-content-between align-items-center my-4 my-md-3" style="z-index: <?= $zIndex ?>">
  <!-- <div class="top-bottom-bordered d-flex justify-content-between align-items-center my-2" style="z-index: <?= $zIndex ?>"> -->

    <div class="left">
      <?= $displayDate ." - ". $ordine->form_cognome()->value() ." ". $ordine->form_nome()->value() ." - ". $ordine->importoTotale()->value() ." EUR" ?>
      <br />
      <?= badge($ordine) ?>
      <span class="font-sans-ss font-color-black40"><?= $ordine->title()->value() ?></span>
      <a class="font-sans-sss font-color-blue ml-1" target="_blank" href="<?= page('iscrizione2')->url() ."/order:". $ordine->orderId()->value() ?>">
        <i class="fas fa-external-link-alt d-inline-block mr-1"></i>Ordine</a>
      <a class="font-sans-sss font-color-blue ml-1" target="_blank" href="<?= $site->url() ."/recap-ordine-print/order:". $ordine->orderId() ?>">
        <i class="fas fa-external-link-alt d-inline-block mr-1"></i>Stampa</a>
<!--       <a class="font-sans-sss font-color-blue ml-1" target="_blank" href="<?= $site->url() ."/panel/pages/segreteria-ordini/o". $ordine->orderId()->value() ."/edit" ?>">
        <i class="fas fa-external-link-alt d-inline-block mr-1"></i>Panel</a>
 -->      <?php if($state === "payed" && $ordine->emailVerificata() == "0"): ?>
        <br />(Email non verificata)
      <?php endif ?>
    </div>

    <?php if($state === "new"): ?>
    
    <div class="right">
      <form id="verify-email-<?= $idCode ?>" name="verify-email" action="<?= page("iscrizione2verifyemail")->url() ."/order:". $ordine->orderId()->value() ?>" method="post">
        <input type="hidden" id="code-<?= $idCode ?>" name="code" value="<?= $ordine->verificationCode()->value() ?>">
        <button class="btn btn-primary black-light btn-small" type="submit" id="submitt-<?= $idCode ?>">ATTIVA ORDINE&nbsp;&rarr;</button>
      </form>
    </div>

    <?php elseif($state === "open"): ?>

<!--     <div class="right">
      <div class="btn-group" role="group">
        <button id="btnGroupDrop1-<?= $idCode ?>" type="button" class="btn btn-primary black-light btn-small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Conferma pagamento
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1-<?= $idCode ?>">
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="bonifico" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Bonifico bancario</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="contanti" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Contanti</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="pos" data-orderid="<?= $ordine->orderId()->value() ?>"
          >POS</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="assegno" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Assegno</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="paypal" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Paypal</a>
        </div>
      </div>
    </div>
 -->


    <div class="right">
      <a class="btn btn-primary black-light btn-small"
      href="javascript:;" 
      data-toggle="modal" data-target="#modalConferma"
      data-orderid="<?= $ordine->orderId()->value() ?>"
      >
        Conferma pagamento…
      </a>
<!--  

      <div class="btn-group" role="group">
        <button id="btnGroupDrop1-<?= $idCode ?>" type="button" class="btn btn-primary black-light btn-small dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          Conferma pagamento
        </button>
        <div class="dropdown-menu" aria-labelledby="btnGroupDrop1-<?= $idCode ?>">
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="bonifico" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Bonifico bancario</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="contanti" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Contanti</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="pos" data-orderid="<?= $ordine->orderId()->value() ?>"
          >POS</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="assegno" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Assegno</a>
          <a class="dropdown-item" data-toggle="modal" data-target="#modalConferma" href="javascript:;"
            data-method="paypal" data-orderid="<?= $ordine->orderId()->value() ?>"
          >Paypal</a>
        </div>
      </div>
-->
    </div>


    <?php elseif($state === "payed"): ?>

    <div class="right">
      <div class="d-flex align-items-center">
        <i class="fas fa-info-circle d-inline-block mr-1"></i>
        <span class="font-sans-ss pt-1" data-toggle="tooltip" data-html="true" data-placement="top" title="<?= $tooltipHtml ?>">Pagato tramite <?= $ordine->pagamentoMetodo()->value() ?></span>
        <?php if($ordine->stampato() == "1"): ?>
          <a href="<?= $page->url() ."?printed=0&mode=$returnMode&orderId=". $ordine->orderId()->value() ?>" title="Ordine stampato e archiviato" class="d-inline-block ml-3">
            <i class="fas fa-check-square"></i>
            <i class="fas fa-file"></i>
          </a>
        <?php else: ?>
          <a href="<?= $page->url() ."?printed=1&mode=$returnMode&orderId=". $ordine->orderId()->value() ?>" title="Ordine stampato e archiviato" class="d-inline-block ml-3">
            <i class="far fa-square"></i>
            <i class="far fa-file"></i>
          </a>
        <?php endif ?>
      </div>
    </div>

    <?php endif ?>

  </div>
  <hr />
