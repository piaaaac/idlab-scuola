<?php include_once(kirby()->roots()->snippets() .'/load-cart-js-functions.php') ?>

<script>
  $(document).ready(function(){
    $.ajax({
      url: ajaxCallUrl,
      success: function(result){
        updateCart(JSON.parse(result))
      }
    });
  });
</script>

<div id="cart-cover" onclick="toggleCartPanel(false);"></div>

<div id="cart">
  
  <div class="header mb-5">
    <div class="d-flex justify-content-between align-items-center">
      <h1>Le tue iscrizioni</h1>
      <h4><a href="javascript:toggleCartPanel(false);">&times;</a></h4>
    </div>
  </div>
  
  <div>
    <ul id="cartItemList"></ul>
  </div>

  <div id="cart-empty" class="mt-5 d-none">
    <p class="font-sans-s my-1">Il carrello è vuoto.</p>
    <p class="font-sans-s my-1"><a class="font-color-white" href="<?= page('corsi')->url() ?>">Aggiungi un corso &rarr;</a></p>
  </div>

  <div id="cart-totale" class="d-none">
    <div class="bordered d-flex justify-content-between mt-5">
      <p class="font-color-white font-sans-s">Totale</p>
      <p class="font-color-blue font-sans-s ammontare"></p>
    </div>
    <div class="mt-3">
      <a class="btn btn-primary" href="<?= page('iscrizione1')->url() ?>" role="button">
        ACQUISTA <i class="far fa-credit-card d-inline-block"></i>
      </a>
      <a class="btn btn-secondary ml-2" href="javascript:clearCart();" role="button">
        SVUOTA
      </a>
    </div>
  </div>

</div>



