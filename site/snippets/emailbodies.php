<?php

function verificaEmail($ordine, $verificationCode){
  // --- email body ---------------------------------
  $body = "
Ciao ". $ordine->form_nome()->value() .",
usa il codice $verificationCode per verificare la tua email e continuare con l'iscrizione ai corsi.
Puoi accedere al tuo ordine in qualsiasi momento utilizzando questo link: ". page("iscrizione2")->url() ."/order:". $ordine->orderId()->value() ."\n
". c::get('emailSignature');
  // ------------------------------------------------

  // email object and return
  return email(array(
    'to'      => $ordine->form_email()->value(),
    'subject' => "Ordine ". $ordine->orderId()->value() ." - Verifica email",
    'body'    => $body
  ));
}


function confermaPagamento($ordine){
  // --- email body ---------------------------------
  $body = "
Ciao ". $ordine->form_nome()->value() .",
Il pagamento del tuo ordine è avvenuto con successo.
Benvenuto/a alla Scuola SUPERiore di Arti Applicate di Milano!
\n
Puoi accedere al tuo ordine utilizzando questo link: ". page("iscrizione2")->url() ."/order:". $ordine->orderId()->value() ."\n
". c::get('emailSignature');
  // ------------------------------------------------

  // email object and return
  return email(array(
    'to'      => $ordine->form_email()->value(),
    'subject' => "Ordine ". $ordine->orderId()->value() ." - Pagamento eseguito",
    'body'    => $body
  ));
}


?>