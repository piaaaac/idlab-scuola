<?php 
// $textColor => "black-text" | "white-text"

// retreive cart count
$cartArticleeeeIdsString = a::get($_COOKIE, c::get("cartCookieKey"));
$cartArticles = [];
if($cartArticleeeeIdsString){
  $cartArticles = explode(",", $cartArticleeeeIdsString);
}
if(!$cartArticles || count($cartArticles)==0){
  $showCartNum = false;
  $cartNumber = null;
} else {
  $showCartNum = true;
  $cartNumber = count($cartArticles);
}

$menuPages = array(
  page('corsi'),
  "/",
  page('lapis'),
  "/",
  page('la-scuola'),
  "/",
  page('crowdfunding'),
  "/",
  page('info'),
  "/",
  "search",
  "/",
  "cart"
);
?>

<?php foreach($menuPages as $i => $item): ?>
  
  <?php if($item === "/"): ?>

    <span class="separator menu-font-size <?= $textColor ?>">/</span>

  <?php elseif($item === "search"): ?>

    <a class="menu-item menu-font-size <?= $textColor ?>" href="javascript:toggleSearch()"><i class="fas fa-search"></i></a>

  <?php elseif($item === "cart"): ?>

    <a class="menu-item menu-font-size <?= $textColor ?>" href="javascript:toggleCartPanel(true)"><i class="fas fa-shopping-cart"></i></a>
    <a class="cart-number-dot d-inline-flex justify-content-center align-items-center <?= r($showCartNum == false, "hide") ?>" href="javascript:toggleCartPanel(true)">
      <span class="cart-number font-sans-sss"><?= $cartNumber ?></span>
    </a>

  <?php else: ?>

    <a class="menu-item menu-font-size <?= $textColor ?><?= r($item->isOpen(), ' active') ?>" href="<?= $item->url() ?>">
      <?= ($item->uid() === "info") ? "Info" : $item->title()->html() ?></a>

  <?php endif ?>

<?php endforeach ?>



