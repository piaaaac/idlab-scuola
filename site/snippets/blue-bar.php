<div class="bar-blue">
  <div class="container-fluid super-cont">
    <div class="row">
      <div class="col">
        <div class="h d-flex align-items-center justify-content-between flex-wrap flex-sm-nowrap">
          <div class="font-sans-l mt-4 mb-sm-3 mb-0 mr-3"><?= $page->fasciaBlue()->kt() ?></div>
          <a class="btn btn-primary btn-small white my-3" href="<?= $page->fasciaBlueBtnUrl()->toUrl() ?>" role="button"><?= $page->fasciaBlueBtnTxt()->upper() ?> &rarr;</a>
        </div>
      </div>
    </div>
  </div>
</div>
