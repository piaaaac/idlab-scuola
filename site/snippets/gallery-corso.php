
<?php
/**
 * @param $id - string, only with multiple galleries on page
 * @param $items - Kirby Collection
 */

$gid = $id ?? "default";
?>

<div id="<?= "gallery-$gid" ?>">
  <div class="gallery-overlay">
    <div class="gallery-container d-flex justify-content-between w-100 h-100">
      
      <div class="sx justify-content-start">
        <a class="no-underline font-sans-l" href="javascript:showPrevPic();">&larr;</a>
      </div>
        
      <div class="center flex-grow w-100 h-100">
        <?php $i = 0; ?>
        <?php foreach($items as $item): ?>
          <div id="<?= "gi-$gid-$i" ?>" class="gallery-item w-100 h-100">

            <?php if($item->giimage() && $image = $item->giimage()->toFile()): ?>
              <div class="flex-grow-1 w-100" style="
              background-image: url(<?=$image->url()?>);
              background-size: contain;
              background-repeat: no-repeat;
              background-position: center center;
              "> </div>
            <?php endif ?>

            <div class="mt-3 flex-stretch">
              <p class="font-sans-s"><?= $item->gilabel()->value() ?></p>
              <p class="font-sans-sss mt-2"><?= $item->gicredits()->value() ?></p>
            </div>
          </div>
          <?php $i++; ?>
        <?php endforeach ?>
      </div>

      <div class="dx justify-content-end">
        <a class="no-underline font-sans-l" href="javascript:showNextPic();">&rarr;</a>
      </div>

      <a class="no-underline font-sans-l close-button" href="javascript:closeGallery();">&times;</a>

    </div>
  </div>
</div>

<?php /* PREV VERSION OK

<?php
/**
 * @param $items - Kirby Collection
 * /
?>

<div class="gallery-overlay">
  <div class="gallery-container d-flex justify-content-between w-100 h-100">
    
    <div class="sx justify-content-start">
      <a class="no-underline font-sans-l" href="javascript:showPrevPic();">&larr;</a>
    </div>
      
    <div class="center flex-grow w-100 h-100">
      <?php $i = 0; ?>
      <?php foreach($items as $item): ?>
        <div id="<?= "gi-$i" ?>" class="gallery-item w-100 h-100">

          <?php if($item->giimage() && $image = $item->giimage()->toFile()): ?>
            <div class="flex-grow-1 w-100" style="
            background-image: url(<?=$image->url()?>);
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
            "> </div>
          <?php endif ?>

          <div class="mt-3 flex-stretch">
            <p class="font-sans-s"><?= $item->gilabel()->value() ?></p>
            <p class="font-sans-sss mt-2"><?= $item->gicredits()->value() ?></p>
          </div>
        </div>
        <?php $i++; ?>
      <?php endforeach ?>
    </div>

    <div class="dx justify-content-end">
      <a class="no-underline font-sans-l" href="javascript:showNextPic();">&rarr;</a>
    </div>

    <a class="no-underline font-sans-l close-button" href="javascript:closeGallery();">&times;</a>

  </div>
</div>
*/ ?>