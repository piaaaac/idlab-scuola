<?php
if($site->user() && !$site->user()->hasRole('admin')){

  // user logged but not admin role
  echo "Utente non abilitato. Esegui nuovamente il <a href=\"". $site->url() ."/panel\">login</a> al pannello, o vai alla <a href=\"". $site->url() ."\">homepage</a>.";
  exit();

} else if(!$site->user()){

  // no login detected
  echo "Area riservata. Esegui il <a href=\"". $site->url() ."/panel\">login</a> al pannello, o vai alla <a href=\"". $site->url() ."\">homepage</a>.";
  exit();

} else if($site->user() && !$site->user()->hasRole('admin')){

  // OK !!! loged user has admin role
  $userIsAdmin = true;

}
?>
