<?php
/* ----------------------------
<< 
  $item                   corso con iscrizioni
  $mode                   complete|simple
  $maxIscrittiXTurno      max val for progress bar
  $maxIscrittiXCorso      max val for progress bar
  $maxOrdiniXCorso        max val for progress bar
  $maxOrdiniXTurno        max val for progress bar

>> HTML
---------------------------- */

$fontColorType = $item->fields["tipo"] === "speciali" ? "font-color-gold" : "font-color-blue";
$bgColorType = $item->fields["tipo"] === "speciali" ? "bg-color-gold" : "bg-color-blue";
$maxScale = $maxOrdiniXCorso;

?>

<?php if($mode === "simple"): ?>

  <?php 
    $nIscrittiCorso = 0;
    $nOrdiniCorso = 0;
    foreach($item->turni as $t){
      $nIscrittiCorso += count($t->ordiniPagati);
      $nOrdiniCorso += count($t->ordini);
    }
    $fontColorType2 = $nIscrittiCorso > 0 ? $fontColorType : "font-color-black20";
    $nOrdiniAperti = $nOrdiniCorso - $nIscrittiCorso;
    $val100Iscritti = mapTo100($nIscrittiCorso, $maxScale);
    $val100Ordini = mapTo100($nOrdiniAperti, $maxScale);
  ?>
    
  <div class="corso mt-1">
    <div class="corso-titolo">
      <div class="d-flex justify-content-between align-items-start mt-2">
        <a class="font-sans-s hover-black flex-grow-1 mr-2 <?= $fontColorType2 ?>" href="?corsoid=<?= $item->fields["corsoid"] ?>">
          <?php if(!$item->kirbyObj->isVisible()): ?>
            <i class="fas fa-eye-slash"></i>
          <?php endif ?>
          <?= $item->fields["title"] ?>
        </a>
        <?php if($nOrdiniAperti > 0): ?>
          <!-- <span class="font-sans-ssss mt-1 mr-1"><i class="fas fa-dot-circle font-color-red"></i></span> -->
          <span class="font-sans-sss font-color-red mt-1">+<?= $nOrdiniAperti ?></span>
        <?php endif ?>
        <span class="n-iscritti-simple font-sans-sss mt-1 text-right <?= $fontColorType2 ?>"><?= $nIscrittiCorso ?> iscritti</span>
      </div>
    </div>
    <div class="progress">
      <div class="progress-bar <?=$bgColorType?>" role="progressbar" style="width: <?= $val100Iscritti ?>%;" aria-valuenow="<?= $val100Iscritti ?>" aria-valuemin="0" aria-valuemax="100"></div>
      <?php if($nOrdiniAperti > 0): ?>
        <div class="progress-bar second bg-color-red" role="progressbar" style="width: <?= $val100Ordini ?>%;" aria-valuenow="<?= $val100Ordini ?>" aria-valuemin="0" aria-valuemax="100"></div>
      <?php endif ?>
    </div>
  </div>

<?php elseif($mode === "complete" || $mode === "completeLarge"): ?>

  <?php
    $mt = $mode === "completeLarge" ? "mt-3" : "mt-3";
    $mb = $mode === "completeLarge" ? "mb-3" : "mb-2";
  ?>

  <div class="corso <?=$mt?> mb-5">
    <div class="corso-titolo">
      <p class="<?=$mb?>">
        <!-- <a class="font-sans-s font-color-blue d-block" href="<?= $item->kirbyObj->url() ?>"> -->
        <a class="font-sans-s d-block <?= $fontColorType ?>" href="?corsoid=<?= $item->fields["corsoid"] ?>">
          <?= $item->fields["title"] ?>
          <br />(<?= $item->fields["corsoid"] ?>)
        </a>
      </p>
    </div>
    
    <?php foreach($item->turni as $t): ?>
      <?php 
        $nIscritti = count($t->ordiniPagati);
        $nOrdini = count($t->ordini);
        $nOrdiniAperti = $nOrdini - $nIscritti;
        $fontColorGrey =  ($nIscritti > 0) ? "font-color-black" : "font-color-black20";
        $vuoto =          ($nIscritti > 0) ? ""                 : "vuoto";
        $val100Iscritti = mapTo100($nIscritti, $maxScale);
        $val100Ordini = mapTo100($nOrdiniAperti, $maxScale);
        $fontSize = $mode === "completeLarge" ? "font-sans-ss" : "font-sans-sss";
      ?>

      <div class="turno <?= $vuoto ?>">
        <!-- <div class="text"> -->
          <span class="<?= "$fontSize $fontColorGrey" ?>">
            <?= $t->fields["title"] ?>
          </span>
          <?php if($nOrdini > 0): ?>
            <br />
            <?php if($nIscritti > 0): ?>
              <span class="text-nowrap <?="$fontSize $fontColorType"?> mr-1">
                <?= $nIscritti ?> iscritti
              </span>
            <?php endif ?>
            <?php if($nOrdiniAperti > 0): ?>
              <span class="text-nowrap font-color-red <?=$fontSize?>">
                <?= $nOrdiniAperti ?> ordini aperti
              </span>
            <?php endif ?>
          <?php endif ?>
        <!-- </div> -->
        <div class="progress">
          <div class="progress-bar <?=$bgColorType?>" role="progressbar" style="width: <?= $val100Iscritti ?>%;" aria-valuenow="<?= $val100Iscritti ?>" aria-valuemin="0" aria-valuemax="100"></div>
          <?php if($nOrdiniAperti > 0): ?>
            <div class="progress-bar second bg-color-red" role="progressbar" style="width: <?= $val100Ordini ?>%;" aria-valuenow="<?= $val100Ordini ?>" aria-valuemin="0" aria-valuemax="100"></div>
          <?php endif ?>
        </div>
      </div>

    <?php endforeach ?>
  </div>

<?php endif ?>
