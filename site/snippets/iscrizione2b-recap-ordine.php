<?php
echo "
<!--
---------------------------------
snippet iscrizione2b-recap-ordine
---------------------------------
-->
";

extract($orderData);
extract($otherData);
if($orderIsPayed === false){
  extract($paypalForm);
}

// a::show($orderData);
// a::show($otherData);
// a::show($cartArray);
// exit();

$goBackLink = page("iscrizione1")->url() ."/order:". param("order");
$ordine = page('segreteria-ordini')->children()->findBy("orderId", $orderId);
?>

<?php if(userIsAdmin($site) == true): ?>
  <div class="container-fluid super-cont">
    <div class="row">
      <div class="col">
        <div class="alert alert-primary d-flex justify-content-start align-items-center" role="alert">

          <?php if($emailVerificata): ?>
            <i class="fas fa-check-square"></i>
          <?php else: ?>
            <i class="far fa-square"></i>
          <?php endif ?>
          <span class="ml-2 mt-1 mr-4">
            Email verificata
          </span>

          <?php if($orderIsPayed): ?>
            <i class="fas fa-check-square"></i>
          <?php else: ?>
            <i class="far fa-square"></i>
          <?php endif ?>
          <span class="ml-2 mt-1 mr-4">
            Ordine pagato
          </span>

          <!-- <i class="fas fa-sliders-h"></i> -->
          <span class="ml-2 mt-1 mr-4">
            <a href="<?= $site->url() ."/panel/pages/segreteria-ordini/". $ordine->uid() ."/edit" ?>">Vedi l'ordine nel pannello &rarr;</a>
          </span>
          <span class="ml-2 mt-1">
            <a href="<?= $site->url() ."/recap-ordine-print/order:". $ordine->orderId() ?>" target="_blank">Stampa &rarr;</a>
          </span>

        </div>
        <?php if(!$emailVerificata): ?>
          <form id="verify-email" name="verify-email" action="<?= page("iscrizione2verifyemail")->url() ."/order:". $ordine->orderId()->value() ?>" method="post">
            <input type="hidden" id="code" name="code" value="<?= $ordine->verificationCode()->value() ?>">
            <div class="alert alert-primary d-flex justify-content-start align-items-center" role="alert">
              <i class="fas fa-pencil-alt"></i>
              <!-- <i class="material-icons md-18">warning</i> -->
              <span class="ml-3 mt-1">
                Email non verificata (codice verifica: <?= $ordine->verificationCode()->value() ?>).
                <button class="btn btn-link attiva-button" type="submit" id="submitt">Attiva l'ordine ora &rarr;</button>
              </span>
            </div>
          </form>
        <?php endif ?>
      </div>
    </div>
  </div>
<?php endif ?>

<div class="container-fluid super-cont">
  <div class="row">
    
    <div class="col-lg-8">

      <?php 
      // if($orderIsPayed === true): 
      if(true): 
      ?>

        <div class="row">
          <div class="col-12">
            <p class="font-sans-s font-color-blue mt-3">ORDINE <?= $orderId ?></p>
            <hr class="mt-1 mb-0"/>
          </div>
        </div>

        <div class="row mb-4 pt-2">
          <div class="col-md">
            <em>Creato: </em>
            <?= $ordine->form_dataOraOrdine()->value() ?>
            <br />
            <em>Attivato: </em>
            <?php if($d = $ordine->dataOraVerifica()->value()){ echo $d; } else { echo "—"; } ?>
            <br />
            <em>Pagato: </em>
            <?php if($d = $ordine->dataOraPagamento()->value()){ echo $d; } else { echo "—"; } ?>
            <br />
          </div>
          <div class="col-md">
            <em> </em>
            <?= " " ?>
            <br />
          </div>
        </div>

      <?php endif ?>

      <div class="row">
        <div class="col-12">
          <p class="font-sans-s font-color-blue mt-3">DATI PERSONALI / PERSONAL DETAILS</p>
          <hr class="mt-1 mb-0"/>
        </div>
      </div>

      <div class="row mb-4 pt-2">
        <div class="col-md">
          <em>Cognome / Surname: </em>
          <?= $form_cognome ?>
          <br />
          <em>Nome / Name: </em>
          <?= $form_nome ?>
          <br />
          <em>Codice fiscale / Only for Italian residents: </em>
          <?= $form_codiceFiscale ?>
          <br />
          <em>Telefono / Phone number: </em>
          <?= $form_telefono ?>
          <br />
          <em>Email: </em>
          <?= $form_email ?>
          <br />
        </div>
        <div class="col-md">
          <em>Cittadinanza / Nationality: </em>
          <?= $form_cittadinanza ?>
          <br />
          <em>Data di nascita / Date of birth: </em>
          <?= $form_dataNascita ?>
          <br />
          <em>Luogo di nascita / Place of birth: </em>
          <?= $form_luogoNascita ?>
          <br />
          <em>Provincia / Province: </em>
          <?= $form_provinciaNascita ?>
          <br />
        </div>
      </div>

      <div class="row">
        <div class="col-12">
          <p class="font-sans-s font-color-blue mt-3">RESIDENZA / ADDRESS</p>
          <hr class="mt-1 mb-4"/>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-md">
          <em>Città (o stato estero) / City (or state if abroad): </em>
          <?= $form_residenzaLuogo ?>
          <br />
          <em>Indirizzo / Street name: </em>
          <?= $form_residenzaIndirizzo ?>
          <br />
          <em>N° civico / Nr: </em>
          <?= $form_residenzaCivico ?>
          <br />
        </div>
        <div class="col-md">
          <em>CAP / ZIP code: </em>
          <?= $form_residenzaCap ?>
          <br />
          <em>Provincia: </em>
          <?= $form_residenzaProvincia ?>
          <br />
        </div>
      </div>

      <?php if($fatturazioneAziendale): ?>
        <div class="row">
          <div class="col-12">
            <p class="font-sans-s font-color-blue mt-3">DATI DI FATTURAZIONE AZIENDALI / INVOICE DETAILS</p>
            <hr class="mt-1 mb-4"/>
          </div>
        </div>

        <div class="row mb-4">
          <div class="col-md">
            <em>Ragione sociale: </em>
            <?= $form_aziendaRagioneSociale ?>
            <br />
            <em>Codice fiscale: </em>
            <?= $form_aziendaCodiceFiscale ?>
            <br />
            <em>Partita Iva: </em>
            <?= $form_aziendaPartitaIva ?>
            <br />
            <em>Luogo: </em>
            <?= $form_aziendaLuogo ?>
            <br />
          </div>
          <div class="col-md">
            <em>Indirizzo / Street name: </em>
            <?= $form_aziendaIndirizzo ?>
            <br />
            <em>N° civico / Nr: </em>
            <?= $form_aziendaCivico ?>
            <br />
            <em>CAP / ZIP code: </em>
            <?= $form_aziendaCap ?>
            <br />
            <em>Provincia / Province: </em>
            <?= $form_aziendaProvincia ?>
            <br />
          </div>
        </div>
      <?php endif ?>

      <div class="row">
        <div class="col-12">
          <p class="font-sans-s font-color-blue mt-3">ISCRIZIONE AI TURNI / ENROLMENTS</p>
          <hr class="mt-1 mb-4"/>
        </div>
      </div>

      <div class="row mb-4">
        <?php 
        $n = 1;
        foreach ($ordine->iscrizioni()->toStructure() as $iscrizione): 

          $articleeeeId = $iscrizione->iscrizione_articleeeeId()->value();
          $corsoId = explode("~", $articleeeeId)[0];
          $turnoUid = explode("~", $articleeeeId)[1];
          $corso = page("corsi")->children()->findBy("corsoId", $corsoId);
          if(!$corso){
            go(page('error')->url() ."/err:3386526166");
          } 
          $turno = $corso->children()->findBy("uid", $turnoUid);
          if(!$turno){
            go(page('error')->url() ."/err:3386526167");
          }
          $turnoDatesString = turnoDates($turno);
          $line1 = $corso->title()->value();
          $line2 = $turno->title()->value() . $turnoDatesString;
          ?>
          <div class="col-12">
            <?= "Iscrizione / Enrolment #$n" ?> &mdash; <?= $line1 ?> <span class="font-sans-sss"><?= $line2 ?></span>
            <br />
            <br />
            <em>Cognome / Surname: </em>
            <?= $iscrizione->iscrizione_cognome()->value() ?>
            <br />
            <em>Nome / Name: </em>
            <?= $iscrizione->iscrizione_nome()->value() ?>
            <br />
            <em>Email: </em>
            <?= $iscrizione->iscrizione_email()->value() ?>
            <br />
            <em>Telefono: </em>
            <?= $iscrizione->iscrizione_telefono()->value() ?>
            <hr class="my-3"/>
          </div>        
          <?php 
          $n++;
        endforeach ?>
      </div>

      <div class="row">
        <div class="col-12">
          <p class="font-sans-s font-color-blue mt-3">REGOLAMENTO CORSI E TRATTAMENTO DATI / COURSES' REGULATION AND PERSONAL DATA PROCESSING</p>
          <hr class="mt-1 mb-4"/>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-9">
          <strong><?= r($form_checkbox1, "SI", "NO") ?></strong>
          <span class="font-sans-ss">
            – Dichiaro di aver preso visione e di accettare il <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/7-servizi-erogati/regolamento_corsi.pdf" target="_blank">Regolamento dei corsi</a> e quanto contenuto nell'<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/12-emergenza-covid-19/informativa_covid_19_terzi.pdf" target="_blank">Informativa Covid-19</a> (obbligatorio per poter proseguire)
            <br />
            / I have read and accept the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/7-servizi-erogati/regolamento_corsi.pdf" target="_blank">Courses' Regulations</a> and the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/12-emergenza-covid-19/informativa_covid_19_terzi.pdf" target="_blank">Covid-19 informative report</a> (required in order to continue)
          </span>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9">
          <strong><?= r($form_checkbox2, "SI", "NO") ?></strong>
          <span class="font-sans-ss">
            – Dichiaro di aver preso visione dell’<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Informativa Privacy</a> e acconsento al trattamento dei miei dati personali (obbligatorio per poter proseguire)
            <br />
            / I have read the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Privacy Policy</a> and consent to the processing of my personal data (required in order to continue)
          </span>
        </div>
      </div>

      <div class="row mb-4">
        <div class="col-sm-9">
          <strong><?= r($form_checkbox3, "SI", "NO") ?></strong>
          <span class="font-sans-ss">
            – Dichiaro di aver preso visione dell’<a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Informativa Privacy</a> a acconsento al trattamento dei miei dati personali per ricevere newsletter o comunicazioni relative ai servizi offerti dalla Scuola Superiore d’Arte Applicata (facoltativo)
            <br />
            / I have read the <a class="font-sans-ss font-color-gold" href="https://scuolaarteapplicata.it/super/content/15-amministrazione-trasparente/10-privacy/informativa_privacy.pdf" target="_blank">Privacy Policy</a> and consent to the processing of my personal data to receive newsletters or communications relating to the services offered by the Higher School of Applied Art (optional)
          </span>
        </div>
      </div>

    </div>

    <div class="col-lg-4">

      <?php if($orderIsPayed === false): ?>

      

      <!-- Order is not paid -->

        <div class="row">
          <div class="col-12">
            <p class="font-sans-s font-color-blue mt-3">CORSI SELEZIONATI / SELECTED COURSES</p>
            <hr class="mt-1 mb-0"/>
          </div>
        </div>

        <div class="cart-recap">
          <?php foreach($cartArray as $item): ?>

            <?php
            $corso = page("corsi")->children()->findBy("corsoId", $item->corso->corsoId);
            if(!$corso){
              go(page('error')->url() ."/err:3316516176");
            } 
            $turno = $corso->children()->findBy("uid", $item->turno->uid);
            if(!$turno){
              go(page('error')->url() ."/err:3316516177");
            }
            ?>

            <div class="cart-recap-item d-flex justify-content-between align-items-center pt-3 pb-2">
            
              <div class="title">
                <span class="d-inline-block font-sans-s mr-2">
                  <?= $item->corso->title ?>
                </span>
                <span class="d-inline-block font-sans-sss">
                  <!-- (0) -->
                  <?= $item->turno->nome . turnoDates($turno) ?>
                </span>
              </div>

              <div class="sconto font-sans-sss">
                <?php if(property_exists($item, "sconto")): ?>
                  <?= $item->sconto->description ?>
                <?php endif ?>
              </div>

              <div class="costo">
                &euro;&nbsp;<?= $item->costoFinale ?>
              </div>

            </div>
          <?php endforeach ?>

          <div class="d-flex justify-content-between my-3">
            <p class="spacer">&nbsp;</p>
            <p class="text-right">Totale &euro;&nbsp;<span id="total-cart-price"><?= $importoTotale ?></span></p>
          </div>

        </div>

      <?php elseif($orderIsPayed === true): ?>



      <!-- Order is paid -->

        <div class="row">
          <div class="col-12">
            <p class="font-sans-s font-color-blue mt-3">CORSI ACQUISTATI</p>
            <hr class="mt-1 mb-0"/>
          </div>
        </div>

        <div class="cart-recap">


          <?php if($cartArray = json_decode($ordine->cartArray()->value())): ?>
            
            <?php foreach($cartArray as $item): ?>
              <div class="cart-recap-item d-flex justify-content-between align-items-center pt-3 pb-2">
              
                <div class="title">
                  <span class="d-inline-block font-sans-s mr-2"><?= $item->corso->title ?></span>
                  <span class="d-inline-block font-sans-sss">
                    <!-- (1) -->
                    <?php if(property_exists($item->turno, "nome")): ?>
                      <?= $item->turno->nome ?>
                    <?php else: ?>
                      <?= $item->turno->uid ?>
                    <?php endif ?>
                  </span>
                </div>

                <div class="sconto font-sans-sss">
                  <?php if(property_exists($item, "sconto")): ?>
                    <?= $item->sconto->description ?>
                  <?php endif ?>
                </div>

                <div class="costo">
                  &euro;&nbsp;<?= $item->costoFinale ?>
                </div>

              </div>
            <?php endforeach ?>

          <?php else: ?>

            <!-- THIS SHOULD NEVER HAPPEN SINCE ALL ORDERS SEEM TO HAVE CARTARRAY -->

            <?php foreach(explode("\n", $ordine->paymentRecord()->value()) as $item): ?>
              <?php 
              $articleeeeId = explode(":EUR", $item)[0];
              $corsoId = explode("~", $articleeeeId)[0];
              $turnoUid = explode("~", $articleeeeId)[1];
              $payed = explode(":EUR", $item)[1];
              $corso = page("corsi")->children()->findBy("corsoId", $corsoId);
              if(!$corso){
                echo "Non è stato trovato il corso relativo al pagamento (Item='$item', Error code 09608960).";
                exit();
              }
              $turno = $corso->children()->findBy("uid", $turnoUid);
              if(!$turno){
                echo "Non è stato trovato il turno relativo al pagamento (Item='$item', Error code 09608961).";
                exit();
              }
              $itemName = $corso->title()->value(); 
              // .", ". $turno->dataInizio()->value() ." &mdash; ". $turno->dataFine()->value();
              ?>

              <div class="cart-recap-item d-flex justify-content-between align-items-center pt-3 pb-2">
              
                <div class="title">
                  <span class="d-inline-block font-sans-s">
                    (v2)
                    <?= $itemName ?></span>
                </div>

                <div class="costo">
                  &euro;&nbsp;<?= $payed ?>
                </div>

              </div>
            <?php endforeach ?>
          
          <?php endif ?>

          <div class="d-flex justify-content-between my-3">
            <p class="spacer">&nbsp;</p>
            <!-- <p class="text-right">Totale &euro;&nbsp;<span id="total-cart-price">< ?= $importoTotale ?></span></p> -->
            <p class="text-right">Totale &euro;&nbsp;<span id="total-cart-price"><?= explode(" EUR", $ordine->pppaymentAmount()->value())[0] ?></span></p>
          </div>

        </div>

      <?php endif ?>

    </div>

  </div>
</div>

<?php if($orderIsPayed === false): ?>

  <!-- show payment section + paypal button -->

  <div class="container-fluid super-cont bg-color-white pb-3 mt-5">

    <div class="row mb-3 mt-5 pt-2">
      <div class="col text-center">
        <span class="font-sans-s">Effettua il pagamento per completare l’iscrizione. / Select a payment method to finalize enrolment.</span>
      </div>
    </div>

    <div class="row">
      <div class="col text-center">



        <?php /* OLD___PP
        <form method="post" action="<?= c::get('paypalFormAction') ?>">
        
          <!-- Basic info -->
          <input type="hidden" name="cmd" value="_cart">
          <input type="hidden" name="upload" value="1">
          <input type="hidden" name="business" value="<?= c::get('paypalBusinessEmail') ?>">
          <input type="hidden" name="currency_code" value="EUR">
          <input type="hidden" name="cbt" value="Return to Scuola SUPERiore">
          <input type="hidden" name="cancel_return" value="<?= page('iscrizione2')->url() ."/order:$orderId" ?>">
          <input type="hidden" name="image_url" value="http://www.alexpiacentini.com/test/idlab/scuola/assets/images/logo-scuola.png">
          <!-- <input type="hidden" name="return" value="www"> -->
          
          <!-- Articles info -->
          <?php foreach($cartArray as $i => $item): ?>
          <?php $itemName = $item->corso->title .", ". $item->turno->dataInizio ." - ". $item->turno->dataInizio; ?>
          <?php "" 
          // $discount = (float)$item->corso->costo - (float)$item->costoFinale; 
          ?>

          <input type="hidden" name="<?= "item_number_". ($i+1) ?>"       value="<?= $item->articleeeeId ?>" />
          <input type="hidden" name="<?= "item_name_". ($i+1) ?>"         value="<?= $itemName ?>" />
          <input type="hidden" name="<?= "amount_". ($i+1) ?>"            value="<?= $item->costoFinale ?>" />
          <input type="hidden" name="<?= "quantity_". ($i+1) ?>"          value="1">
          <?php 
          // <input type="hidden" name="<?= "discount_amount_". ($i+1) ?>"   value="<?= $discount ?>">
          ?>
          <?php endforeach ?>
          
          <!-- orderId ref -->
          <input name="custom" value="<?= $orderId ?>" type="hidden">

          <!-- User billing info -->
          <input type="hidden" name="first_name"      value="<?= $pp_first_name ?>">
          <input type="hidden" name="last_name"       value="<?= $pp_last_name ?>">
          <input type="hidden" name="address1"        value="<?= $pp_address1 ?>">
          <input type="hidden" name="address2"        value="<?= $pp_address2 ?>">
          <input type="hidden" name="address_country" value="<?= $pp_address_country ?>">
          <input type="hidden" name="city"            value="<?= $pp_city ?>">
          <input type="hidden" name="state"           value="<?= $pp_state ?>">
          <input type="hidden" name="zip"             value="<?= $pp_zip ?>">

          <button type="submit" id="payment-btn" class="btn btn-primary mx-auto">Paga con Paypal o carta &rarr;</button>

        </form>
        
        OLD___PP end  */ ?>



        <?php /* NEW___PP */

        $items = [];
        foreach($cartArray as $i => $item){
          $items[] = [
            "name" => $item->articleeeeId,
            "description" => $item->corso->title .", ". $item->turno->dataInizio ." - ". $item->turno->dataInizio,
            "unit_amount" => [
              "currency_code" => "EUR",
              "value" => "$item->costoFinale"
            ],
            "quantity" => "1"
          ];
        }

        ?>

        <?php /* SANDBOX
        <script src="https://www.paypal.com/sdk/js?client-id=<?= c::get('paypalClientId') ?>&currency=EUR&debug=true"></script>
        */ ?>

        <script src="https://www.paypal.com/sdk/js?client-id=<?= c::get('paypalClientId') ?>&currency=EUR"></script>

        <div class="d-inline-block" id="pp-btn"></div>
        
        <script>

          paypal.Buttons({
            createOrder: function (data, actions) {
              return actions.order.create({
                "purchase_units": [{
                  "amount": {
                    "value": "<?= $importoTotale ?>"
                  },
                  // "items": <?= json_encode($items) ?>,
                  "invoice_id": "<?= $orderId ?>",
                  // "soft_descriptor": "Acquisto corsi (ordine <?= $orderId ?>)"
                }]
              });
            },
            onApprove: function (data, actions) {
              return actions.order.capture().then(function(details) {
                console.log(details);
                var postData = {
                  "pp_details": JSON.stringify(details)
                };
                post('<?= page("iscrizione3paypal")->url() ?>', postData);
              });
            }
          }).render('#pp-btn');




          /**
           * via https://stackoverflow.com/a/133997/2501713
           * 
           * sends a request to the specified url from a form. this will change the window location.
           * @param {string} path the path to send the post request to
           * @param {object} params the paramiters to add to the url
           * 
           * Use:
           * post('/contact/', {name: 'Johnny Bravo'});
           */

          function post (path, params) {
            var form = document.createElement('form');
            form.method = "post";
            form.action = path;

            for (var key in params) {
              if (params.hasOwnProperty(key)) {
                var hiddenField = document.createElement('input');
                hiddenField.type = 'hidden';
                hiddenField.name = key;
                hiddenField.value = params[key];
                form.appendChild(hiddenField);
              }
            }

            document.body.appendChild(form);
            form.submit();
          }


        </script>

        <?php /* end NEW___PP */ ?>

      </div>
    </div>
    
    <div class="col-md-6 offset-md-3 mt-3 mb-3">
      <div class="col text-center">
        <p class="font-sans-ss my-4">
          In alternativa è possibile effettuare il pagamento mediante bonifico bancario. Clicca <a class="font-sans-ss font-color-gold" href="<?= page("info-pagamento")->url() ."/order:$orderId" ?>">qui</a> per informazioni dettagliate.
          <br />
          / It's also possible to pay directly at the school or via bank transfer. Click <a class="font-sans-ss font-color-gold" href="<?= page("info-pagamento")->url() ."/order:$orderId" ?>">here</a> to know more.
        </p>
      </div>
    </div>

  </div>
  
<?php elseif($orderIsPayed === true): ?>

  <!-- show payment info/method -->

  <div class="container-fluid super-cont">
    <div class="row">
      <div class="col">
        <div class="alert alert-success text-center d-flex justify-content-center align-items-center" role="alert">
          <i class="fas fa-check"></i>
          <!-- <i class="material-icons md-18">done</i> -->
          <span class="ml-2">Pagamento effettuato tramite <?= $pagamentoMetodo ?>.</span>
        </div>
      </div>
    </div>
  </div>

<?php endif ?>
