      <div class="px-4 py-2 d-flex justify-content-between align-items-center bg-dark" id="admin-header">
        
        <div class="d-inline-flex align-items-center">
          <a href ="<?= $site->url() ."/panel/pages/segreteria-ordini/edit" ?>" class="d-inline-flex align-items-center mr-4">
            <i class="fas fa-cog d-block mr-1"></i>
            <!-- <i class="material-icons md-18 d-block mr-1">settings</i> -->
            <small>Admin panel</small>
          </a>
          <a href ="<?= page('segreteria-conferma-pagamenti')->url() ?>" class="d-inline-flex align-items-center">
            <i class="fas fa-clipboard-list d-block mr-1"></i>
            <!-- <i class="material-icons md-18 d-block mr-1">assignment</i> -->
            <small>Utilities</small>
          </a>
        </div>
        
        <div class="d-inline-flex align-items-center">
          <small class="text-white-50 mx-2 d-inline-block">
            <?= $site->user()->firstname() ." ". $site->user()->lastname() ?>
          </small>
          <small class="text-white-50 mx-2">
            <a href="<?= $site->url() ."/logout" ?>">
              Logout
            </a>
          </small>
        </div>

      </div>
