<?php
echo "
<!--
------------------------------------
snippet iscrizione2a-inserire-codice
------------------------------------
-->
";
?>

<div class="container-fluid super-cont">
  <div class="row">
    <div class="col">
        <p class="font-sans-s mt-3 mb-3 pt-2">CONFERMA INDIRIZZO EMAIL / EMAIL VERIFICATION</p>
        <p class="font-sans-s">
          Ti abbiamo inviato un'email con un codice di verifica. Inserisci qui il codice per continuare.
          <br />
          / We've sent you an email with a verification code. Paste the code here to continue.
        </p>
        <p>
          <form id="verify-email" name="verify-email" action="<?= page("iscrizione2verifyemail")->url() ."/order:$orderId" ?>" method="post">
            <div class="form-group mt-5 pt-1">
              <input type="text" class="form-control" id="code" name="code" placeholder="Codice">
            </div>
            <div class="d-flex flex-column align-items-center mt-5">
              <button class="btn btn-primary" type="submit" id="submitt">CONTINUA L'ISCRIZIONE / CONTINUE &nbsp;&rarr;</button>
              <a class="d-inline-block font-sans-ss mt-4 mb-1" href="<?= page("sendemail")->url() ."/emailname:verificaemail/order:$orderId" ?>">Inviami nuovamente il codice / Send code again</a>
            </div>
          </form>
        </p>
    </div>
  </div>
</div>

<script>
  $(document).ready(function(){
    $("#code").focus();
  })
</script>