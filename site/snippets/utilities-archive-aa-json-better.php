<?php

// echo ini_get('memory_limit'); exit();
ini_set('memory_limit','512M');


$allCorsi = page("corsi")->children();
// $allOrdini = page("segreteria-ordini")->children();

$betterCorsi = [];
foreach($allCorsi as $corso){
  $betterCorsi[] = $corso->getPublicContent(true);
}
// $betterOrdini = [];
// foreach($allOrdini as $ordine){
//   $betterOrdini[] = $ordine->getPublicContent(true);
// }

$betterData = [];
$betterData["corsi"] = $betterCorsi;
// $betterData["ordini"] = $betterOrdini;

header("Content-type: application/json; charset=utf-8");
echo json_encode($betterData);

