<!-- search-bar: md up -->
<div id="search-bar" class="fixed-top d-none d-md-flex align-items-center">
  <div class="container-fluid super-cont bor">
    <div class="row">
      <div class="col-12">
        <div class="h d-flex align-items-center justify-content-start">

          <div class="font-sans-l"><i class="fas fa-search"></i><span class="font-sans-ll"> Cerca:</span></div>

          <form id="search-big" class="flex-grow-1" action="<?= page('search')->url()?>" method="get">
            <input id="search-big-input" class="form-control font-sans-ll mr-2" type="search" placeholder="illustrazione vettoriale" aria-label="Search" name="q">
          </form>

          <a class="font-sans-s" href="javascript: toggleSearch()">&times;</a>

        </div>
      </div>
    </div>
  </div>
</div>

<!-- Menu bar --------------------------------------->

<!-- xs sm -->
<div class="super-menu-full d-block d-md-none fixed-top <?= $type ?>">
  <div class="container-fluid super-cont">
    <div class="row">
      <div class="h col-12 d-flex justify-content-between align-items-center">

        <a class="" href="<?= page('homepage')->url() ?>">
          <img class="logo-h-60" src="<?= $site->logoTinyBlack()->toFile()->url() ?>" />
        </a>

        <button class="hamburger hamburger--slider" type="button" onclick="javascript:toggleXsMenu();">
          <span class="hamburger-box">
            <span class="hamburger-inner"></span>
          </span>
        </button>

      </div>
    </div>
  </div>
</div>

<!-- md -->
<div class="super-menu-full d-none d-md-block d-lg-none fixed-top <?= $type ?>">
  <div class="container-fluid super-cont">
    <div class="row">
      <div class="h col-12 d-flex justify-content-between align-items-center">

        <a class="" href="<?= page('homepage')->url() ?>">
          <img class="logo-h-60" src="<?= $site->logoTinyBlack()->toFile()->url() ?>" />
        </a>

        <div class="super-menu">
          <?php snippet('menu', ["textColor" => "black-text"]) ?>
        </div>

      </div>
    </div>
  </div>
</div>

<!-- lg xl -->
<div class="super-menu-full d-none d-lg-block fixed-top <?= $type ?>">
  <div class="container-fluid super-cont">
    <div class="row">
      <div class="h col-12 d-flex justify-content-between align-items-center">
      
        <a class="" href="<?= page('homepage')->url() ?>">
          <img class="logo-h-60" src="<?= $site->logoOneLineBlack()->toFile()->url() ?>" />
        </a>

        <div class="super-menu">
          <?php snippet('menu', ["textColor" => "black-text"]) ?>
        </div>

      </div>
    </div>
  </div>
</div>