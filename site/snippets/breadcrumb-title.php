<?php
// $textColor = "white-text"; // test
if(!isset($textColor)) $textColor = "black-text"; // test
$textColorString = [
  "white-text" => "font-color-white",
  "black-text" => ""
];
?>

<div class="container-fluid super-cont">
  <div class="row">
    <div class="col breadcrumb-title <?= $textColorString[$textColor] ?>">

      <?php foreach($site->breadcrumb() as $crumb): ?>
        <?php if($crumb->uid() == "homepage"): ?>

        <?php elseif(
          $crumb->uid() === "corsi" 
          && $page->template() === "corso" 
          && $page->disciplinaUid()->value() == "corsi-speciali"
        ): ?>
          <a class="font-super-ll font-color-gold corsi-speciali" href="<?= $crumb->url() ?>">Corsi<!-- Speciali --></a>
          
        <?php elseif($crumb->uid() != $page->uid()): ?>
          <a class="font-super-ll font-color-blue" href="<?= $crumb->url() ?>"><?= html($crumb->title()) ?></a>
        
        <?php else: ?>
          <span class="font-super-ll"><?= html($crumb->title()) ?></a>
        
        <?php endif ?>
      <?php endforeach ?>

    </div>
  </div>
</div>
