<?php
date_default_timezone_set('Europe/Rome');
error_reporting(E_ALL);

/** --------------------------------------------------------
 * via https://k2.getkirby.com/docs/cookbook/creating-pages-from-frontend#save-registrations-as-structure-field
 * >> gets page, fieldName and data array
 * << 
 */
function addToStructure($p, $field, $data = array()) {
  $fieldData = $p->$field()->yaml();
  $fieldData[] = $data;
  $fieldData = yaml::encode($fieldData);
  $p->update(array(
    $field => $fieldData
  ));
}


// --------------------------------------------------------
// >> gets date/time in format AAAA-MM-DD hh:mm:ss
// << returns string
//
function dateTimeFormatted($dateStr){
  setlocale(LC_TIME, 'ita', 'it_IT');
  $date = DateTime::createFromFormat('Y-m-d H:i:s', $dateStr);
  if(!$date){
    return "&mdash;";
  }
  $formatted = strftime("%e %b %Y alle %k:%M", $date->getTimestamp());
  setlocale(LC_ALL,NULL);
  return $formatted;
}


// --------------------------------------------------------
// >> gets a date in format AAAA-MM-DD
// << returns string
//
function annoFormativo($dateStr, $corsoId){

  $casiSpeciali = [];
  $casiSpeciali["corso-70"] = "-"; // Oneri di segreteria

  if(array_key_exists($corsoId, $casiSpeciali)){
    return $casiSpeciali[$corsoId];
  }

  // setlocale(LC_TIME, 'ita', 'it_IT');
  $date = DateTime::createFromFormat('Y-m-d', $dateStr);
  if(!$date){
    go(page('error')->url() ."/err:3378978979");
  }
  $defaultString = "&mdash;";
  $string = $defaultString;
  
  $anniFormativi = [
    (object)["strAF" => "2017/2018", "inizioAF" => new DateTime('2017-06-17 00:00:00') ],
    (object)["strAF" => "2018/2019", "inizioAF" => new DateTime('2018-06-17 00:00:00') ],
    (object)["strAF" => "2019/2020", "inizioAF" => new DateTime('2019-06-17 00:00:00') ],
    (object)["strAF" => "2020/2021", "inizioAF" => new DateTime('2020-06-17 00:00:00') ],
    (object)["strAF" => "2021/2022", "inizioAF" => new DateTime('2021-06-17 00:00:00') ],
    (object)["strAF" => "2022/2023", "inizioAF" => new DateTime('2022-06-17 00:00:00') ],
    (object)["strAF" => "2023/2024", "inizioAF" => new DateTime('2023-06-17 00:00:00') ],
    (object)["strAF" => "2024/2025", "inizioAF" => new DateTime('2024-06-17 00:00:00') ],
    (object)["strAF" => "2025/2026", "inizioAF" => new DateTime('2025-06-17 00:00:00') ],
    (object)["strAF" => "2026/2027", "inizioAF" => new DateTime('2026-06-17 00:00:00') ],
    (object)["strAF" => "2027/2028", "inizioAF" => new DateTime('2027-06-17 00:00:00') ],
    (object)["strAF" => "2028/2029", "inizioAF" => new DateTime('2028-06-17 00:00:00') ],
    (object)["strAF" => "2029/2030", "inizioAF" => new DateTime('2029-06-17 00:00:00') ]
  ];

  foreach($anniFormativi as $af){
    if($date > $af->inizioAF){
      $string = $af->strAF;
    }
  } 

  // setlocale(LC_ALL,NULL);
  return $string;
}


// --------------------------------------------------------
// map a value to a 0-100 scale
//
function mapTo100($value, $maxValue){
  if($maxValue === 0){ return 0; }
  return $value * 100 / $maxValue;
}


// --------------------------------------------------------
// interactive print_r
// via http://php.net/manual/it/function.print-r.php
//
function print_r_tree($data)
{
    // capture the output of print_r
    $out = print_r($data, true);

    // replace something like '[element] => <newline> (' with <a href="javascript:toggleDisplay('...');">...</a><div id="..." style="display: none;">
    $out = preg_replace('/([ \t]*)(\[[^\]]+\][ \t]*\=\>[ \t]*[a-z0-9 \t_]+)\n[ \t]*\(/iUe',"'\\1<a href=\"javascript:toggleDisplay(\''.(\$id = substr(md5(rand().'\\0'), 0, 7)).'\');\">\\2</a><div id=\"'.\$id.'\" style=\"display: none;\">'", $out);

    // replace ')' on its own on a new line (surrounded by whitespace is ok) with '</div>
    $out = preg_replace('/^\s*\)\s*$/m', '</div>', $out);

    // print the javascript function toggleDisplay() and then the transformed output
    echo '<pre><script language="Javascript">function toggleDisplay(id) { document.getElementById(id).style.display = (document.getElementById(id).style.display == "block") ? "none" : "block"; }</script>'."\n$out"."</pre>";
}


// --------------------------------------------------------
// >> gets news page
// << returns date string
//
function newsDate($news){
  setlocale(LC_TIME, 'ita', 'it_IT');
  $date = DateTime::createFromFormat('Y-m-d', $news->dataNews()->value());
  if(!$date){
    go(page('error')->url() ."/err:3378873643");
  }
  $d = $date->getTimestamp();
  $formatted = strftime("%e %B %Y", $d);
  setlocale(LC_ALL,NULL);
  return $formatted;
}



// --------------------------------------------------------
// safely returns array value if key exists
function safelyRead($array, $key, $default){
  if(array_key_exists($key, $array)){
    return $array[$key];
  } else {
    return $default;
  }
}


// --------------------------------------------------------
// >> gets posted data with form field names
// << returns data with fields named 
//    for Kirby page segreteria-ordine
//
function ordineDataFromPostedForm(){

  // --- prepare page data
  
  $pageData = array(

    // data from input form
    'form_dataOraOrdine'          => $_POST["dataOraOrdine"],
    'form_cognome'                => ucwords($_POST["cognome"]),
    'form_nome'                   => ucwords($_POST["nome"]),
    'form_dataNascita'            => $_POST["dataNascita"],
    'form_cittadinanza'           => $_POST["cittadinanza"],
    'form_luogoNascita'           => ucwords($_POST["luogoNascita"]),
    'form_provinciaNascita'       => $_POST["provinciaNascita"],
    'form_codiceFiscale'          => strtoupper($_POST["codiceFiscale"]),
    'form_telefono'               => $_POST["telefono"],
    'form_email'                  => $_POST["email"],
    'form_residenzaIndirizzo'     => $_POST["residenzaIndirizzo"],
    'form_residenzaCivico'        => $_POST["residenzaCivico"],
    'form_residenzaCap'           => $_POST["residenzaCap"],
    'form_residenzaLuogo'         => ucwords($_POST["residenzaLuogo"]),
    'form_residenzaProvincia'     => $_POST["residenzaProvincia"],
    'form_aziendaRagioneSociale'  => safelyRead($_POST, "aziendaRagioneSociale", null),
    'form_aziendaIndirizzo'       => safelyRead($_POST, "aziendaIndirizzo", null),
    'form_aziendaCivico'          => safelyRead($_POST, "aziendaCivico", null),
    'form_aziendaLuogo'           => ucwords(safelyRead($_POST, "aziendaLuogo", null)),
    'form_aziendaProvincia'       => safelyRead($_POST, "aziendaProvincia", null),
    'form_aziendaCap'             => safelyRead($_POST, "aziendaCap", null),
    'form_aziendaCodiceFiscale'   => safelyRead($_POST, "aziendaCodiceFiscale", null),
    'form_aziendaPartitaIva'      => safelyRead($_POST, "aziendaPartitaIva", null),
    'form_checkbox1'              => (array_key_exists("checkbox1", $_POST) && $_POST["checkbox1"] === "check1") ? true : false,
    'form_checkbox2'              => (array_key_exists("checkbox2", $_POST) && $_POST["checkbox2"] === "check2") ? true : false,
    'form_checkbox3'              => (array_key_exists("checkbox3", $_POST) && $_POST["checkbox3"] === "check3") ? true : false,

    // other common data
    'pagamentoMetodo'             => "",
    'note'                        => "",
    'pagamentoRiferimento'        => "",
    'pagamentoOk'                 => 0

  );
  return $pageData;
}


// --------------------------------------------------------
// >> gets $ordine
// << returns formatted as cached data
//
function formDataFromPage($ordine){

  $fatturazioneAziendale = (
    trim($ordine->form_aziendaRagioneSociale()->value() != "")
    || trim($ordine->form_aziendaCodiceFiscale()->value() != "")
    || trim($ordine->form_aziendaPartitaIva()->value() != "")
    || trim($ordine->form_aziendaIndirizzo()->value() != "")
    || trim($ordine->form_aziendaCivico()->value() != "")
    || trim($ordine->form_aziendaLuogo()->value() != "")
    || trim($ordine->form_aziendaProvincia()->value() != "")
    || trim($ordine->form_aziendaCap()->value() != "")
  );

  $formData = array(
    'form_cognome'                => $ordine->form_cognome()->value(),
    'form_nome'                   => $ordine->form_nome()->value(),
    'form_dataNascita'            => $ordine->form_dataNascita()->value(),
    'form_cittadinanza'           => $ordine->form_cittadinanza()->value(),
    'form_luogoNascita'           => $ordine->form_luogoNascita()->value(),
    'form_provinciaNascita'       => $ordine->form_provinciaNascita()->value(),
    'form_codiceFiscale'          => $ordine->form_codiceFiscale()->value(),
    'form_telefono'               => $ordine->form_telefono()->value(),
    'form_email'                  => $ordine->form_email()->value(),
    'form_residenzaIndirizzo'     => $ordine->form_residenzaIndirizzo()->value(),
    'form_residenzaCivico'        => $ordine->form_residenzaCivico()->value(),
    'form_residenzaLuogo'         => $ordine->form_residenzaLuogo()->value(),
    'form_residenzaProvincia'     => $ordine->form_residenzaProvincia()->value(),
    'form_residenzaCap'           => $ordine->form_residenzaCap()->value(),
    'form_aziendaRagioneSociale'  => $ordine->form_aziendaRagioneSociale()->value(),
    'form_aziendaCodiceFiscale'   => $ordine->form_aziendaCodiceFiscale()->value(),
    'form_aziendaPartitaIva'      => $ordine->form_aziendaPartitaIva()->value(),
    'form_aziendaIndirizzo'       => $ordine->form_aziendaIndirizzo()->value(),
    'form_aziendaCivico'          => $ordine->form_aziendaCivico()->value(),
    'form_aziendaLuogo'           => $ordine->form_aziendaLuogo()->value(),
    'form_aziendaProvincia'       => $ordine->form_aziendaProvincia()->value(),
    'form_aziendaCap'             => $ordine->form_aziendaCap()->value(),
    'form_checkbox1'              => $ordine->form_checkbox1()->value(),
    'form_checkbox2'              => $ordine->form_checkbox2()->value(),
    'form_checkbox3'              => $ordine->form_checkbox3()->value(),
    'importoTotale'               => $ordine->importoTotale()->value(),

    'fatturazioneAziendale'       => $fatturazioneAziendale,
    'pagamentoMetodo'             => $ordine->pagamentoMetodo()->value(),
    'note'                        => $ordine->note()->value(),
    'pagamentoRiferimento'        => $ordine->pagamentoRiferimento()->value(),
    'emailVerificata'             => $ordine->emailVerificata()->value(),
    'pagamentoOk'                 => $ordine->pagamentoOk()->value()
  ); 
  return $formData;
}

// --------------------------------------------------------
// Given the order returns HTML string for order status bedge
//
function badge($ordine){
  $controlString = "NEW";
  if($ordine->emailVerificata()->value() == "1"){
    if($ordine->pagamentoOk()->value() == "1"){
      $controlString = "PAYED";
    } else {
      $controlString = "OPEN";
    }
  }
  $badges = array(
    "NEW" => "<span class=\"badge badge-new\">NEW</span>",
    "OPEN" => "<span class=\"badge badge-open\">OPEN</span>",
    "PAYED" => "<span class=\"badge badge-payed\">PAYED</span>"
  );
  // $state = explode("-", $ordine->title()->value())[0];
  return $badges[$controlString];
}


// --------------------------------------------------------
// Returns true if logged user has admin role
//
function userIsAdmin($site){
  $userIsAdmin = ($site->user() && $site->user()->hasRole('admin'));
  return $userIsAdmin;
}


// --------------------------------------------------------
// Checks if article is in cart
//
function isInCart($articleeeeId){
  $cartArticleeeeIdsString = null;
  $cartArticles = [];
  if(cookie::exists(c::get("cartCookieKey"))){
    $cartArticleeeeIdsString = cookie::get(c::get("cartCookieKey"));
  } else {
    return false;
  }
  if($cartArticleeeeIdsString){
    $cartArticles = explode(",", $cartArticleeeeIdsString);
  }
  $found = false;
  foreach ($cartArticles as $a) {
    if($a === $articleeeeId){
      return true;
    }
  }
  return false;
}


// --------------------------------------------------------
// calculate discount amount
//
function sconto($price, $amt, $type){
  $sconto = 0;
  if($type === "euro"){
    $sconto = (float)$amt;
  } else if($type === "percent") {
    $sconto = (float)$price * (float)$amt / 100;
  }
  return $sconto;
}


// --------------------------------------------------------
// Get cart items structure in an array of objects
//
// --- V0 --- Sconti su 2° e 3° turno dello stesso corso
/*
function cartObjectFromArticleeeeIdsString($cartArticleeeeIdsString){
  $cartArticles = [];
  if($cartArticleeeeIdsString){
    $cartArticles = explode(",", $cartArticleeeeIdsString);
  }

  $sconto2amt = page('segreteria-settaggio-sconti')->scontoSecondoCorsoAmt()->value();
  $sconto2type = page('segreteria-settaggio-sconti')->scontoSecondoCorsoType()->value();
  $sconto3amt = page('segreteria-settaggio-sconti')->scontoTerzoCorsoAmt()->value();
  $sconto3type = page('segreteria-settaggio-sconti')->scontoTerzoCorsoType()->value();

  $cartArray = [];
  foreach ($cartArticles as $articleeeeId){
    $corsoId = explode("~", $articleeeeId)[0];
    $turnoUid = explode("~", $articleeeeId)[1];
    $corso = page("corsi")->children()->findBy("corsoId", $corsoId);
    if(!$corso){
      go(page('error')->url() ."/err:3316516156");
    }
    $turno = $corso->children()->findBy("uid", $turnoUid);
    if(!$turno){
      go(page('error')->url() ."/err:3316516157");
    }
    $article = new stdClass();
    $article->articleeeeId = $articleeeeId;
    $article->corso = new stdClass();
    $article->corso->corsoId = $corsoId;
    $article->corso->title = $corso->title()->value();
    $article->turno = new stdClass();
    $article->turno->uid = $turnoUid;
    $article->turno->nome = $turno->title()->value();
    $article->turno->costo = (float)$turno->itemCost()->value();
    $article->turno->scontabile = ($turno->scontabile() == '1');
    $article->turno->dataInizio = $turno->dataInizio()->value();
    $article->turno->dataFine = $turno->dataFine()->value();
    $article->costoFinale = (float)$turno->itemCost()->value();

    // sconto
    if($turno->scontabile() == '1'){
      $countCourseOccurrencies = 0;
      foreach ($cartArray as $e) {
        if(
          ($e->corso->corsoId == $corsoId) && 
          ($e->turno->scontabile)
        ){
          $countCourseOccurrencies++;
        }
      }
      if($countCourseOccurrencies == 1){
        $amt = sconto($turno->itemCost()->value(), $sconto2amt, $sconto2type);
        $article->sconto = new stdClass();
        $article->sconto->description = "$sconto2amt ". r($sconto2type=="percent", "%", "€") ." su secondo turno acquistato";
        $article->sconto->amt = $amt;
        $article->costoFinale -= $amt;
      } else if($countCourseOccurrencies >= 2) {
        $amt = sconto($turno->itemCost()->value(), $sconto3amt, $sconto3type);
        $article->sconto = new stdClass();
        $article->sconto->description = "$sconto3amt ". r($sconto3type=="percent", "%", "€") ." su terzo turno acquistato";
        $article->sconto->amt = $amt;
        $article->costoFinale -= $amt;
      }
    }
    // end sconto

    $cartArray[] = $article;
  }

  return $cartArray;
}
*/
//
// --- V1 --- Sconto su qualsiasi turno scontabile, non necessariamente di corso uguale
//            Lo sconto varia se gli articoli scontabili sono 2 (es. 5%) o 3+ (es. 10%)
//
function cartObjectFromArticleeeeIdsString($cartArticleeeeIdsString){
  $cartArticles = [];
  if($cartArticleeeeIdsString){
    $cartArticles = explode(",", $cartArticleeeeIdsString);
  }

  $sconto2amt = page('segreteria-settaggio-sconti')->scontoSecondoCorsoAmt()->value();
  $sconto2type = page('segreteria-settaggio-sconti')->scontoSecondoCorsoType()->value();
  $sconto3amt = page('segreteria-settaggio-sconti')->scontoTerzoCorsoAmt()->value();
  $sconto3type = page('segreteria-settaggio-sconti')->scontoTerzoCorsoType()->value();

  // --- Prima componi carrello senza sconti

  $cartArray = [];
  $turniScontabili = 0;
  foreach ($cartArticles as $articleeeeId){
    $corsoId = explode("~", $articleeeeId)[0];
    $turnoUid = explode("~", $articleeeeId)[1];
    $corso = page("corsi")->children()->findBy("corsoId", $corsoId);
    if(!$corso){
      go(page('error')->url() ."/err:3316516156");
    }
    $turno = $corso->children()->findBy("uid", $turnoUid);
    if(!$turno){
      go(page('error')->url() ."/err:3316516157");
    }
    $article = new stdClass();
    $article->articleeeeId = $articleeeeId;
    $article->corso = new stdClass();
    $article->corso->corsoId = $corsoId;
    $article->corso->title = $corso->title()->value();
    $article->turno = new stdClass();
    $article->turno->uid = $turnoUid;
    $article->turno->nome = $turno->title()->value();
    $article->turno->costo = (float)$turno->itemCost()->value();
    $article->turno->scontabile = ($turno->scontabile() == '1');
    $article->turno->dataInizio = $turno->dataInizio()->value();
    $article->turno->dataFine = $turno->dataFine()->value();
    $article->costoFinale = (float)$turno->itemCost()->value();

    if($turno->scontabile() == '1'){
      $turniScontabili++;
    }

    $cartArray[] = $article;
  }

  // --- Poi calcola eventuali sconti

  if ($turniScontabili >= 2) {
    foreach ($cartArray as $article) {
      $corso = page("corsi")->children()->findBy("corsoId", $article->corso->corsoId);
      if(!$corso){
        go(page('error')->url() ."/err:3316517156");
      }
      $turno = $corso->children()->findBy("uid", $article->turno->uid);
      if(!$turno){
        go(page('error')->url() ."/err:3316517157");
      }
      if ($turno->scontabile() == '1') {
        if ($turniScontabili == 2) {
          $amt = sconto($turno->itemCost()->value(), $sconto2amt, $sconto2type);
          $article->sconto = new stdClass();
          $article->sconto->description = "Prezzo scontato (-$sconto2amt". r($sconto2type=="percent", "%", "€") .") per l'acquisto di due articoli scontabili";
          $article->sconto->amt = $amt;
          $article->costoFinale -= $amt;
        } else if ($turniScontabili >= 3) {
          $amt = sconto($turno->itemCost()->value(), $sconto3amt, $sconto3type);
          $article->sconto = new stdClass();
          $article->sconto->description = "Prezzo scontato (-$sconto3amt". r($sconto3type=="percent", "%", "€") .") per l'acquisto di tre o più articoli scontabili";
          $article->sconto->amt = $amt;
          $article->costoFinale -= $amt;
        }
      }
    }
  }
  // end sconto

  return $cartArray;
}

// --------------------------------------------------------
// Returns a string starting with "," and containing 
//  the formatted dates of the turno if both dates have a value
//
function turnoDates($turno, $separator=","){
  setlocale(LC_TIME, 'ita', 'it_IT');

  // if($turno->dataInizio()->isEmpty() || $turno->dataFine()->isEmpty()){ 
  // ^^^ gave problems from 'recap-ordine-print'
  if($turno->dataInizio() == '' || $turno->dataFine() == ''){
    return "";
  }
  $dateStart = DateTime::createFromFormat('Y-m-d', $turno->dataInizio()->value());
  $dateEnd = DateTime::createFromFormat('Y-m-d', $turno->dataFine()->value());
  
  // if($turno->showOnlyMonthInizio()->exists() && $turno->showOnlyMonthInizio()->isTrue()){
  // ^^^ gave problems from 'recap-ordine-print'
  if($turno->showOnlyMonthInizio() == '1'){
    $formatStart = "%B";
  } else {
    $formatStart = "%e %b";
  }
  // if($turno->showOnlyMonthFine()->exists() && $turno->showOnlyMonthFine()->isTrue()){
  // ^^^ gave problems from 'recap-ordine-print'
  if($turno->showOnlyMonthFine() == '1'){
    $formatEnd = "%B %Y";
  } else {
    $formatEnd = "%e %b %Y";
  }
  $formattedStart = strftime($formatStart, $dateStart->getTimestamp());
  $formattedEnd = strftime($formatEnd, $dateEnd->getTimestamp());
  setlocale(LC_ALL,NULL);
  return "$separator $formattedStart &mdash; $formattedEnd";
}


// --------------------------------------------------------
// Searches or adds to the mailchimp list "Interessati"
// $method - GET or POST
// $email - email address to read (GET) or add (POST)
//
function mailchimpListApi($method, $email, $status){
  $listId = c::get("mailchimpListId");
  $ch = curl_init();

  if($method === "GET"){

    $emailHash = md5(strtolower($email));
    curl_setopt($ch, CURLOPT_URL, "https://us1.api.mailchimp.com/3.0/lists/$listId/members/$emailHash");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERPWD, "anyuser:" . c::get("mailchimpApikey"));
    $headers = array();
    $headers[] = "Content-Type: application/x-www-form-urlencoded";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
      echo 'Error:' . curl_error($ch);
    }
    a::show($result);
    curl_close ($ch);

  } else {

    // $status = "pending";
    // $status = "subscribed";
    $jsonStr = '{ "email_address": "'. $email .'", "status": "'. $status .'" }';
    $jsonObj = json_decode($jsonStr, true); 
    curl_setopt($ch, CURLOPT_URL, "https://us1.api.mailchimp.com/3.0/lists/$listId/members/");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonStr);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_USERPWD, "anyuser:" . c::get("mailchimpApikey"));
    $headers = array();
    $headers[] = "Content-Type: application/x-www-form-urlencoded";
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    if (curl_errno($ch)) {
      echo 'Error:' . curl_error($ch);
    }
    a::show($result);
    curl_close ($ch);

  }
}


?>