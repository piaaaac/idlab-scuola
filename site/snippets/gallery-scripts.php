<?php
/**
 * @param $itemsForSaving - only when preceding snippet "gallery-gallery-item" that has savable photos
 */

// only w/ gallery-gallery-item
if(isset($itemsForSaving)){
  $i = 0;
  $imgUrls = [];
  foreach($itemsForSaving as $item){
    $imgUrls["gi-default-$i"] = $item->thumb(["height" => 900])->url();
    $i++;
  }
}
?>

<script>
  var galleryId, currentPic, selector, picsNum, imgUrls;

  function initGallery (id) {
    galleryId = id;
    currentPic = 0;
    selector = "[id^='gi-"+ galleryId +"']";
    picsNum = $(selector).length;
    imgUrls = <?= isset($itemsForSaving) ? json_encode($imgUrls) : "{}" ?>;
  }

  function openGallery (n) {
    $("body").addClass("no-scroll");
    $("#gallery-"+ galleryId +" .gallery-overlay").addClass("show");
    showGalleryPic(n);
  }
  function closeGallery () {
    $("body").removeClass("no-scroll");
    $("#gallery-"+ galleryId +" .gallery-overlay").removeClass("show");
  }
  function showGalleryPic (n) {
    if(typeof n === "number"){
      $(".gallery-item").removeClass("show");
      $("#gi-" + galleryId + "-"+ (+n)).addClass("show");
    }
    currentPic = +n;
  }
  function showNextPic () {
    currentPic++;
    if((currentPic) >= picsNum){
      currentPic = 0;
    }
    showGalleryPic(currentPic);
  }
  function showPrevPic () {
    currentPic--;
    if(currentPic < 0){
      currentPic = picsNum - 1;
    }
    showGalleryPic(currentPic);
  }

  // only w/ gallery-gallery-item
  function dlCurrent () {
    var currentPicUrl = imgUrls["gi-default-" + (+currentPic)];
    console.log("currentPicUrl", currentPicUrl);
    window.open(currentPicUrl, '_blank');
  }

  $(document).ready(function(e){
    initGallery("default");
  });
  
  // --- keyboard handling

  $(document).keydown(function(e){
    // var k = String.fromCharCode(e.which);
    var code = e.which;
    console.log(code);
    if(code == 27) { closeGallery(); }
    if(code == 39) { showNextPic(); }
    if(code == 37)  { showPrevPic(); }
  });

</script>