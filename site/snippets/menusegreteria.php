<?php 
/* --------------------------------------
PARAMETER
$selected
  - contatti-studenti
  - iscrizioni-per-corso
  - conferma-pagamenti
-------------------------------------- */

// $selected

?>

    <div class="container-fluid super-cont">
      <!--       
      <div class="row mb-4">
        <div class="col">
          <a href="<?= $site->url() ."/panel/pages/segreteria-ordini/edit" ?>"><< Torna al pannello</a>
        </div>
      </div>
       -->
      <div class="row mb-4">
        <div class="col">
          <ul class="nav nav-tabs">

            <li class="nav-item">
              <a 
                class="nav-link <?= r($selected == "conferma-pagamenti", "active", "") ?>" 
                href="<?= page("segreteria-conferma-pagamenti")->url() ?>">
                Conferma pagamenti
              </a>
            </li>

            <li class="nav-item">
              <a 
                class="nav-link <?= r($selected == "iscrizioni-per-corso", "active", "") ?>" 
                href="<?= page("segreteria-iscrizioni-per-corso")->url() ?>">
                Lista iscrizioni per corso
              </a>
            </li>

            <li class="nav-item">
              <a 
                class="nav-link <?= r($selected == "contatti-studenti", "active", "") ?>" 
                href="<?= page("segreteria-contatti-studenti")->url() ?>">
                Contatti studenti
              </a>
            </li>

          </ul>
        </div>
      </div>
    </div>
