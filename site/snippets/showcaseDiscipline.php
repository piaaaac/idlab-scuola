<?php
$discipline = page('discipline')->children()->visible();
$discipline = $discipline->not('corsi-speciali');

// Old
// $corsiSpeciali = page('corsi')->children()->visible()
//   ->filterBy("disciplinaUid", "corsi-speciali")
//   ->filterBy("mostraInHP", "1");
// $items = $discipline->merge($corsiSpeciali);
// $items = $items->sortBy("title");

// New
$corsiHP = page('corsi')->children()->visible()
  ->filterBy("mostraInHP", "1");
$items = $discipline->merge($corsiHP);
$items = $items->sortBy("title");
$colors = [
  "capolettera" => [
    "normal"    => "font-color-blue",
    "special"   => "font-color-gold"
  ],
  "text" => [
    "normal"    => "font-color-black",
    "special"   => "font-color-gold"
  ],
  "button" => [
    "normal"    => "font-color-blue",
    "special"   => "font-color-gold"
  ],
];

?>

<div class="container-fluid super-cont" id="showcase-discipline">
  <div class="row">
<!-- 
    <div class="col-12">
      <hr class="thin mb-3 mt-2">
    </div>
 -->
    <?php $i = 1; ?>
    <?php foreach($items as $item): ?>

      <?php 
        $paddingTop = false; 
        $paddingLeft = ""; 
        if($i % 2 == 0){
          $paddingLeft = "discipline-left-padding-sm-only"; 
          $paddingTop = true; 
        }
      ?>
      
      <?php if($item->template() == "disciplina"): ?>
        
        <?php
        $imgUrl = "";
        if($item->imgDisciplina() && $image = $item->imgDisciplina()->toFile()){
          $imgUrl = $image->url();
        } else {
          $imgUrl = $site->url() ."/assets/images/fallback-img-disciplina.png";
        }
        ?>

        <div class="col-md-6 <?= $paddingLeft ?>">
          <?php if($paddingTop): ?><div class="d-none d-md-block my-5"><br /><br /><br /></div><?php endif ?>
          <div class="d-flex align-items-center disciplina-item" style="background-image: url(<?= $imgUrl ?>)">
            <div class="">
              <p class="font-supersuper-llll capolettera font-color-blue mb-2"><?= $item->capolettera()->value() ?></p>
              <div class="text"><?= $item->nome()->kirbytext() ?></div>
              <a class="btn btn-primary mt-2" href="<?= $item->url() ?>" role="button">TUTTI I CORSI &rarr;</a>
            </div>
          </div>
        </div>

      <?php elseif($item->template() == "corso"): ?>

        <?php


        // Old
        // $nome = (trim($item->nome()->kirbytext()) !== "")
        //   ? $item->nome()->kirbytext()
        //   : $item->title()->kirbytext();
        
        // New
        $nome = $item->nome()->isNotEmpty()
          ? $item->nome()->value()
          : $item->title()->value();
        $nome = "<p>". html::breaks($nome) ."</p>";
        $type = $item->disciplinaUid()->value() == "corsi-speciali" ? "special" : "normal";

        $imgUrl = "";
        if($item->imgCorso() && $image = $item->imgCorso()->toFile()){
          $imgUrl = $image->url();
        } else {
          $disciplina = page('discipline/'. $item->disciplinaUid()->value());
          if($item->imgDisciplina() && $image = $item->imgDisciplina()->toFile()){
            $imgUrl = $image->url();
          } else {
            $imgUrl = $site->url() ."/assets/images/fallback-img-corso-speciale.png";
          }
        }
        ?>

        <div class="col-md-6 <?= $paddingLeft ?>">
          <?php if($paddingTop): ?><div class="d-none d-md-block my-5"><br /><br /><br /></div><?php endif ?>
          <!-- <div class="d-flex align-items-center disciplina-item corso-speciale"  -->
          <div class="d-flex align-items-center disciplina-item corso" 
            style="background-image: url(<?= $imgUrl ?>)">
            <div class="">
              <p class="font-supersuper-llll capolettera <?= $colors["capolettera"][$type] ?> mb-2"><?= $item->capolettera()->value() ?></p>
              <div class="text font-super-ll ?>"><?= $nome ?></div>
              <!-- <div class="text font-super-ll <?= $colors["text"][$type] ?>"><?= $nome ?></div> -->
              <a class="btn btn-primary <?= $colors["button"][$type] ?> mt-2" href="<?= $item->url() ?>" role="button">ISCRIVITI &rarr;</a>
            </div>
          </div>
        </div>

        <!-- <p>Corso: <?= $item->title()->value() ?></p> -->
      <?php endif ?>
      <?php $i++; ?>

    <?php endforeach ?>

  </div>
</div>
