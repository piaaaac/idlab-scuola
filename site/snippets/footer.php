<?php
/* ----------------------------
<< 
  $hideFooter   -> bool

---------------------------- */
if(!isset($hideFooter)){ $hideFooter = false; }

$bg = page('footer-texts')->files()->shuffle()->first()->url();

?>
<?php if(!$hideFooter): ?>

  <footer style="
  background-image: url(<?= $bg ?>);
  background-position: center center;
  background-repeat: no-repeat;
  background-size: cover;
  ">

    <div class="header">

      <!-- custom form -->
      <!--  
      <div class="bar-newsletter">
        <div class="container-fluid super-cont">
          <div class="row">
            <div class="col">
              <form id="subscribe" action="<?= page('subscribe')->url()?>" method="post">
                <div class="h d-flex flex-wrap flex-md-nowrap align-items-center justify-content-between">
                  <span class="font-sans-l">Ricevi la newsletter di <span class="font-supersuper-l">Super</span>: </span>
                  <input class="email mx-2 mx-sm-3 flex-grow-1 font-sans-l" type="search" placeholder="la tua email" aria-label="Subscribe" name="q">
                  <button class="btn btn-primary btn-small font-color-gold hover-gold-dark" type="submit">INVIA &rarr;</button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
      -->

      <div class="bar-newsletter">
        <div class="container-fluid super-cont">
          <div class="row">
            <div class="col">

              <!-- Begin Mailchimp Signup Form -->
              <div id="mc_embed_signup" class="super-mc-embed-signup">
                <form action="https://scuolaarteapplicata.us1.list-manage.com/subscribe/post?u=bdcf187dfbfe65e8a135d819b&amp;id=d3ac6b5495" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                  <div id="mc_embed_signup_scroll" class="h d-block d-block d-md-flex align-items-center justify-content-between">
                    
                    <!--  
                    <div class="font-sans-l mt-4">Ricevi la newsletter di <span class="font-supersuper-l">Super</span>:&nbsp;</div>
                    -->
                    <div class="d-flex align-items-center">
                      <div class="font-sans-l d-inline-block d-sm-none mt-1">Newsletter&nbsp;di&nbsp;</div>
                      <div class="font-sans-l d-none d-sm-inline-block mt-1">Ricevi&nbsp;la&nbsp;newsletter&nbsp;di&nbsp;</div>
                      <img class="logo-h-60" src="<?= $site->logoTinyBlack()->toFile()->url() ?>" />
                      <div class="font-sans-l d-inline-block mt-1">:&nbsp;</div>
                    </div>
                    
                    <div id="field-group-super" class="mc-field-group">
                      <input type="email" value="" name="EMAIL" placeholder="la tua email" class="required email font-sans-l mt-2 mt-md-1 py-0" id="mce-EMAIL">
                    </div>
                    
                    <div class="clear"><input type="submit" value="INVIA &rarr;" name="subscribe" id="mc-embedded-subscribe" class="btn btn-primary btn-small font-color-gold hover-gold-dark mt-4 mt-md-0 mb-3 mb-md-0"></div>

                  </div>
                  <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                  </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                  <div style="position: absolute; left: -5000px;" aria-hidden="true">
                    <input type="text" name="b_bdcf187dfbfe65e8a135d819b_d3ac6b5495" tabindex="-1" value="">
                  </div>
                </form>
              </div>
              <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script>
              <script type='text/javascript'>
                (function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';}(jQuery));var $mcj = jQuery.noConflict(true);
              </script>
              <!--End mc_embed_signup-->

            </div>
          </div>
        </div>
      </div>

      <div class="bar-social">
        <div class="container-fluid super-cont">
          <div class="row">
            <div class="col">
              
              <!-- V1 
              <div class="h d-flex align-items-center flex-wrap">
                <div class="d-flex align-items-center frase-social">
                  <img class="mr-1 logo-h-60" src="<?= $site->logoTinyBlack()->toFile()->url() ?>" />
                  <div class="mt-1"><p><?= page('footer-texts')->fraseSopra()->value() ?>&nbsp;</p></div>
                </div>
                <div class="frase-social mt-1 py-2"><?= page('footer-texts')->fraseSopraB()->kirbytext() ?></div>
              </div>
              -->

              <!-- V2 -->
              <div class="h d-flex flex-wrap align-items-center">
                <img class="mr-1 d-inline-block logo-h-60" src="<?= $site->logoTinyBlack()->toFile()->url() ?>" />
                <p class="frase-social d-inline-block mt-3 mb-2 mt-md-2 mb-md-1"><?= page('footer-texts')->fraseSopra()->value() ?>&nbsp;</p>
                <div class="frase-social d-inline-block mt-3 mb-2 mt-md-2 mb-md-1"><?= page('footer-texts')->fraseSopraB()->kirbytext() ?></div>
              </div>

            </div>
          </div>
        </div>
      </div>

    </div>


    <div class="main">
      <div class="container-fluid super-cont">
        <div class="row">

          <div class="column1 col-12 col-lg-4">
            <div class="row">
              <div class="column1-a col-12 col-lg-6">
  
                <!--  
                <p class="font-super-ll d-block d-lg-none mb-5">
                  Scuola <span class="font-supersuper-ll">Super</span>iore
                  <br />d’Arte Applicata
                </p>
                <p class="font-super-l d-none d-lg-block mb-5">
                  Scuola
                  <br /><span class="font-supersuper-l">Super</span>iore
                  <br />d’Arte
                  <br />Applicata
                </p>
                -->
                
                <!-- xs > md -->
                <div class="d-block d-lg-none mb-5"> 
                  <img class="logo-max-w" src="<?= $site->logoTwoLinesWhite()->toFile()->url() ?>" />
                </div>
                <!-- lg > xl -->
                <div class="d-none d-lg-block mb-5">
                  <img class="logo-max-w" src="<?= $site->logoFourLinesWhite()->toFile()->url() ?>" />
                </div>

              </div>
              <div class="column1-b col-12 col-lg-6">
  
                <div class="dynamic-text smll">
                  <?= page('footer-texts')->colonna1()->kirbytext() ?>
                </div>
                
              </div>
            </div>
          </div>

          <div class="col-12 col-sm-8 col-lg-4">
            <div class="dynamic-text">
              <?= page('footer-texts')->colonna2()->kirbytext() ?>

              <!-- Amministrazione trasparente -->
              <?php if(page("amministrazione-trasparente")->isVisible()): ?>
                <a class="font-sans-s font-color-white hover-gold d-block my-3" href="<?= page("amministrazione-trasparente")->url() ?>">
                  &rarr; <?=page("amministrazione-trasparente")->title()->value()?>
                </a>
              <?php endif ?>

              <!-- Gallery -->
              <?php if(page("gallery")->isVisible()): ?>
                <a class="font-sans-s font-color-white hover-gold d-block my-3" href="<?= page("gallery")->url() ?>">
                  &rarr; <?=page("gallery")->title()->value()?>
                </a>
              <?php endif ?>

            </div>
          </div>
        
          <div class="col-12 col-sm-8 col-lg-4">
            <div class="dynamic-text">
              <?= page('footer-texts')->colonna3()->kirbytext() ?>
              <br/>&nbsp;
            </div>

            <div id="partnerships" class="font-super-ss">
              <?= $site->partnerships()->kirbytext() ?>
            </div>

          </div>
          
        </div>
      </div>
    </div>

  </footer>

<?php endif ?>

<?php /*snippet('google-analytics')*/ ?>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131142587-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131142587-1');
</script>


<script>

  function handleMenuOnScroll(scrollPosition){
    if(scrollPosition >= 60){
      if(state.scrolledPastMenu === false){
        $(".super-menu-full.big-logo").addClass("show");
        // if($(".super-menu-full").hasClass("big-logo")){
          $("#search-bar").removeClass("menu-is-closed");
        // }
        state.scrolledPastMenu = true;
      }
    } else {
      if(state.scrolledPastMenu === true){
        $(".super-menu-full.big-logo").removeClass("show");
        if($(".super-menu-full").hasClass("big-logo")){
          $("#search-bar").addClass("menu-is-closed");
        }
        state.scrolledPastMenu = false;
      }
    }
  }

  function handleCrowdfundingButton (scroll) {
    
    var pageUid = "<?= $page->uid() ?>";

    if (pageUid !== "homepage") { return; }

    var mql = window.matchMedia('(max-width: 768px)');
    if (mql.matches) {
      var trigger = $("#crowdfunding-button-trigger").offset().top;
      if (scroll > trigger && $("#crowdfunding-btn-mobile").hasClass("show")) {
        $("#crowdfunding-btn-mobile").removeClass("show");
      }
      if (scroll <= trigger && !$("#crowdfunding-btn-mobile").hasClass("show")) {
        $("#crowdfunding-btn-mobile").addClass("show");
      }
    }
  }

    
  $(document).ready(function(){
    // console.log("Ready");

    // INITIALIZE
    ////////////////////////////////////
    
    if($(".super-menu-full").hasClass("big-logo")){
      $("#search-bar").addClass("menu-is-closed");
    }
  
    var scroll = $(window).scrollTop();
    handleMenuOnScroll(scroll);
    handleCrowdfundingButton(scroll);
    // BIND
    ////////////////////////////////////
    
    // $(window).resize(function(){
    //   handleResizeStart();
    //   window.clearTimeout(resizeTimeout);
    //   resizeTimeout = window.setTimeout(function(){
    //     handleResizeEnd();
    //     console.log("Resized");
    //   }, resizeTimeoutMs)
    // });

    $(window).scroll(function (event) {
      var scroll = $(window).scrollTop();
      handleMenuOnScroll(scroll);
      handleCrowdfundingButton(scroll);
    });

    $(".link-hp-super").click(function() {
      document.location = "<?= (page("homepage")->imgLink()->value()) ?>";
    });
    $(".link-hp-lapis").click(function() {
      document.location = "<?= (page("homepage")->imgLinkR()->value()) ?>";
    });
  });
  
</script>

<?php snippet('cart') ?>

<?php snippet('menu-xs') ?>

</body>
</html>