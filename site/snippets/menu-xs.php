<?php 
$textColor = "";
$menuPages = array(
  page('corsi'),
  page('lapis'),
  page('la-scuola'),
  page('crowdfunding'),
  page('info')
);


// retreive cart count
$cartArticleeeeIdsString = a::get($_COOKIE, c::get("cartCookieKey"));
$cartArticles = [];
if($cartArticleeeeIdsString){
  $cartArticles = explode(",", $cartArticleeeeIdsString);
}
if(!$cartArticles || count($cartArticles)==0){
  $showCartNum = false;
  $cartNumber = null;
} else {
  $showCartNum = false;
  $cartNumber = count($cartArticles);
}

?>

<div id="menu-xs" class="d-block d-md-none">

  <div class="d-flex justify-content-between mb-4">

    <div class="pt-2">
      <a class="menu-item <?= $textColor ?>" href="javascript:toggleCartPanel(true)"><i class="fas fa-shopping-cart"></i></a>
      <?php if($showCartNum): ?>
        <span class="cart-number" class="font-sans-sss"><?= $cartNumber ?></span>
      <?php endif?>
    </div>
  
    <form id="search" class="<?= $textColor ?>" action="<?= page('search')->url()?>" method="get">
      <div class="d-flex">
        <button class="btn btn-outline-success mr-2" type="submit"><i class="fas fa-search"></i></button>
        <input class="form-control" type="search" placeholder="cerca" aria-label="Search" name="q">
      </div>
    </form>

  </div>

  <div>
    <?php foreach($menuPages as $i => $item): ?>
      <a class="menu-xs-item bordered-list <?= $textColor ?><?= r($item->isOpen(), ' active') ?>" href="<?= $item->url() ?>">
        <?= $item->title()->html() ?>
      </a>
    <?php endforeach ?>  
  </div>

</div>