<?php
/* ----------------------------
<< 
  $hideMenu     -> bool
  $fasciaNews   -> kirby field

---------------------------- */

// a::show($site->maintenance());
// exit();

if($site->maintenance()->value() === "true"){
  $redirectMaintenance = true;
  if($user = $site->user()){
    if($user->hasRole('admin') || $user->hasRole('maintenanceNormalUser')){
      $redirectMaintenance = false;
    } 
  }
  if($redirectMaintenance){
    go("maintenance");
    // header("Location: ". $site->url() ."/index-maintenance.html");
  }
}

if(!isset($hideMenu)){ $hideMenu = false; }

// IF USER IS LOGGED AND HAS ADMIN RIGHTS, REDIRECT TO IFRAME
$jsRedirect = "false";
$redirectUrl = page('admin')->url() ."?url=". urlencode(url::current());
if($user = $site->user()){
  if($user->hasRole('admin')){
    $jsRedirect = "true";
  } 
} 

$loadFormScripts = (preg_match("/^iscrizione/", $page->template()))
  ? true : false;

?>


<?php include_once(kirby()->roots()->snippets() .'/commonfunctions.php'); ?>
<!doctype html>
<html lang="<?= site()->language() ? site()->language()->code() : 'en' ?>">
<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
  <title><?= $site->title()->html() ?> | <?= $page->title()->html() ?></title>
  <meta name="description" content="<?= $site->description()->html() ?>">

  <!-- Facebook Pixel Code (New 20200123) -->
  <script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window,document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
   fbq('init', '973271886392917'); 
  fbq('track', 'PageView');
  </script>
  <noscript>
   <img height="1" width="1" 
  src="https://www.facebook.com/tr?id=973271886392917&ev=PageView
  &noscript=1"/>
  </noscript>
  <!-- End Facebook Pixel Code -->

  <?php 
  if($page->template() == "disciplina" || $page->template() == "corso") {
    
    if($page->template() == "disciplina") {
      $title = $page->snTitle() && $page->snTitle()->value() ? $page->snTitle()->value() : $page->title()->value();
      $desc = $page->snDesc() ? $page->snDesc()->value() : "";
      if($page->snImgFile() && $image = $page->snImgFile()->toFile()){
        $imgUrl = $image->url();
      } else {
        $imgUrl = $site->url() ."/assets/images/fallback-sn.png";
      }
    }

    if($page->template() == "corso") {
      $title = $page->snTitle() && $page->snTitle()->value() ? $page->snTitle()->value() : $page->title()->value();
      $desc = $page->snDesc() ? $page->snDesc()->value() : "";
      if($page->snImgFile() && $image = $page->snImgFile()->toFile()){
        $imgUrl = $image->url();
      } else {
        $disciplina = page("discipline/" . $page->disciplinaUid()->value());
        if($disciplina->snImgFile() && $image = $disciplina->snImgFile()->toFile()){
          $imgUrl = $image->url();
        } else {
          $imgUrl = $site->url() ."/assets/images/fallback-sn.png";
        }
      }
    }

    snippet('sn-meta-tags', [
      "title"       => $title,
      "snImgFile"   => $imgUrl,
      "currentUrl"  => thisUrl(),
      "desc"        => $desc
    ]);
  }
  ?>

  <?php 
  if(c::get('dev_offline')){
    snippet('load-scripts-offline', ["loadFormScripts" => $loadFormScripts]);
  } else {
    snippet('load-scripts', ["loadFormScripts" => $loadFormScripts]);
  }
  ?>

  <?php snippet('favicon') ?>

  <script>

    $(document).ready(function(){
      var ifrm = window.frameElement; // reference to iframe element container
      var parentIsAdminPage = (ifrm && ifrm.id === "ifrm");
      if(<?= $jsRedirect ?> && !parentIsAdminPage){
        window.location.replace("<?= $redirectUrl ?>");
      }
      <?php if(isset($fasciaNews) && $fasciaNews): ?>
        window.setTimeout(function(){
          startTransitionFasciaNews();
        }, 100);
      <?php endif ?>
    });

// try to create an error
// var error = empty.name;

    var state = {
      menuXsIsOpen:       false,
      scrolledPastMenu:   false,
      searchPlaceholder:  0,
      searchPlaceholderInterval: null,
    }

    // e.g. if(!mediaQueries.lg.matches){ … }
    var mediaQueries = {
      sm: window.matchMedia( "(min-width: 576px)" ),
      md: window.matchMedia( "(min-width: 768px)" ),
      lg: window.matchMedia( "(min-width: 992px)" ),
      xl: window.matchMedia( "(min-width: 1200px)" ),
    }

    function scrollToId(sId){
      var scrollMargin = 30;
      var menuHeight = 60;
      if(mediaQueries.sm.matches){ menuHeight = 53; }
      if(mediaQueries.lg.matches){ menuHeight = 50; }
      var pixels = $("#" + sId).offset().top - menuHeight - scrollMargin;
      $([document.documentElement, document.body]).animate({
        scrollTop: pixels
      }, 500);
    }

    function toggleXsMenu(){
      if(state.menuXsIsOpen === false){
        state.menuXsIsOpen = true;
        if(state.scrolledPastMenu === false){
          $(".super-menu-full.big-logo").addClass("show");
        }
      } else {
        state.menuXsIsOpen = false;
        if(state.scrolledPastMenu === false){
          $(".super-menu-full.big-logo").removeClass("show");
        }
      }
      $(".hamburger").toggleClass("is-active", state.menuXsIsOpen);
      $("#menu-xs").toggleClass("open", state.menuXsIsOpen);
    }

    function scrollToTop(){
      $([document.documentElement, document.body]).animate({scrollTop: 0}, 500);
    }

    // for cart icon
    function toggleCartPanel(openIt){
      $("#cart-cover").toggleClass("show", openIt);
      $("#cart").toggleClass("open", openIt);
    }

    // for cart icon
    function toggleSearch(){
      $("#search-bar").toggleClass("open");
      if($("#search-bar").hasClass("open")){
        state.searchPlaceholderInterval = window.setInterval(function() {
          cyclePlaceholders();
        }, 1000);
        $("#search-big-input").focus();
        if($(".super-menu-full.big-logo").hasClass("show")){ 
          $("#search-bar").removeClass("menu-is-closed"); 
        }
      } else {
        clearInterval(state.searchPlaceholderInterval);
      }
    }

    function cyclePlaceholders(){
      var placeholders = ["illustrazione vettoriale", "gatto", "matita"];
      state.searchPlaceholder++;
      if(state.searchPlaceholder >= placeholders.length){ state.searchPlaceholder = 0; }
      $("#search-big-input").attr("placeholder", placeholders[state.searchPlaceholder]);
    }

    function handleFirstTab(e) {
      if (e.keyCode === 9) { // the "I am a keyboard user" key
        document.body.classList.add('user-is-tabbing');
        window.removeEventListener('keydown', handleFirstTab);
      }
    }

    function startTransitionFasciaNews(){
      var leftStartPosition = 300;
      var speedFactor = 12;

      var jqel = $("#fascia-news .content-wrapper").first();
      var w = function(){ return jqel.width(); }
      var ww = function(){ return $(window).width(); }
      
      var cyclesCount = -1;
      var el = jqel[0];

      var transitionFasciaCss = function(){
        console.log("transitionFasciaCss");
        console.log("w()", w());
        cyclesCount++;

        var leftFrom = (cyclesCount == 0) 
          ? leftStartPosition
          : ww();

        var leftTo = -w() - 20;

        var ms = Math.abs(leftFrom - leftTo) * speedFactor;

        el.style.transition = 'left 1ms linear';
        el.style.left = leftFrom + "px";
        void el.offsetWidth;
        window.setTimeout(function(){
          el.style.transition = 'left ' + ms + 'ms linear';
          el.style.left = leftTo + 'px';
        }, 100);
        window.setTimeout(function(){
          transitionFasciaCss()
        }, ms);
      }

      transitionFasciaCss();

      // window.setInterval(function(){
      //   el.style.marginLeft = $(window).width() + "px";
      //   transitionFasciaCss();
      // }, msN);


      // function transitionFasciaJQ(){
      //   console.log("transitionFasciaJQ");
      //   var currentMargin = el.style.marginLeft.split("px")[0];
      //   ms = ml - currentMargin * 15;
      //   jqel.animate({ "marginLeft": ml + "px" }, ms, "linear", function() {
      //     jqel.css("marginLeft", $(window).width() + "px");
      //     transitionFasciaJQ();
      //   });
      // }
      // jqel.css("marginLeft", "300px");
      // transitionFasciaJQ();


    }

  </script>

  <!-- <script src="<?= $site->url() ?>/assets/js/p5-bg.js"></script> -->

  <?= css('assets/css/index-200605.css') ?>

</head>
<body id="bootstrap-overrides">

  <?php if(isset($fasciaNews) && $fasciaNews): ?>
    <div id="fascia-news" class="d-flex align-items-center">
      <div class="content-wrapper font-color-white">
        <!--  
        < ?php for($i=0; $i++; $i<10): ?>
          < ?=$fasciaNews->kirbytext();?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        < ?php endfor ?>
        -->

        <!-- < ?=$fasciaNews->kirbytext();?> -->
        
        <?php
        $repeat = 3;
        // $fn = str_replace(" ", " ", $fasciaNews); ///// Problems with links !!!
        $fn = $fasciaNews;
        $txt = $fn;
        for($i=1; $i<$repeat; $i++){
          $txt .= "            ". $fn;
        }

        // $txt = $fasciaNews ."          ". $fasciaNews ."          ". $fasciaNews ."          ". $fasciaNews ."          ". $fasciaNews;
        echo kirbytext($txt);
        ?>

      </div>
    </div>
  <?php endif ?>

  <?php if(!$hideMenu): ?>
  
    <header>
      <?php 
        // Menu bar full
        $pagesToMenu = [
          "homepage"        => ["type" => "big-logo", "textColor" => "black-text"],
          "discipline"      => ["type" => "big-logo", "textColor" => "black-text"],
          "discipline-sub"  => ["type" => "normal", "textColor" => "black-text"],
          "corsi"           => ["type" => "normal", "textColor" => "black-text"],
          "la-scuola"       => ["type" => "normal", "textColor" => "black-text"],
          "manifesto"       => ["type" => "normal", "textColor" => "black-text"],
          "la-scuola"       => ["type" => "normal", "textColor" => "black-text"],
          "info"            => ["type" => "normal", "textColor" => "black-text"],
          "default"         => ["type" => "normal", "textColor" => "black-text"],
        ];
        $basePage = explode("/", $page->uri())[0];
        if(substr($page->uri(), 0, 11) == "discipline/"){
          $basePage = "discipline-sub";
        }
        if(!array_key_exists($basePage, $pagesToMenu)){
          $basePage = "default";
        }
        snippet('menu-bar', $pagesToMenu[$basePage]);
      ?>
    </header>
    
  <?php endif ?>


