<?php
$textColorString = [
  "logo" => [
    "black-text" => "",
    "white-text" => "font-color-white"
  ],
  "menu" => [
    "black-text" => "black-text",
    "white-text" => "white-text"
  ],
];
// $textColor = "black-text";
?>

<!-- xs sm -->
<div class="container-fluid super-cont d-block d-md-none">
  <div class="row mt-3">
    <div class="col-12 d-flex justify-content-between align-items-start">
      
      <!--  
      <h1><a class="font-super-ll hover-gold <?= $textColorString["logo"][$textColor] ?>" href="<?= page('homepage')->url() ?>">
        Scuola <span class="font-supersuper-ll">Super</span>iore<br />
        d'Arte Applicata<br />
      </a></h1>
      -->
      <a class="" href="<?= page('homepage')->url() ?>">
        <img class="logo-h-60" src="<?= $site->logoTwoLinesBlack()->toFile()->url() ?>" />
      </a>

      <button class="mt-2 hamburger hamburger--slider <?= $textColorString["menu"][$textColor] ?>" type="button" onclick="javascript:toggleXsMenu();">
        <span class="hamburger-box">
          <span class="hamburger-inner"></span>
        </span>
      </button>

    </div>
  </div>
</div>

<!-- md and up -->
<div class="container-fluid super-cont d-none d-md-block">
  <div class="row mt-3">
    <div class="col-md-6">
      
      <!--  
      <h1><a class="font-super-ll hover-gold <?= $textColorString["logo"][$textColor] ?>" href="<?= page('homepage')->url() ?>">
        Scuola <span class="font-supersuper-ll">Super</span>iore<br />
        d'Arte Applicata<br />
      </a></h1>
      -->
      <a class="" href="<?= page('homepage')->url() ?>">
        <img class="logo-h-60" src="<?= $site->logoTwoLinesBlack()->toFile()->url() ?>" />
      </a>

    </div>
    <div class="col-md-6 super-menu text-right">

      <?php snippet('menu', ["textColor" => $textColorString["menu"][$textColor]]) ?>

    </div>
  </div>
</div>