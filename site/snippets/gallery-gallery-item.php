<?php
/**
 * @param $items - Kirby Collection of files
 * @param $startIndex
 * @param $total
 */
?>

<div id="gallery-default">
  <div class="gallery-overlay">
    <div class="gallery-container d-flex justify-content-between w-100 h-100">
      
      <div class="sx justify-content-start">
        <a class="no-underline font-sans-l" href="javascript:showPrevPic();">&larr;</a>
      </div>
        
      <div class="center flex-grow w-100 h-100">
        <?php $i = 0; ?>
        <?php foreach($items as $item): 
          $caption = $item->caption()->value();
          $credits = $item->credits()->value();
          $url = $item->thumb(["height" => 900])->url();
          ?>
          <div id="<?= "gi-default-$i" ?>" class="gallery-item w-100 h-100">

            <div class="flex-grow-1 w-100" style="
            background-image: url(<?= $url ?>);
            background-size: contain;
            background-repeat: no-repeat;
            background-position: center center;
            "> </div>

            <div class="mt-3 flex-stretch">
              <p class="font-sans-s"><?= $caption ?></p>
              <p class="font-sans-sss mt-2"><?= $credits ?></p>
              <p class="font-sans-sss mt-2"><?= ($startIndex + $i) . " / $total" ?></p>
            </div>
          </div>
          <?php $i++; ?>
        <?php endforeach ?>
      </div>

      <div class="dx justify-content-end">
        <a class="no-underline font-sans-l" href="javascript:showNextPic();">&rarr;</a>
      </div>

      <div class="top-left">
        <a class="no-underline font-sans-s mr-3" href="javascript:dlCurrent();">Save</a>
      </div>
      <div class="top-right">
        <a class="no-underline font-sans-l" href="javascript:closeGallery();">&times;</a>
      </div>

    </div>
  </div>
</div>