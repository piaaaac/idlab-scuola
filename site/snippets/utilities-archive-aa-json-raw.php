<?php
$allOrdini = page("segreteria-ordini")->children();
$allCorsi = page("corsi")->children();

// --- all turni
$allTurni = new Pages;
foreach($allCorsi as $corso){
  foreach($corso->children() as $turno){
    $allTurni->add($turno);
  }
}


// --- find problematic ordini (strange characters)
// --- 1. find range
// echo "ordini:" . $allOrdini->slice(860, 10)->toJson();
// exit();
// --- 2. find exact ordine
// kill([
//   $allOrdini->nth(950)->content(),
//   $allOrdini->nth(951)->content(),
//   $allOrdini->nth(952)->content(),
//   $allOrdini->nth(953)->content(),
//   $allOrdini->nth(954)->content(),
//   $allOrdini->nth(955)->content(),
//   $allOrdini->nth(956)->content(),
//   $allOrdini->nth(957)->content(),
//   $allOrdini->nth(958)->content(),
//   $allOrdini->nth(959)->content(),
//   $allOrdini->nth(960)->content(),
//   $allOrdini->nth(961)->content(),
//   $allOrdini->nth(962)->content(),
//   $allOrdini->nth(963)->content(),
//   $allOrdini->nth(964)->content(),
//   $allOrdini->nth(965)->content(),
//   $allOrdini->nth(966)->content(),
//   $allOrdini->nth(967)->content(),
//   $allOrdini->nth(968)->content(),
//   $allOrdini->nth(969)->content(),
// ]);
// exit();


// --- make json strings
// $jsonOrdini = $allOrdini->slice(0, 970)->toJson();
$jsonOrdini = $allOrdini->toJson();
$jsonCorsi = $allCorsi->toJson();
$jsonTurni = $allTurni->toJson();

// --- create final json string
$json = <<<STRING
{
  "ordini": $jsonOrdini,
  "corsi": $jsonCorsi,
  "turni": $jsonTurni
}
STRING;

// --- output
header("Content-type: application/json; charset=utf-8");
echo $json;
