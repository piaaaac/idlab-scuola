<?php
include_once(kirby()->roots()->snippets() .'/commonfunctions.php'); // for newsDate
$news = page('newsbox')->children()->visible()->sortBy("dataNews", "desc");
if(isset($exclude)) $news = $news->not($exclude);
if(isset($limit)) $news = $news->limit($limit);
$num = $news->count();

// kill($news);

$nc = new Pages();
$nc->add($news->last());
$nc->add($news->slice(0, $num - 1));
$news = $nc;

function btnText($news){
  $ctas = [
    "avvisi"        => "LEGGI",
    "corsi"         => "LEGGI",
    "eventi"        => "SCOPRI",
    "mostre"        => "SCOPRI",
    "bandi"         => "PARTECIPA",
    "segnalazioni"  => "LEGGI"
  ];
  return $ctas[$news->tipo()->value()];
}

?>

<div id="showcase-news" class="">

  <div class="wrap-center">
    <div class="slick">
      <?php foreach($news as $n): ?>
        <div class="news-item-v3 align-self-stretch">
          <div class="cont-wrapper">
            <div class="pr-0">
              <p class="font-sans-ss mt-1 mb-4"><?= newsDate($n) ?></p>
              <div class="d-flex mb-4">
                <a href="<?= $n->url() ?>" class="font-sans-l font-color-red"><?= $n->title() ?></a>
              </div>
            </div>
            <div class="flex-grow-1"></div>
            <div>
              <a class="btn btn-primary btn-small red" href="<?= $n->url() ?>" role="button"><?= btnText($n) ?> &rarr;</a>
            </div>
          </div>
        </div>
      <?php endforeach ?>
    </div>
  </div>

  <div class="container-fluid super-cont">
    <div class="row">
      <div class="col-12 d-flex justify-content-between align-items-top mt-4 mb-5">
        <div class="dots"></div>
        <a class="font-sans-ss ml-5 no-underline" href="<?= page('newsbox')->url() ?>">&rarr; Archivio</a>
      </div>
    </div>
  </div>

  <!--  
  <div class="side-grads">
    <div class="container-fluid super-cont">
      <div class="width">
      </div>
    </div>
  </div>
  -->

</div>

<script>
  var slick = null;
  if ($("#showcase-news .slick").length > 0) {
    slick = $("#showcase-news .slick").slick({
      arrows:         false,
      slidesToShow:   5,
      slidesToScroll: 5,
      initialSlide:   0,
      swipeToSlide:   true,
      autoplay:       true,
      // variableWidth:  true,
      dots:           true,
      appendDots:     "#showcase-news .dots",
      responsive: [{
      
        breakpoint: 991, // md & down
        settings: {
          slidesToShow: 4,
          slidesToScroll: 4,
        }
      }, {
        breakpoint: 575, // xs
        settings: {
          slidesToShow: 3,
          slidesToScroll: 3,
        }
      }]
    });
  }
</script>








