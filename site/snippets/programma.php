<?php
/**
 * @param $programmaStructure - kirby structure field
 */

$items = [];
foreach($programmaStructure as $item){
  $items[] = [
    "title"       => $item->prname()->value(),
    "anchor-name" => str::slug($item->prname()->value()),
    "textHtml"    => $item->prtext()->kirbytext(),
  ];
}

?>

<div class="py-3"></div>
<hr class="my-2" />
<div class="row">
  <div class="col-md-4">PROGRAMMA</div>
  <div class="col-md-8">
  <?php foreach($items as $item): ?>
    
    <a class="font-sans-s font-color-blue mr-1" href="javascript: scrollToId('programma-<?= $item["anchor-name"] ?>');">
      <?= $item["title"] ?>
    </a>
    <!-- $txt = $item->prtext()->value(); -->

  <?php endforeach ?>
  </div>
</div>

<?php foreach($items as $item): ?>
  
<hr class="my-2" />
<div id="programma-<?= $item["anchor-name"] ?>"></div>
<div class="row programma-item">
  <div class="col-md-4">
    <p><?= $item["title"] ?></p>
  </div>
  <div class="col-md-8">
    <?= $item["textHtml"] ?>
  </div>
</div>

<?php endforeach ?>
