  <!-- Bootstrap -->
  <link rel="stylesheet" href="<?= $site->url() ?>/assets/lib/bootstrap.min.css">
  <script src="<?= $site->url() ?>/assets/lib/jquery.3.3.1.min.js"></script>
  <script src="<?= $site->url() ?>/assets/lib/popper.min.js"></script>
  <script src="<?= $site->url() ?>/assets/lib/bootstrap.min.js"></script>

  <!-- icons -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
  <!-- <link rel="stylesheet" href="<?= $site->url() ?>/assets/lib/fontawesome-v5.2.0/all.css"> -->
  

  <!-- mailchimp css -->
  <link href="//cdn-images.mailchimp.com/embedcode/classic-10_7.css" rel="stylesheet" type="text/css">
  <style type="text/css">#mc_embed_signup{ clear:left; }</style>

  <?php if (isset($loadFormScripts) && $loadFormScripts === true): ?>

    <!-- gijgo datepicker -->
    <script src="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/js/gijgo.min.js" type="text/javascript"></script>
    <link href="https://cdn.jsdelivr.net/npm/gijgo@1.9.6/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap-validate -->
    <script src="<?= $site->url() ?>/assets/offline/bootstrap-validate.js"></script>
    <!-- jQuery validate -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js" type="text/javascript"></script>
  
  <?php endif ?>

  <!-- Page specific -->
  <?php if ($page->uid() == "lapis"): ?>
    <script src="<?= $site->url() ?>/assets/lib/bigpicture/dist/BigPicture.min.js"></script>
    <script src="<?= $site->url() ?>/assets/lib/clipboard.js/dist/clipboard.min.js"></script>
  <?php endif ?>

  <?php if (in_array($page->template(), ["homepage", "newsbox-entry"])): ?>
    <script src="<?= $site->url() ?>/assets/lib/slick/slick/slick.min.js"></script>
    <link rel="stylesheet" type="text/css" href="<?= $site->url() ?>/assets/lib/slick/slick/slick.css"/>
  <?php endif ?>

