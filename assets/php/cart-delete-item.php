<?php 
define('DS', DIRECTORY_SEPARATOR);

// load kirby
require("..". DS ."..". DS .'kirby'. DS .'bootstrap.php');
$kirby = kirby();
$site = $kirby->site();
$returnUrl = page("iscrizione1")->url(); // not working fromm this dir. Create page and move this file
$returnUrl = "https://scuolaarteapplicata.it/super/iscrizione1";
// echo $returnUrl;
// exit();

$delete = $_GET["articleeeeId"];
// $orderId = $_GET["orderId"];

$cartArticleeeeIdsString = a::get($_COOKIE, c::get("cartCookieKey"));
$cartArticles = explode(",", $cartArticleeeeIdsString);
$pos = array_search($delete, $cartArticles);
if($pos === false){
  echo "Errore nel rimuovere l'elemento dal carrello (Error code 30909908). 
  <a href=\"$returnUrl\">Torna all'iscrizione</a>";
  exit();
}
unset($cartArticles[$pos]);

// setcookie(name,value,expire,path,domain,secure,httponly);
setcookie(
  c::get("cartCookieKey"),                        // $cookie_name, 
  implode(",", $cartArticles),                    // $cookie_value, 
  time() + (60 * c::get("cartCookieDuration")),   // time() + (86400 * 30), 
  c::get("cartCookiePath"),
  c::get("cartCookieDomain")
);

// echo "\n".'$cartArticleeeeIdsString='. $cartArticleeeeIdsString;
// echo "\n".'$cartArticles='. $cartArticles;
// echo "\n".'$pos='. $pos;
// exit();

// redirect::to(page("iscrizione1")->url() ."/order:$orderId");
redirect::to($returnUrl);

?>
