<?php
define('DS', DIRECTORY_SEPARATOR);

// load kirby
require("..". DS ."..". DS .'kirby'. DS .'bootstrap.php');
$kirby = kirby();
$site = $kirby->site();

include_once(kirby()->roots()->snippets() .'/commonfunctions.php');

/////////////// 
////////
// Server
$cartArticleeeeIdsString = a::get($_COOKIE, c::get("cartCookieKey"));
// Local
// $cartArticleeeeIdsString = "corso-15~turno-3,corso-15~turno-2";
////////
/////////////// 

$cartArray = cartObjectFromArticleeeeIdsString($cartArticleeeeIdsString);
$jsonCart = json_encode($cartArray);
// echo json_last_error();
echo $jsonCart;
exit();

/*
echo "<br>--------------------------<br>";
echo $jsonCart;
echo "<br>--------------------------<br>";

[
  {
    "corso": {
      "corsoId": "corso-1",
      "title": "Fumetto breve",
      "costo": "180"
    },
    "turno": {
      "uid": "asdadaw",
      "dataInizio": "2018-08-01",
      "dataFine": "2018-09-12"
    }
  },
  {
    "corso": {
      "corsoId": "corso-1",
      "title": "Fumetto breve",
      "costo": "180"
    },
    "turno": {
      "uid": "dasdasdasdsa",
      "dataInizio": "2018-07-17",
      "dataFine": "2018-07-17"
    },
    "sconto": {
      "description": "5 percent",
      "amt": 9
    }
  },
  {
    "corso": {
      "corsoId": "corso-2",
      "title": "Mosaico breve",
      "costo": "300"
    },
    "turno": {
      "uid": "ksdjfndskj",
      "dataInizio": "2018-07-19",
      "dataFine": "2018-07-27"
    }
  }
]
*/

?>