<?php
define('DS', DIRECTORY_SEPARATOR);

// load kirby
require("..". DS ."..". DS .'kirby'. DS .'bootstrap.php');
$kirby = kirby();
$site = $kirby->site();
// a::show($kirby);
include_once(kirby()->roots()->snippets() .'/commonfunctions.php');

$articleeeeId =  str_replace("---", "~", param("articleeeeId"));
if(!$articleeeeId){
  echo "Pagina non trovata: nessun corso/turno ricevuto.";
  exit();
}
$corsoId = explode("~", $articleeeeId)[0];
$turnoUid = explode("~", $articleeeeId)[1];
$corso = page("corsi")->children()->findBy("corsoId", $corsoId);
if(!$corso){
  echo "Corso non trovato: $corsoId";
  exit();
}
$filtered = $corso->children()->filterBy("uid", $turnoUid);
if($filtered->count() == 0){
  echo "Turno non trovato: $turnoUid";
  exit();
}
$turno = $filtered->first();
// echo $corso->title();
// echo $turno->title();

// exit();

$ordini = page("segreteria-ordini")->children()->filter(function($ordine) use ($articleeeeId) {
  $articleeeeIds = explode(",", $ordine->articleeeeIds()->value());
  return(in_array($articleeeeId, $articleeeeIds));
});
$ordini->sortBy("pagamentoOk", "desc");
// echo $ordini->count();
$ordiniPagati = $ordini->filterBy("pagamentoOk", "1");

?>

<?php /* via https://github.com/cognitom/paper-css/blob/master/examples/a3-landscape.html */ ?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Registro A3 — <?= $corso->title()->value() ?> / <?= $turno->title()->value() ?></title>

  <!-- Normalize or reset CSS with your favorite library -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/7.0.0/normalize.min.css">

  <!-- Load paper.css for happy printing -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/paper-css/0.4.1/paper.css">

  <!-- Set page size here: A5, A4 or A3 -->
  <!-- Set also "landscape" if you need -->
  <style>
    @page { size: A3 landscape }
    *{
      font-family: sans-serif;
      font-size: 13px;
    }
    table{
      border-spacing: 0px;
    }
    hr{
      height: 0px;
      border: 0;
      border-bottom: 2px solid black;
    }
    .w-100 { width: 100%; }
    .header { 
      font-size: smaller;
      text-transform: uppercase;
      padding-bottom: 8px;
    }
    .hspacer { 
      width: 60px;
    }
    .bb { 
      border-bottom: 1px solid #ccc; 
      padding: 5px 5px 5px 0;
    }
  </style>
</head>

<!-- Set "A5", "A4" or "A3" for class name -->
<!-- Set also "landscape" if you need -->
<body class="A3 landscape">

  <!-- Each sheet element should have the class "sheet" -->
  <!-- "padding-**mm" is optional: you can set 10, 15, 20 or 25 -->
  <section class="sheet padding-10mm">

    <table cellpadding="0">
      <tr>
        <td class="header">Corso</td>
        <td class="hspacer"></td>
        <td class="header">Turno</td>
      </tr>
      <tr>
        <td><b><?= $corso->title()->value() ?></b></td>
        <td></td>
        <td><b><?= $turno->title()->value() ?></b></td>
      </tr>
    </table>
    
    <div class="w-100">
      <hr />
      <br/><br/><br/>
    </div>

    <!-- Write HTML just like a web page -->
    <table class="w-100">
      <tr class="w-100">
        <td class="header" style="width: 15%">Nome</td>
        <td class="header" style="width: 15%">Cognome</td>
        <td class="header" style="width: 15%">Email</td>
        <td class="header" style="width: 65%">Tel</td>
      </tr>
      <?php foreach($ordiniPagati as $item): ?>
        <tr>
          <td class="bb"><?= $item->form_cognome()->value() ?></td>
          <td class="bb"><?= $item->form_nome()->value() ?></td>
          <td class="bb"><?= $item->form_email()->value() ?></td>
          <td class="bb"><?= $item->form_telefono()->value() ?></td>
        </tr>
      <?php endforeach ?>
      <?php for($i = 0; $i < 36 - $ordiniPagati->count(); $i++): ?>
        <tr>
          <td class="bb">&nbsp;</td>
          <td class="bb">&nbsp;</td>
          <td class="bb">&nbsp;</td>
          <td class="bb">&nbsp;</td>
        </tr>
      <?php endfor ?>

    </table>

  </section>

</body>

</html>

<?php exit(); ?>

<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width,initial-scale=1.0">
  <meta name="description" content="<?= $site->description()->html() ?>">
  <link rel="stylesheet" type="text/css" href="<?= "../../../assets/css/index.css" ?>">
  <?php snippet('load-scripts', ["loadFormScripts" => false]) ?>
</head>
<body>
  <main id="print" class="registro">
    <div class="container-fluid super-cont">

      <!-- TITOLO -->

      <div class="row">
        <div class="col-12 my-5">
          <p class="my-0 font-sans-ll">Ordine <?= $page->orderId()->value() ?></p>
          <p class="my-0">A.F.: <?= $annoFormativoOrdine ?></p>
        </div>
      </div>

      <!-- DATI ANAGRAFICI -->

      <div class="row">
        <div class="col-12">
          <p>DATI ANAGRAFICI</p>
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-4">
          <p>
            <em>Cognome</em>
            <?= $formData["form_cognome"] ?>
          </p><p>
            <em>Nome completo</em>
            <?= $formData["form_nome"] ?>
          </p><p>
            <em>Luogo e data di nascita</em>
            <?= $formData["form_luogoNascita"] ?> 
            (<?= $formData["form_provinciaNascita"] ?>)
            &mdash;
            <?= $formData["form_dataNascita"] ?>
          </p><p>
            <em>Cittadinanza</em>
            <?= $formData["form_cittadinanza"] ?>
          </p><p>
            <em>Codice fiscale</em>
            <?= $formData["form_codiceFiscale"] ?>
          </p>
        </div>

        <div class="col-4">
          <p>
            <em>Residenza</em>
            <?= $formData["form_residenzaCivico"] ?>,
            <?= $formData["form_residenzaIndirizzo"] ?>
          </p><p>
            <em>CAP e citt&agrave;</em>
            <?= $formData["form_residenzaCap"] ?>
            &mdash;
            <?= $formData["form_residenzaLuogo"] ?>
            (<?= $formData["form_residenzaProvincia"] ?>)
          </p><p>
            <em>Telefono</em>
            <?= $formData["form_telefono"] ?>
          </p><p>
            <em>Email</em>
            <?= $formData["form_email"] ?>
          </p>
        </div>
        
        <div class="col-4">
          <?php if($formData["fatturazioneAziendale"]): ?>
            <p>
              <em>Fatturazione aziendale</em>
              <?= $formData["form_aziendaRagioneSociale"] ?>
              <br /><?= $formData["form_aziendaCivico"] ?>,
              <?= $formData["form_aziendaIndirizzo"] ?>
              <br /><?= $formData["form_aziendaCap"] ?>
              <?= $formData["form_aziendaLuogo"] ?>
              (<?= $formData["form_aziendaProvincia"] ?>)
            </p><p>
              <em>Codice fiscale</em>
              <?= $formData["form_aziendaCodiceFiscale"] ?>
            </p><p>
              <em>Partita IVA</em>
              <?= $formData["form_aziendaPartitaIva"] ?>
            </p>
          <?php else: ?>
            <p>
              <em>Fatturazione aziendale</em>
              &mdash;
            </p>
          <?php endif ?>

        </div>
      </div>

      <!-- CORSI ACQUISTATI -->

      <div class="row mt-5">
        <div class="col-12 d-flex justify-content-between">
          <p>
            ARTICOLI ACQUISTATI (<?=count($cartArray)?>)
          </p>
          <!-- <p>Anno Formativo: <?= $annoFormativoOrdine ?></p> -->
        </div>
        <div class="col-12">
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-2"><em>Anno formativo</em></div>
        <div class="col-2"><em>Tipologia</em></div>
        <div class="col-3"><em>Corso</em></div>
        <div class="col-3"><em>Turno/Anno</em></div>
        <div class="col-2"><em>Costo e sconti</em></div>
      </div>
      <div class="row">
        <div class="col-12"><hr class="my-2" /></div>
      </div>

      <?php foreach($cartArray as $item): ?>
        <div class="row">
          <div class="col-2">
            <?= annoFormativo($item->turno->dataInizio, $item->corso->corsoId) ?>
          </div>
          <div class="col-2">
            <?php
            $corso = page("corsi")->children()->findBy("corsoId", $item->corso->corsoId);
            echo $corso->tipo()->value();
            $turno = page("corsi/". $corso->uid() ."/". $item->turno->uid);
            // a::show($turno);
            ?>
          </div>
          <div class="col-3">
            <?= $item->corso->title ?>
          </div>
          <div class="col-3">
            <?= $item->turno->nome ?>
            <em class="font-color-black">
              <?= turnoDates($turno, "") ?>
            </em>
          </div>
          <div class="col-2">
            <?php

            if(isset($item->sconto)){
              echo $item->turno->costo;
              echo " - ";
              echo $item->sconto->amt;
              echo " = ";
              echo "&euro; ". $item->costoFinale;
              echo "<em class=\"font-color-black\">(sconto: ". $item->sconto->description .")</em>";
            } else {
              echo "&euro; ". $item->costoFinale;
            }

            ?>
          </div>
        </div>
        <div class="row">
          <div class="col-12"><hr class="my-2" /></div>
        </div>
      <?php endforeach ?>

      <!-- ORDINE -->

      <div class="row mt-5">
        <div class="col-12">
          <p>ORDINE</p>
          <hr />
        </div>
      </div>

      <div class="row">
        <div class="col-4">
          <p>
            <em>Importo totale ordine</em>
            <?= "&euro; ". $page->importoTotale()->value() ?>
          </p><p>
            <em>Pagamento</em>
            <?= $page->pagamentoOk()->value() 
                  ? $page->pagamentoMetodo()->value() 
                  : "Ordine non ancora pagato.";
            ?>
          </p><p>
            <em>Rif. pagamento</em>
            <?= $page->pagamentoRiferimento()->value() 
                  ? $page->pagamentoRiferimento()->value() 
                  : "&mdash;";
            ?>
          </p>
        </div>
        <div class="col-4">
          </p><p>
            <em>Data creazione</em>
            <?= dateTimeFormatted($page->form_dataOraOrdine()->value()) ?>
          </p><p>
            <em>Data attivazione ordine</em>
            <?php if($d = $page->dataOraVerifica()->value()){ echo dateTimeFormatted($d); } else { echo "—"; } ?>
          </p><p>
            <em>Data pagamento</em>
            <?php if($d = $page->dataOraPagamento()->value()){ echo dateTimeFormatted($d); } else { echo "—"; } ?>
        </div>
        <div class="col-4">
          <p>
            <em>Note della segreteria</em>
            <?= $page->note()->value() ? $page->note()->value() : "&mdash;" ?>
          </p>
        </div>
      </div>


    </div>
  </main>
  
</body>
</html>
