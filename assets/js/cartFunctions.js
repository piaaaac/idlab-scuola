
// https://www.sitepoint.com/how-to-deal-with-cookies-in-javascript/
// setCookie("website", "audero.it", new Date(new Date().getTime() + 10000));
// setCookie("author", "aurelio", 30);
function setCookie(key, value, expires, path, domain) {
  var cookie = key + "=" + value + "; ";
  if (expires) {
    if(expires instanceof Date) {
      if (isNaN(expires.getTime())){
       expires = new Date();
      }
    }
    else {
      expires = new Date(new Date().getTime() + parseInt(expires) * 1000 * 60 * 60 * 24);
    }
    cookie += "expires=" + expires.toGMTString() + "; ";
  }
  if (path) {
    cookie += "path=" + path + "; ";
  }
  if (domain) {
    cookie += "domain=" + domain + "; ";
  }
  document.cookie = cookie;
}

function getCookie(key) {
  return ('; ' + document.cookie).split('; ' + key + '=').pop().split(';').shift()
}

function setCartCookie(value) {
  setCookie(
    cartCookieKey, 
    encodeURIComponent(value), 
    new Date(new Date().getTime() + cartCookieDuration * 60000), 
    cartCookiePath,   // "/super/",
    cartCookieDomain  // ".scuolaarteapplicata.it"
  );
}

function getCartCookie() {
  return decodeURIComponent(getCookie(cartCookieKey))
}

function addToCartCookie(articleeeeId){
  // console.log(articleeeeId);
  var articles = getCartCookie().split(",")

  // V1
  // don't allow duplicate rows
  // if(articles.includes(articleeeeId)){
  //   alert("L'elemento è già presente nel carrello.")
  // } else {
  //   if(!articles || articles == ""){ articles = [] }
  //   articles.push(articleeeeId)
  //   var value = articles.join(",")
  //   setCartCookie(value);
  //   console.log("Cookie updated: ", getCartCookie())
  // }

  // V2
  // Allow duplicate rows
  if(!articles || articles == ""){ articles = [] }
  articles.push(articleeeeId)
  var value = articles.join(",")
  setCartCookie(value);
  console.log("Cookie updated: ", getCartCookie())

}

function removeFromCartCookie(articleeeeId){
  var articles = getCartCookie().split(",")
  if(!articles.includes(articleeeeId)){
    alert("Turno non presente nel carrello")
  } else {
    var index = articles.indexOf(articleeeeId)
    articles.splice(index, 1)
    var value = articles.join(",")
    setCartCookie(value);
    console.log("Cookie updated: ", getCartCookie())
  }
}

function handleEditCart(action, articleeeeId){
  if(action === "add"){
    addToCartCookie(articleeeeId)
    toggleCartPanel(true)
  } else if(action === "remove") {
    removeFromCartCookie(articleeeeId)
  }
  $.ajax({
    url: ajaxCallUrl,
    success: function(result){
      updateCart(JSON.parse(result))
      updateButton(JSON.parse(result))
      updateCartNum(JSON.parse(result))
    }
  });
}

function clearCart(articleeeeId){
  setCartCookie("")
  $.ajax({
    url: ajaxCallUrl,
    success: function(result){
      updateCart(JSON.parse(result))
      updateCartNum(JSON.parse(result))
    }
  });
}

function updateButton(json){

  // Disabilitato per permettere 
  // l'acquisto multiplo dello stesso corso 
  return;

  var cartArticleeeeIds = json.map(function(j){
    return j.articleeeeId
  })
  console.log($("#page-corso-dropdown .turno-item"))
  $("#modal-turni .turno-item").each(function(i,e){
    console.log(e)
    var articleeeeId = $(e).attr("data-articleeeeId")
    console.log("cartArticleeeeIds", cartArticleeeeIds)
    console.log("articleeeeId", articleeeeId)
    if(cartArticleeeeIds.includes(articleeeeId)){
      $(e).toggleClass("disabled", true)
    } else {
      $(e).toggleClass("disabled", false)
    }
  })
}

function updateCartNum(json){
  console.log("updateCartNum()", json);
  var n = json.length
  if(n == 0){
    $(".cart-number").text("")
    $(".cart-number-dot").toggleClass("hide", true)
  } else {
    $(".cart-number").text(n)
    $(".cart-number-dot").toggleClass("hide", false)
  }
}

function updateCart(json){
  console.log("updateCart() data:", json)

  // alert(JSON.stringify(json));

  if(json.length == 0){ 

    // totale
    $("#cart-empty").toggleClass("d-none", false)
    $("#cart-totale").toggleClass("d-none", true)
    $("#cart-totale .ammontare").text("")
    $("#cartItemList").html("")

  } else {

    var cont = document.getElementById("cartItemList");
    var totale = 0;
    cont.innerHTML = ""

    json.forEach(function(article){
      var sconto = article.hasOwnProperty("sconto") ? true : false
      
      // texts
      var datesString = ""
      if(article.turno.dataInizio != "" && article.turno.dataFine != ""){
        datesString = ", " + article.turno.dataInizio + " — " + article.turno.dataFine
      }
      var txtTitle = document.createTextNode(article.corso.title)
      var txtTurno = document.createTextNode(article.turno.nome + datesString)
      var txtSconto = null
      var price = +article.turno.costo
      if(sconto){
        txtSconto = document.createTextNode(article.sconto.description)
        price -= article.sconto.amt
      }
      totale += price;
      var txtPrice = document.createTextNode(price + " €")
      
      // dom elements
      var a = document.createElement("a");
      var li = document.createElement("li");
      var p1 = document.createElement("p");
      var p2 = document.createElement("p");
      var span1 = document.createElement("span");
      var span2 = document.createElement("span");
      var span3 = document.createElement("span");
      var span4 = document.createElement("span");

      // attributes
      li.className = "d-flex justify-content-between"
      p1.className = "nome-corso"
      span1.className = "d-block font-color-white font-super-m"
      span2.className = "d-block font-color-white font-sans-sss mt-2"
      p2.className = "prezzo"
      a.className = "font-sans-sss font-color-black"
      a.innerHTML = "Rimuovi"
      a.href = "javascript:;"
      a.onclick = function(e){handleEditCart('remove', article.articleeeeId)}
      span3.className = "d-block font-color-blue font-sans-s"
      span4.className = "d-block font-color-blue font-sans-sss"

      // append
      li.appendChild(p1)
      li.appendChild(p2)
      p1.appendChild(span1)
      p1.appendChild(span2)
      p2.appendChild(a)
      p2.appendChild(span3)
      span1.appendChild(txtTitle)
      span2.appendChild(txtTurno)
      span3.appendChild(txtPrice)
      if(sconto){
        p2.appendChild(span4)
        span4.appendChild(txtSconto)
      }
      cont.appendChild(li)
    })

    // totale
    $("#cart-empty").toggleClass("d-none", true)
    $("#cart-totale").toggleClass("d-none", false)
    $("#cart-totale .ammontare").text(Math.round(totale*100)/100 + " €")

  }
}


/*
[
  {
    "corso": {
      "corsoId": "corso-1",
      "title": "Fumetto breve",
      "costo": "180"
    },
    "turno": {
      "uid": "asdadaw",
      "dataInizio": "2018-08-01",
      "dataFine": "2018-09-12"
    }
  },
  {
    "corso": {
      "corsoId": "corso-1",
      "title": "Fumetto breve",
      "costo": "180"
    },
    "turno": {
      "uid": "dasdasdasdsa",
      "dataInizio": "2018-07-17",
      "dataFine": "2018-07-17"
    },
    "sconto": {
      "description": "5 percent",
      "amt": 9
    }
  },
  {
    "corso": {
      "corsoId": "corso-2",
      "title": "Mosaico breve",
      "costo": "300"
    },
    "turno": {
      "uid": "ksdjfndskj",
      "dataInizio": "2018-07-19",
      "dataFine": "2018-07-27"
    }
  }
]
*/





