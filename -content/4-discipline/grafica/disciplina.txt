Title: Grafica

----

Nome: Grafica

----

Capolettera: G

----

Testo: 

La grafica è il prodotto di un progetto di comunicazione visiva che vive di tipografia, fotografia e illustrazioni.
Per quanto sia un’arte applicata relativamente giovane, il nostro mondo è completamente immerso nella grafica e si esprime costantemente attraverso di essa.
Trattandosi della lingua visiva della comunicazione, la grafica risponde a delle regole precise che permettono la conversazione tra i soggetti, la comprensione del messaggio, ... Caratteristiche che da un lato rendono la grafica un linguaggio codificato, riconoscibile e vincolato, dall’altro le permettono di muoversi tra i confini della comunicazione visiva con grande libertà espressiva.

I corsi di grafica intendono insegnare a trasformare le idee in immagini condivisibili, comprensibili e adatte a qualsiasi media/contesto, e a preparare all’uso degli strumenti, digitali e non, propri dell’elaborazione di un prodotto grafico.

----

Docenterappresentante: giuseppe-rossello

----

Imgdisciplina: disciplina-grafica.png

----

Snimgfile: disciplina-grafica.png

----

Sntitle: Grafica

----

Sndesc: La grafica è il prodotto di un progetto di comunicazione visiva che vive di tipografia, fotografia e illustrazioni.